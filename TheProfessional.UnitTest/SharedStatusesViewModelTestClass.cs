﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheProfessional.ViewModel;
using System.Data;
using TheProfessional.Model;

namespace TheProfessional.UnitTest
{
    [TestClass]
    public class SharedStatusesViewModelTestClass
    {
        [TestMethod]
        public void SharedStatusesViewModelTest()
        {
            SharedStatusesModel model = new SharedStatusesModel("U", "X", "Suspended", "#00FFFFFF"); 
            
            SharedStatusesViewModel vm = new SharedStatusesViewModel();
            DataTable table = vm.GetAllRecords();

            vm.AddRecord(model);
            table = vm.GetAllRecords();

            model.Description = "Unsubscribed";
            vm.EditRecord(model);
            table = vm.GetAllRecords();

            vm.DeleteRecord(model);
            table = vm.GetAllRecords();
        }
    }
}
