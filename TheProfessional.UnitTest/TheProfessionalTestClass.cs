﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows;
using TheProfessional;

namespace TheProfessional.UnitTest
{
    [TestClass]
    public class TheProfessionalTestClass
    {
        [TestMethod]
        public void TheProfessionalTest()
        {
            Application app = new Application();  
            TheProfessional.View.ViewWindow win = new TheProfessional.View.ViewWindow();
            app.Run(win);
        }
    }
}
