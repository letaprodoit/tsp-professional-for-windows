using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using TheProfessional.Library;

namespace TheProfessional.Library.Converters
{
	/// <summary>
	/// Enum to value converter.
	/// </summary>
	public class EnumToValueConverter : IValueConverter
	{
		/// <summary>
		/// Converts an Enum value to it's string.
		/// </summary>
		public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
			return GetDefaultValue ( value as Enum );
		}


		/// <summary>
		/// Converts an Enum's string value to the Enum value.
		/// </summary>
		public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			return GetEnumValue ( value as string, targetType );
		}


        /// <summary>
        /// Converts the enum values to a list
        /// </summary>
        /// <returns>The enum type.</returns>
        /// <param name="enumType">The enum type.</param>
        /// <param name="returnDefaults">Flag to determine if the list should be the enum's defaultvalue or the enum's value.</param>
        public static List<ListEntry> ConvertToList(Type enumType, bool returnDefaults)
        {
            List<ListEntry> items = new List<ListEntry>();

            if (enumType == null)
                return items;

            foreach (Enum enumValue in Enum.GetValues(enumType))
            {
                object value = GetDefaultValue(enumValue);

                if (!returnDefaults)
                {
                    value = enumValue;
                }

                if (!IsNullOrEmpty(enumValue))
                {
                    items.Add(new ListEntry { DisplayName = enumValue.ToString(), Source = value });
                }
            }

            return items;
        }

        /// <summary>
        /// Determine if the current enum value is a null entry
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        private static bool IsNullOrEmpty(Enum enumValue)
        {
            object eValue = enumValue;
            string dValue = GetDefaultValue(enumValue);

            if (String.Equals(eValue.ToString(), "None") && dValue == "")
                return true;

            return false;
        }

		/// <summary>
		/// Gets the default value.
		/// </summary>
		/// <returns>The default value.</returns>
		/// <param name="value">Value.</param>
		public static string GetDefaultValue( Enum value )
		{
			if (value == null)
				return "";

			FieldInfo fieldInfo = value.GetType ().GetField (value.ToString ());
			DefaultValueAttribute[] attributes = (DefaultValueAttribute[])fieldInfo.GetCustomAttributes (typeof(DefaultValueAttribute), false);

			if (attributes.Length > 0) {
				return attributes [0].Value.ToString ();
			}
			return value.ToString ();
		}


		/// <summary>
		/// Gets the enum value.
		/// </summary>
		/// <returns>The enum value.</returns>
		/// <param name="value">Value.</param>
		/// <param name="enumType">Enum type.</param>
		public static object GetEnumValue( string value, Type enumType )
		{
			string[] names = Enum.GetNames ( enumType );
			foreach ( string name in names )
			{
				if ( GetDefaultValue ( ( Enum )Enum.Parse ( enumType, name ) ).Equals ( value ) )
				{
					return Enum.Parse ( enumType, name );
				}
			}
			throw new ArgumentException ( "The string is not a description or value of the specified enum." );
		}
	}
}