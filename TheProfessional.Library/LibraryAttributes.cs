using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace TheProfessional.Library
{
	/// <summary>
	/// Shared comment owners.
	/// </summary>
    public enum FieldType
    {
        /// <summary>
        /// Empty enum
        /// </summary>
        [DefaultValue("")]
        None = 0,
        /// <summary>
        /// The field is a textarea field
        /// </summary>
        [DefaultValue("Text")]
        Text = 1,
        /// <summary>
        /// The field is a file field
        /// </summary>
        [DefaultValue("File")]
        File = 2,
        /// <summary>
        /// The field is a email field
        /// </summary>
        [DefaultValue("Email")]
        Email = 3,
        /// <summary>
        /// The field is a phone field
        /// </summary>
        [DefaultValue("Phone")]
        Phone = 4,
        /// <summary>
        /// The field is a URL field
        /// </summary>
        [DefaultValue("URL")]
        URL = 5,
        /// <summary>
        /// The field is a date field
        /// </summary>
        Date = 6,
    };

	/// <summary>
    /// Simple attribute class for storing String Values for Enums
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum)]
    public sealed class StringValueAttribute : Attribute
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.StringValueAttribute"/> class.
		/// </summary>
		/// <param name="name">Name.</param>
        public StringValueAttribute(string name) 
        {
            _value = name; 
        }

        private readonly string _value;
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value></value>
        public string Value
        {
            get { return _value; }
        }
    }

    /// <summary>
    /// Simple attribute class for setting property Ignore flag
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class IgnoreAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.IgnoreAttribute"/> class.
        /// </summary>
        public IgnoreAttribute()
        {
            Value = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.IgnoreAttribute"/> class.
        /// </summary>
        /// <param name="flag">Name.</param>
        public IgnoreAttribute(bool flag)
        {
            Value = flag;
        }

        /// <summary>
        /// Gets/Sets the value.
        /// </summary>
        /// <value></value>
        public bool Value { get; set; }
    }
    /// <summary>
    /// Simple attribute class for setting property HideInGrid flag
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class HideInGridAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.HideInGridAttribute"/> class.
        /// </summary>
        public HideInGridAttribute()
        {
            Value = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.HideInGridAttribute"/> class.
        /// </summary>
        /// <param name="flag">Name.</param>
        public HideInGridAttribute(bool flag)
        {
            Value = flag;
        }

        /// <summary>
        /// Gets/Sets the value.
        /// </summary>
        /// <value></value>
        public bool Value { get; set; }
    }

    /// <summary>
    /// Simple attribute class for setting property Title flag
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
	public sealed class ColumnAttribute : Attribute
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Library.ColumnAttribute"/> class.
        /// </summary>
		public ColumnAttribute()
        {
        }

        /// <summary>
		/// Gets/Sets the title.
        /// </summary>
        /// <value></value>
		public string Title { get; set; }

		/// <summary>
		/// Gets/Sets the name.
		/// </summary>
		/// <value></value>
		public string Name { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if foreign key.
		/// </summary>
		/// <value></value>
		public bool ForeignKey { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if primary key.
		/// </summary>
		/// <value></value>
		public bool PrimaryKey { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if autoincrement.
		/// </summary>
		/// <value></value>
		public bool AutoIncrement { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if has a default value.
		/// </summary>
		/// <value></value>
		public bool HasDefault { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if a virtual column.
		/// </summary>
		/// <value></value>
		public bool Virtual { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if column should autosize
		/// </summary>
		/// <value></value>
		public bool AutoSize { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if column should sorted
		/// </summary>
		/// <value></value>
		public bool Sort { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if column should be ignored
		/// </summary>
		/// <value></value>
		public bool Ignore { get; set; }

        /// <summary>
        /// Gets/Sets value true/false if the field should be hidden in the grid
        /// </summary>
        /// <value></value>
        public bool HideInGrid { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if column data is required
		/// </summary>
		/// <value></value>
		public bool Required { get; set; }

		/// <summary>
		/// Gets/Sets value true/false if column data is readonly
		/// </summary>
		/// <value></value>
		public bool ReadOnly { get; set; }

		/// <summary>
		/// Gets/Sets the field type
		/// </summary>
		/// <value></value>
		public FieldType Field { get; set; }
    }

    /// <summary>
    /// Simple attribute class for storing String Values for Enums
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class FilterAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.FilterAttribute"/> class.
        /// </summary>
        public FilterAttribute()
        {
        }

        /// <summary>
        /// Gets/sets the field.
        /// </summary>
        /// <value></value>
        public string Field { get; set; }

        /// <summary>
        /// Gets/sets the value.
        /// </summary>
        /// <value></value>
        public string Value { get; set; }
    }
}
