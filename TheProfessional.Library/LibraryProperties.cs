﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheProfessional.Library
{
	/// <summary>
	/// Model properties.
	/// </summary>
    public static class LibraryProperties
    {
        /// <summary>
        /// The project's resources
        /// NOTE: Header strings have to be placed statically because they are used as an attribute
        /// and attribute values can not be dynamic only constants, strings or static arrays
        /// </summary>
        public class Resources : Properties.Resources 
        {
            // Registry types
            /// <summary>
            /// The HKEY_LOCAL_MACHINE.
            /// </summary>
            public const string HKEY_LOCAL_MACHINE = "HKEY_LOCAL_MACHINE";
            /// <summary>
            /// The HKEY_CLASSES_ROOT.
            /// </summary>
            public const string HKEY_CLASSES_ROOT = "HKEY_CLASSES_ROOT";
            /// <summary>
            /// The HKEY_CURRENT_CONFIG.
            /// </summary>
            public const string HKEY_CURRENT_CONFIG = "HKEY_CURRENT_CONFIG";
            /// <summary>
            /// The HKEY_CURRENT_USER.
            /// </summary>
            public const string HKEY_CURRENT_USER = "HKEY_CURRENT_USER";
            /// <summary>
            /// The HKEY_USERS.
            /// </summary>
            public const string HKEY_USERS = "HKEY_USERS";

            // Misc strings
            /// <summary>
            /// The APP_NAME - The name of the application.
            /// </summary>
            public const string APP_NAME = "The Professional";
            /// <summary>
            /// The REGISTRY_APP_PATH - Where the application is stored in the registry.
            /// </summary>
            public const string REGISTRY_APP_PATH = "Software\\The Software People\\" + APP_NAME;
            /// <summary>
            /// The REGISTRY_RUN_LOC - The registry entry where to place autorun apps.
            /// </summary>
            public const string REGISTRY_RUN_LOC = @"Software\\Microsoft\\Windows\\CurrentVersion\\Run";
			/// <summary>
			/// The header_ address.
			/// </summary>
			public const string Header_Address1 = "Address (Line #1)";
			/// <summary>
			/// The header_ address2.
			/// </summary>
			public const string Header_Address2 = "Address (Line #2)";
			/// <summary>
			/// The header_ city.
			/// </summary>
			public const string Header_City = "City";
            /// <summary>
            /// The header_ zip code.
            /// </summary>
            public const string Header_State = "State";
            /// <summary>
			/// The header_ zip code.
			/// </summary>
			public const string Header_ZipCode = "Zip Code";
			/// <summary>
			/// The header_ email.
			/// </summary>
			public const string Header_Email = "Email";
			/// <summary>
			/// The header_ phone1.
			/// </summary>
			public const string Header_Phone1 = "Phone #1";
			/// <summary>
			/// The header_ phone2.
			/// </summary>
			public const string Header_Phone2 = "Phone #2";
			/// <summary>
			/// The header_ fax.
			/// </summary>
			public const string Header_Fax = "Fax";
			/// <summary>
			/// The header_ URL.
			/// </summary>
			public const string Header_URL = "URL";
        }
    }
}
