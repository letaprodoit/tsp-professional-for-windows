using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Threading;

namespace TheProfessional.Library
{
	/// <summary>
	/// Base view model.
	/// </summary>
    public class ObservableObject : INotifyPropertyChanged
	{
		/// <summary>
		/// Occurs when property changed.
		/// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private bool? _closeWindowFlag;
        /// <summary>
        /// The _ close window flag. flag can be null (bool?)
        /// </summary>
        protected bool? CloseWindowFlag
        {
            get { return _closeWindowFlag; }
            set
            {
                _closeWindowFlag = value;
                RaisePropertyChanged("CloseWindowFlag");
            }
        }

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="prop">Property.</param>
		protected virtual void RaisePropertyChanged( string prop )
		{
			if ( PropertyChanged != null )
			{
				PropertyChanged ( this, new PropertyChangedEventArgs ( prop ) );
			}
		}

		/// <summary>
		/// Closes the window.
		/// </summary>
		/// <param name="result" type="bool">Result result can be null.</param>
		protected virtual void CloseWindow( bool? result = true )
		{
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                CloseWindowFlag = CloseWindowFlag == null
                    ? true
                    : !CloseWindowFlag;
            }));
        }
	}
}
