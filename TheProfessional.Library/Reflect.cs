﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Drawing;
using System.Linq;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using System.Diagnostics;

namespace TheProfessional.Library
{
	/// <summary>
	/// Reflection class with multi-purpose functions
	/// </summary>
	public static class Reflect
	{
        /// <summary>
        /// Method to return a list of foreign key values
        /// </summary>
        /// <param name="baseTable"></param>
        /// <param name="propertyName"></param>
        /// <param name="filter">ListEntry type that contains field/value pairs for filtering results of colleciton</param>
        /// <returns></returns>
		public static List<ListEntry> GetForeignKeyPropertiesCollection(DataTable baseTable, string propertyName, ListEntry filter = null)
		{
			bool emptyRecAdded = false;

			List<ListEntry> items = new List<ListEntry>();

			if (baseTable == null || String.IsNullOrEmpty(propertyName))
				return items;

			try
			{
				DataRelationCollection relationCollection = baseTable.ParentRelations;

				foreach (DataRelation relation in relationCollection)
				{
					DataColumn fkParentCol = (DataColumn)relation.ParentColumns.GetValue(0);
					DataColumn fkChildCol = (DataColumn)relation.ChildColumns.GetValue(0);

					if (fkChildCol.ColumnName.Equals(propertyName))
					{
						// If the relationship is one to one then break
						// because this item is not a collection
						if (fkParentCol.Unique && fkChildCol.Unique)
						{
							break;
						}

						if (!emptyRecAdded && fkChildCol.AllowDBNull)
						{
							items.Add(new ListEntry { DisplayName = "", Source = GetNullType(fkChildCol.DataType) });
							emptyRecAdded = true;
						}

						DataTable fkParentTable = relation.ParentTable;

						// If the parent table is empty then throw an exception
						if (fkParentTable.Rows.Count == 0)
						{
							throw new DataException(LibraryProperties.Resources.ErrorMessage_EmptyParentTable);
						}

						foreach (DataRow row in fkParentTable.Rows)
						{
							ListEntry entry = new ListEntry();
							entry.Source = row[fkParentCol];
							entry.DisplayName = "";

							foreach (DataColumn column in fkParentTable.Columns)
							{
								// find the first string column name that is not the parent
								// column and is not the child column and make it 
								// the reference column
								if (column != fkParentCol && column != fkChildCol)
								{
									if (column.DataType == typeof(string) && column.MaxLength > 1)
									{
                                        // If the property has a filter set
                                        // then only add the items that match the filter
                                        if (filter.IsSet())
                                        {
                                            if (row[filter.DisplayName].Equals(filter.Source))
                                            {
                                                entry.DisplayName = row[column].ToString().Truncate() + " - " + entry.Source;
                                                items.Add(entry);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            entry.DisplayName = row[column].ToString().Truncate() + " - " + entry.Source;
                                            items.Add(entry);
                                            break;
                                        }
									}
								}
							}
						}
					}
				}

				if (items.Count > 0)
				{
					items.Sort();

					// This shows calling the Sort(Comparison(T) overload using  
					// an anonymous method for the Comparison delegate.  
					// This method treats null as the lesser of two values.
					items.Sort(delegate(ListEntry x, ListEntry y) {
						if (x.DisplayName == null && y.DisplayName == null)
							return 0;
						if (x.DisplayName == null)
							return -1;
						if (y.DisplayName == null)
							return 1;

						return String.Compare (x.DisplayName, y.DisplayName, false, CultureInfo.CurrentCulture);
					});
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return items;
		}

		/// <summary>
		/// Method to get the null value given the type
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static object GetNullType(Type type)
		{
			if ( type == typeof( uint ) )
			{
				return ( uint )0;
			}
			if ( type == typeof( int ) )
			{
				return ( int )0;
			}
			if ( type == typeof( ulong ) )
			{
				return ( ulong )0;
			}
			if ( type == typeof( long ) )
			{
				return ( long )0;
			}
			if ( type == typeof( ushort ) )
			{
				return ( ushort )0;
			}
			if ( type == typeof( short ) )
			{
				return ( short )0;
			}
			if ( type == typeof( double ) )
			{
				return ( double )0;
			}
			if ( type == typeof( float ) )
			{
				return ( float )0;
			}
			if ( type == typeof( char ) )
			{
				return ( char )'\0';
			}

			return null;
		}

		/// <summary>
		/// Gets the value by type
		/// </summary>
		/// <returns>The value by type.</returns>
		/// <param name="type">Type.</param>
		/// <param name="propValue">The property value.</param>
		public static object GetPropertyValueByType(Type type, object propValue)
		{
			object value = propValue;

			try
			{
				if ( type.BaseType == typeof( Enum ) || type == typeof( Enum ) )
				{
					value = Converters.EnumToValueConverter.GetDefaultValue( ( Enum )propValue );
				}
				else if ( type.BaseType == typeof( char ) || type == typeof( char ) )
				{
					value = propValue.ToString();
					if ( value.Equals( "\0" ) )
					{
						value = "";
					}
				}
				else if ( type.BaseType == typeof( Color ) || type == typeof( Color ) )
				{
					value = propValue.ToString();
					// leave this case for future updates
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}
				
			return value;
		}

		/// <summary>
		/// Verify that all required properties in a class are valid
		/// </summary>
		/// <param name="rec"></param>
		/// <param name="showDialog"></param>
		/// <returns>bool</returns>
		public static bool VerifyRequiredProperties(object rec, bool showDialog)
		{
			bool valid = true;
			string errorMessage = LibraryProperties.Resources.InfoMessage_REQUIRED_FIELDS;
			string badFields = "";

            try
			{
				PropertyInfo[] properties = rec.GetType().GetProperties();
				foreach (PropertyInfo prop in properties)
				{
                    // If the property should be ignored then skip it
                    if (prop.GetCustomAttributes(typeof(IgnoreAttribute), false).Any())
                    {
                        continue;
                    }
                    
                    ColumnAttribute columnAtt = (ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).GetValue(0);

					// If the property is required
					if (columnAtt.Required)
					{
						Type type = prop.PropertyType;
						String name = prop.Name;

                        // if the property has an alternate title use it instead
                        // of the property name itself
                        if (!String.IsNullOrEmpty(columnAtt.Title))
                        {
                            name = columnAtt.Title;
                        }
                        
                        // if the property has a value that is not null
						// perform further tests to guarentee the property's value is valid
						object value = GetPropertyValueByType(type, prop.GetValue(rec, null));

						// if the value is null then mark the flag to false and
						// store the bad fields
						if (String.IsNullOrEmpty(value.ToString()))
						{
							valid = false;
							badFields += name + "\n";
						}
					}
				}

				// if the record is not valid and we want to see the dialog
				// then display it to the user
				if (!valid && showDialog)
				{
					badFields = badFields.ConvertEOLs();
					errorMessage = String.Format(errorMessage.ConvertEOLs(), badFields);

					MessageBox.ShowError(errorMessage, LibraryProperties.Resources.LabelName_TITLE_ERROR);
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return valid;
		}

		/// <summary>
		/// Get property values of a class based on flag
		/// </summary>
		/// <param name="rec"></param>
		/// <param name="primaryFlag">Return primary key values only</param>
		/// <param name="insertFlag">Return </param>
		/// <param name="updateFlag"></param>
		/// <param name="deleteFlag"></param>
		/// <param name="labels"></param>
        /// <param name="types"></param>
        /// <param name="names"></param>
        /// <returns></returns>
		public static object[] GetPropertyInfoByFlag(object rec, bool primaryFlag, bool insertFlag, 
            bool updateFlag, bool deleteFlag, bool labels = false, bool types = false, bool names = false)
		{
			List<object> _values = new List<object>();
			List<string> _labels = new List<string>();
            List<Type> _types = new List<Type>();
            List<string> _names = new List<string>();

			try
			{
				PropertyInfo[] properties = rec.GetType().GetProperties();
				foreach (PropertyInfo prop in properties)
				{
					// If the property should be ignored then skip it
					if (prop.GetCustomAttributes(typeof(IgnoreAttribute), false).Any())
					{
						continue;
					}

					ColumnAttribute columnAtt = (ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).GetValue(0);

					Type type = prop.PropertyType;
					String title = prop.Name;
					String name = title.ToLower(CultureInfo.CurrentCulture);

					// if the property has a value that is not null
					// perform further tests to guarentee the property's value is valid
					object value = GetPropertyValueByType(type, prop.GetValue(rec, null));

					// if the property has an alternate mapping name use it instead
					// of the property name itself
					if (!String.IsNullOrEmpty(columnAtt.Name))
					{
						name = columnAtt.Name;
					}

					// if the property has an alternate title use it instead
					// of the property name itself
					if (!String.IsNullOrEmpty(columnAtt.Title))
					{
						title = columnAtt.Title;
					}

					// add values that are primary keys
					if (primaryFlag)
					{
						// If the property is a autoincrement field then add it to the array
						if (columnAtt.PrimaryKey)
						{
							_values.Add(value);

							if (labels)
							{
								_labels.Add(title);
							}
							else if (types)
							{
								_types.Add(type);
							}
							else if (names)
							{
								_names.Add(name);
							}
						}
					}
					// insert values do not containe the autoincrement field
					// all others are valid
					else if (insertFlag)
					{
						// If the property is not an autoincrement field then add it to the array
						if (!columnAtt.AutoIncrement)
						{
							_values.Add(value);

							if (labels)
							{
								_labels.Add(title);
							}
							else if (types)
							{
								_types.Add(type);
							}
							else if (names)
							{
								_names.Add(name);
							}
						}
					}
					// update values do not containe the autoincrement field
					// and non-virtual fields
					else if (updateFlag)
					{
						// If the property is not an autoincrement or virtual field then add it to the array
						if (!columnAtt.AutoIncrement && !columnAtt.Virtual)
						{
							_values.Add(value);

							if (labels)
							{
								_labels.Add(title);
							}
							else if (types)
							{
								_types.Add(type);
							}
							else if (names)
							{
								_names.Add(name);
							}
						}
					}
					// delete fields contain all fields that are required and not Text fields
					else if (deleteFlag)
					{
						// If the property is not an autoincrement field then add it to the array
						if ((columnAtt.Required || columnAtt.PrimaryKey || columnAtt.ForeignKey || 
							columnAtt.HasDefault) && !columnAtt.Virtual && !columnAtt.Field.Equals(FieldType.Text))
						{
							_values.Add(value);

							if (labels)
							{
								_labels.Add(title);
							}
							else if (types)
							{
								_types.Add(type);
							}
							else if (names)
							{
								_names.Add(name);
							}
						}
					}
					else
					{
						_values.Add(value);

						if (labels)
						{
							_labels.Add(title);
						}
						else if (types)
						{
							_types.Add(type);
						}
						else if (names)
						{
							_names.Add(name);
						}
					}
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			// if we only want labels then return the labels
			// discarding the values
			if (labels)
			{
				return _labels.ToArray();
			}
            else if (types)
            {
                return _types.ToArray();
            }
            else if (names)
            {
                return _names.ToArray();
            }

			return _values.ToArray();
		}
    }

	/// <summary>
	/// Generic Reflection class
	/// </summary>
	public static class Reflect<T>
        where T : class, new()
    {
		private static T _get;
		/// <summary>
		/// Get/Set the Type's instances
		/// </summary>
		public static T Get
		{
			get
			{
				if (_get == null)
				{
					_get = new T();
				}

				return _get;
			}
			set { _get = value; }
		}


		/// <summary>
		/// Gets the method.
		/// </summary>
		/// <returns>The method.</returns>
		/// <param name="methodName">Method name.</param>
		public static MethodInfo GetMethod(string methodName)
		{
			try
			{
				return typeof( T ).GetMethod( methodName );
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String _methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(_methodName + ": " + ex.Message);
			}

			return null;
		}

        /// <summary>
        /// Gets the method.
        /// </summary>
        /// <returns>The method.</returns>
        /// <param name="instance">Instance.</param>
        /// <param name="methodName">Method name.</param>
        public static MethodInfo GetMethodByInstance(T instance, string methodName)
        {
			try
			{
				return instance.GetType().GetMethod(methodName);
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String _methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(_methodName + ": " + ex.Message);
			}

			return null;
		}

		/// <summary>
		/// Invokes the method.
		/// </summary>
		/// <returns>The method.</returns>
		/// <param name="methodName">Method name.</param>
		/// <param name="methodParams">Method parameters.</param>
		public static object InvokeMethod(string methodName, object[] methodParams)
		{
			try
			{
				if ( Get != null )
				{
					MethodInfo method = typeof( T ).GetMethod( methodName );
					return method.Invoke( Get, methodParams );
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String _methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(_methodName + ": " + ex.Message);
			}

			return null;
		}

        /// <summary>
        /// Invokes the method.
        /// </summary>
        /// <returns>The method.</returns>
        /// <param name="methodName">Method name.</param>
        /// <param name="methodParams">Method parameters.</param>
        /// <param name="paramTypes">Method parameter Types.</param>
        public static object InvokeOverloadedMethod(string methodName, object[] methodParams, Type[] paramTypes)
        {
			try
			{
				if (Get != null)
				{
					MethodInfo method = typeof(T).GetMethod(methodName, paramTypes);
					return method.Invoke(Get, methodParams);
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String _methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(_methodName + ": " + ex.Message);
			}

            return null;
        }

		/// <summary>
		/// Invokes the method for an instance
		/// </summary>
		/// <returns>The method.</returns>
		/// <param name="instance">Instance.</param>
		/// <param name="methodName">Method name.</param>
		/// <param name="methodParams">Method parameters.</param>
		public static object InvokeMethodByInstance(T instance, string methodName, object[] methodParams)
		{
			try
			{
				if ( instance != null )
				{
					MethodInfo method = instance.GetType().GetMethod( methodName );
					return method.Invoke( instance, methodParams );
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String _methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(_methodName + ": " + ex.Message);
			}

			return null;
		}

        /// <summary>
        /// Invokes an overloaded method for an instance
        /// </summary>
        /// <returns>The method.</returns>
        /// <param name="instance">Instance.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="methodParams">Method parameters.</param>
        /// <param name="paramTypes">Method parameter Types.</param>
		public static object InvokeOverloadedMethodByInstance(T instance, string methodName, object[] methodParams, Type[] paramTypes)
        {
			try
			{
				if (instance != null)
				{
					MethodInfo method = instance.GetType().GetMethod(methodName, paramTypes);
					return method.Invoke(instance, methodParams);
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String _methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(_methodName + ": " + ex.Message);
			}

            return null;
        }

		/// <summary>
		/// Gets the property.
		/// </summary>
		/// <returns>The property.</returns>
		/// <param name="propertyName">Property name.</param>
		public static PropertyInfo GetProperty(string propertyName)
		{
			try
			{
				return typeof(T).GetProperty(propertyName);
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return null;
		}

        /// <summary>
        /// Gets the property for an instance
        /// </summary>
        /// <param name="instance">Instance.</param>
        /// <returns>The property.</returns>
        /// <param name="propertyName">Property name.</param>
        public static PropertyInfo GetPropertyByInstance(T instance, string propertyName)
        {
			try
			{
				if (instance != null)
				{
					return instance.GetType().GetProperty(propertyName);
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return null;
        }

		/// <summary>
		/// Gets the property's value
		/// </summary>
		/// <returns>The property.</returns>
		/// <param name="propertyName">Property name.</param>
		public static object GetPropertyValue(string propertyName)
		{
			try
			{
				if ( Get != null )
				{
					PropertyInfo prop = typeof(T).GetProperty(propertyName);
					return prop.GetValue(Get);
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return null;
		}

        /// <summary>
        /// Sets the property's value
        /// </summary>
        /// <returns>The property.</returns>
        /// <param name="propertyName">Property name.</param>
        /// <param name="propertyValue">Property value.</param>
        public static void SetPropertyValue(string propertyName, object propertyValue)
        {
			try
			{
				if (Get != null)
				{
					PropertyInfo prop = typeof(T).GetProperty(propertyName);
					prop.SetValue(Get, propertyValue);
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}
        }

		/// <summary>
		/// Gets the property's value for an instance
		/// </summary>
		/// <returns>The property.</returns>
		/// <param name="instance">Instance.</param>
		/// <param name="propertyName">Property name.</param>
		public static object GetPropertyValueByInstance(T instance, string propertyName)
		{
			try
			{
				if ( instance != null )
				{
					PropertyInfo prop = instance.GetType().GetProperty(propertyName);
					return prop.GetValue(instance);
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return null;
		}

        /// <summary>
        /// Sets the property's value for an instance
        /// </summary>
        /// <returns>The property.</returns>
        /// <param name="instance">Instance.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="propertyValue">Property value.</param>
        public static void SetPropertyValueByInstance(T instance, string propertyName, object propertyValue)
        {
			try
			{
	            if (instance != null)
	            {
	                PropertyInfo prop = instance.GetType().GetProperty(propertyName);
	                prop.SetValue(instance, propertyValue);
	            }
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}
        }
    }
}

