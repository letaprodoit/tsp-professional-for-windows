﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Globalization;

namespace TheProfessional.Library
{
	/// <summary>
	/// Combo box item.
	/// </summary>
    public class ListEntry : IEquatable<ListEntry>, IComparable<ListEntry>
    {
		/// <summary>
		/// Gets or sets the text.
		/// </summary>
		/// <value>The text.</value>
        public string DisplayName { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
        public object Source { get; set; }

        /// <summary>
        /// Gets or sets boolean value to determine if the entry has been set with a name and sourc
        /// </summary>
        /// <value>The value.</value>
        public bool IsSet()
        {
            if (!String.IsNullOrEmpty(DisplayName) && Source != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Return the display member for the ComboBox
        /// </summary>
        public static string DisplayMember
        {
            get { return "DisplayName"; }
        }

        /// <summary>
        /// Return the value member for the ComboBox
        /// </summary>
        public static string ValueMember
        {
            get { return "Source"; }
        }

 		/// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="TheProfessional.Library.ListEntry"/>.
		/// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="TheProfessional.Library.ListEntry"/>.</returns>
        public override string ToString()
        {
            return DisplayName;
        }

        /// <summary>
        /// Determine if the two objects are equal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) 
                return false;

            ListEntry objAsPart = obj as ListEntry;

            if (objAsPart == null)
                return false;
                
			return Equals(objAsPart);
        }
        
        /// <summary>
        /// Compare to strings
        /// </summary>
        /// <param name="key1"></param>
        /// <param name="key2"></param>
        /// <returns></returns>
		public static int SortByNameAscending(string key1, string key2)
        {
			return String.Compare(key1, key2, false, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Default comparer for Part type. 
        /// </summary>
        /// <param name="comparePart"></param>
        /// <returns></returns>
        public int CompareTo(ListEntry comparePart)
        {
			int status = 0;

              // A null value means that this object is greater. 
			if (comparePart == null || this.Source.Equals(comparePart.Source))
				status = 1;

			return status;
        }

        /// <summary>
        /// Get Hash code
        /// </summary>
        /// <returns>hash code</returns>
        public override int GetHashCode()
        {
            return Source.GetHashCode();
        }

        /// <summary>
        /// Determine if one entry is equal to another
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(ListEntry other)
        {
            if (other == null) 
                return false;

            return (this.Source.Equals(other.Source));
        }
        // Should also override == and != operators.

        /// <summary>
        /// Gets the index by display name.
        /// </summary>
        /// <returns>The index.</returns>
        /// <param name="combobox">Combobox.</param>
        /// <param name="name">Name.</param>
        public static int GetIndexByName(ItemsControl combobox, string name)
        {
            bool found = false;
            int index = -1;

            foreach (ListEntry cmbItem in combobox.Items)
            {
                index++;

                if (cmbItem.DisplayName.Equals(name))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                index = -1;
            }

            return index;
        }

        /// <summary>
        /// Gets the index by source/value.
        /// </summary>
        /// <returns>The index.</returns>
        /// <param name="combobox">Combobox.</param>
        /// <param name="value">Value.</param>
        public static int GetIndexBySource(ItemsControl combobox, object value)
        {
            bool found = false;
            int index = -1;

            foreach (ListEntry cmbItem in combobox.Items)
            {
                index++;

                if (cmbItem.Source.Equals(value))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                index = -1;
            }

            return index;
        }
    }
}
