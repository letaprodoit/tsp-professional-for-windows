using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Windows;
using System.Windows.Data;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using TheProfessional.Library.Converters;

namespace TheProfessional.Library.Database
{
	/// <summary>
	/// Sql query type.
	/// </summary>
    public enum MySqlQueryType
    {
		/// <summary>
		/// A simple.
		/// </summary>
		[DefaultValue("SELECT {1} FROM {0} {5}")]
        Simple = 0,
		/// <summary>
		/// An ordered query.
		/// </summary>
		[DefaultValue("SELECT {1} FROM {0} {5} ORDER BY {2}")]
        Ordered = 1,
		/// <summary>
		/// A paged query with offset and limit.
		/// </summary>
		[DefaultValue("SELECT {1} FROM {0} {5} LIMIT {3}, {4}")]
        Paged = 2,
		/// <summary>
		/// A limited query
		/// </summary>
		[DefaultValue("SELECT {1} FROM {0} {5} LIMIT {4}")]
        Limited = 3,
		/// <summary>
		/// An ordered, paged query
		/// </summary>
		[DefaultValue("SELECT {1} FROM {0} {5} ORDER BY {2} LIMIT {3}, {4}")]
        OrderedPaged = 4,
		/// <summary>
		/// An ordered, limited query
		/// </summary>
		[DefaultValue("SELECT {1} FROM {0} {5} ORDER BY {2} LIMIT {4}")]
        OrderedLimited = 5,

        /// <summary>
        /// An ordered, limited query
        /// </summary>
        [DefaultValue("UPDATE {0} SET {1} WHERE {2}")]
        Update = 6,
    }

    /// <summary>
    /// Class for building a SQL query element
    /// </summary>
    public class SQLElement
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.Database.SQLElement"/> class.
		/// </summary>
        public SQLElement()
        {
        }

        private string[] _tables;
		/// <summary>
		/// Gets or sets the tables.
		/// </summary>
		/// <value>The tables.</value>
        public string[] Tables 
        {
            get
            {
                if (_tables == null)
                {
                    _tables = new string[] { };
                }
                return _tables;
            }
            set { _tables = value; }
        }

        private string[] _selects;
        /// <summary>
		/// Gets or sets the selects.
		/// </summary>
		/// <value>The selects.</value>
        public string[] Selects
        {
            get
            {
                if (_selects == null)
                {
                    _selects = new string[] { };
                }
                return _selects;
            }
            set { _selects = value; }
        }

        private string[] _joins;
        /// <summary>
		/// Gets or sets the joins.
		/// </summary>
		/// <value>The joins.</value>
        public string[] Joins
        {
            get
            {
                if (_joins == null)
                {
                    _joins = new string[] { };
                }
                return _joins;
            }
            set { _joins = value; }
        }

        private string[] _columns;
        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        /// <value>The fields.</value>
        public string[] Columns
        {
            get
            {
                if (_columns == null)
                {
                    _columns = new string[] { };
                }
                return _columns;
            }
            set { _columns = value; }
        }

        private object[] _values;
        /// <summary>
        /// Gets or sets the values.
        /// </summary>
        /// <value>The values.</value>
        public object[] Values
        {
            get
            {
                if (_values == null)
                {
                    _values = new object[] { };
                }
                return _values;
            }
            set { _values = value; }
        }

        private string[] _wheres;
        /// <summary>
        /// Gets or sets the where values.
        /// </summary>
        /// <value>The where values.</value>
        public string[] Wheres
        {
            get
            {
                if (_wheres == null)
                {
                    _wheres = new string[] { };
                }
                return _wheres;
            }
            set { _wheres = value; }
        }
        
        /// <summary>
		/// Gets or sets the order by.
		/// </summary>
		/// <value>The order by.</value>
        public string OrderBy { get; set; }

		/// <summary>
		/// Gets or sets the offset.
		/// </summary>
		/// <value>The offset.</value>
        public int Offset { get; set; }

		/// <summary>
		/// Gets or sets the limit.
		/// </summary>
		/// <value>The limit.</value>
        public int Limit { get; set; }
    }

	/// <summary>
	/// Database manager.
	/// </summary>
	public class DatabaseManager : IDisposable
	{
        /// <summary>
        /// The _disposed.
        /// </summary>
        bool _disposed;

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Library.Database.DatabaseManager"/> class.
		/// </summary>
		/// <param name="connectionString">Connection string.</param>
		/// <param name="dataSet">Data set.</param>
        public DatabaseManager(string connectionString, DataSet dataSet)
        {
            ConnectionString = connectionString;
            ReadDataSet = dataSet;
        }

		#region Properties
		private static MySqlConnection _connection;
        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        /// <value>The connection.</value>
        public static MySqlConnection Connection
		{
			set { _connection = value; }
			get
			{
				if ( _connection == null )
				{
					_connection = MakeConnection ();
				}
				return _connection;
			}
		}

        /// <summary>
        /// The connection string.
        /// </summary>
        // TODO: User supplies connection settings in initial application setup
		public static string ConnectionString { get; set; }


        /// <summary>
        /// Reads the data set.
        /// </summary>
        /// <returns>The data set.</returns>
		public DataSet ReadDataSet { get; set; }
		#endregion

		#region IDisposable Implementation: Methods
        
		/// <summary>
        /// Releases all resource used by the <see cref="TheProfessional.Library.Database.DatabaseManager"/> object.
		/// </summary>
        /// <remarks>Call <see cref="TheProfessional.Library.Database.DatabaseManager.Dispose()"/> when you are finished using the <see cref="TheProfessional.Library.Database.DatabaseManager"/>.
        /// The <see cref="TheProfessional.Library.Database.DatabaseManager.Dispose()"/> method leaves the <see cref="TheProfessional.Library.Database.DatabaseManager"/> in an unusable
        /// state. After calling <see cref="TheProfessional.Library.Database.DatabaseManager.Dispose()"/>, you must release all references to the
        /// <see cref="TheProfessional.Library.Database.DatabaseManager"/> so the garbage collector can reclaim the memory that the
        /// <see cref="TheProfessional.Library.Database.DatabaseManager"/> was occupying.</remarks>
		public void Dispose()
        {
			Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="TheProfessional.Library.Database.DatabaseManager"/> is reclaimed by garbage collection.
        /// </summary>
        ~DatabaseManager()
        {
            Dispose(false);
        }

        /// <summary>
        /// Dispose the specified disposing.
        /// </summary>
        /// <param name="disposing">If set to <c>true</c> disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }
                if (ReadDataSet != null)
                {
                    ReadDataSet.Dispose();
                    ReadDataSet = null;
                }
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
        #endregion

        #region Public Methods
        /// <summary>
		/// Load this instance.
		/// </summary>
		public void Load()
		{
			Connection = MakeConnection ();
		}


		/// <summary>
		/// Reads the data table
		/// </summary>
		/// <param name="table">Table.</param>
		/// <returns>The data table.</returns>
		public DataTable ReadDataTable( string table )
		{
			if (ReadDataSet == null) return null;
            return ReadDataSet.Tables [ table ];
		}


		/// <summary>
		/// Reads the data view
		/// </summary>
		/// <param name="table">Table.</param>
		/// <returns>The data view.</returns>
		public DataView ReadDataView( string table )
		{
            if (ReadDataSet == null) return null;
            return ReadDataSet.Tables[table].AsDataView();
		}


		/// <summary>
		/// Connection this instance.
		/// </summary>
		public static MySqlConnection MakeConnection()
		{
            MySqlConnection myConnection = new MySqlConnection();

            if (!String.IsNullOrEmpty(ConnectionString))
            {
                // Create a connection to the "pubs" SQL database located on the 
                // local computer.
                myConnection.ConnectionString = ConnectionString;
            }

			return myConnection;
		}


		/// <summary>
		/// Gets the data table from data reader.
		/// </summary>
		/// <returns>The data table from data reader.</returns>
		/// <param name="dataReader">Data reader.</param>
		public DataTable GetDataTableFromDataReader( IDataReader dataReader )
		{
			DataTable schemaTable = dataReader.GetSchemaTable ();
            schemaTable.Locale = CultureInfo.CurrentCulture;
            
            DataTable resultTable = new DataTable();
            resultTable.Locale = CultureInfo.CurrentCulture;

			foreach ( DataRow dataRow in schemaTable.Rows )
			{
				DataColumn dataColumn = new DataColumn ();
				dataColumn.ColumnName = dataRow [ "ColumnName" ].ToString ();
				dataColumn.DataType = Type.GetType ( dataRow [ "DataType" ].ToString () );
				dataColumn.ReadOnly = ( bool )dataRow [ "IsReadOnly" ];
				dataColumn.AutoIncrement = ( bool )dataRow [ "IsAutoIncrement" ];
				dataColumn.Unique = ( bool )dataRow [ "IsUnique" ];

				resultTable.Columns.Add ( dataColumn );
                dataColumn.Dispose();
            }
            schemaTable.Dispose();

			while ( dataReader.Read () )
			{
				DataRow dataRow = resultTable.NewRow ();
				for ( int i = 0; i < resultTable.Columns.Count; i++ )
				{
					dataRow [ i ] = dataReader [ i ];
				}
				resultTable.Rows.Add ( dataRow );
			}

			return resultTable;
		}


		/// <summary>
		/// Gets the data table by adapter.
		/// </summary>
		/// <returns>The data table by adapter.</returns>
		/// <param name="table">Table.</param>
		/// <param name="query">Query.</param>
		public DataTable GetDataTableByAdapter( string table, string query )
		{
			DataTable resultTable = new DataTable ();
            resultTable.Locale = CultureInfo.CurrentCulture;

			try
			{
				Connection.Open ();

				resultTable = ReadDataTable ( table );
				resultTable.Rows.Clear ();
                
				DateTime start = DateTime.Now;
				MySqlDataAdapter adapter = new MySqlDataAdapter ( query, Connection );
                adapter.Fill(resultTable);
                adapter.Dispose();
                TimeSpan ts = DateTime.Now.Subtract(start);
                Trace.Write("Time Elapsed in GetDataTableByAdapter: " + ts.TotalMilliseconds + "\n");
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
            finally
			{
				Connection.Close ();
			}
			return resultTable;
		}


		/// <summary>
		/// Gets the datatable with query only.
		/// </summary>
		/// <returns>The data table fast.</returns>
		/// <param name="query">Query.</param>
		public DataTable GetDataTableFast( string query )
		{
			DataTable resultTable = new DataTable ();
            resultTable.Locale = CultureInfo.CurrentCulture;

			try
			{
				Connection.Open ();

				DateTime start = DateTime.Now;

                MySqlCommand command = new MySqlCommand(query, Connection);
				IDataReader rdr = command.ExecuteReader();
				resultTable = GetDataTableFromDataReader ( rdr );
                rdr.Dispose();
                command.Dispose();
                
                TimeSpan ts = DateTime.Now.Subtract(start);
                Trace.Write("Time Elapsed in GetDataTableFast : " + ts.TotalMilliseconds + "\n");
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
            finally
			{
				Connection.Close ();
			}

			return resultTable;
		}

        /// <summary>
        /// Updates the data table with query only.
        /// </summary>
        /// <returns>The data table fast.</returns>
        /// <param name="query">Query.</param>
        public bool UpdateTableFast(string query)
        {
            bool recordModified = false;

            try
            {
                Connection.Open();

                DateTime start = DateTime.Now;

                MySqlCommand command = new MySqlCommand(query, Connection);

                recordModified = command.ExecuteNonQuery() == 1;
                command.Dispose();

                TimeSpan ts = DateTime.Now.Subtract(start);
                Trace.Write("Time Elapsed in UpdateTableFast : " + ts.TotalMilliseconds + "\n");
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
            finally
            {
                Connection.Close();
            }

            return recordModified;
        }

		/// <summary>
		/// Builds the complex query.
		/// </summary>
		/// <returns>The complex query.</returns>
		/// <param name="elem">Element.</param>
		public string QueryBuilder( SQLElement elem )
		{
			MySqlQueryType type = MySqlQueryType.Simple;
            bool update = false;

            if (elem.Columns.Any() && elem.Values.Any())
            {
                type = MySqlQueryType.Update;
                update = true;
            }
            else
            {
                if (!elem.Selects.Any())
                {
                    elem.Selects = new[] { "*" };
                }

                if (elem.Limit != -1 && elem.Offset != -1)
                {
                    type = MySqlQueryType.Paged;
                }
                else if (elem.Limit != -1 && elem.Offset == -1)
                {
                    type = MySqlQueryType.Limited;

                }
                // If we are sorting the records
                if (!String.IsNullOrEmpty(elem.OrderBy))
                {
                    switch (type)
                    {
                        case MySqlQueryType.Limited:
                            type = MySqlQueryType.OrderedLimited;
                            break;
                        case MySqlQueryType.Paged:
                            type = MySqlQueryType.OrderedPaged;
                            break;
                        default:
                            type = MySqlQueryType.Ordered;
                            break;
                    }
                }
            }

			string sql = EnumToValueConverter.GetDefaultValue(type);

            if (update)
            {
                // TODO? Implement dynamic updates ?
                //UPDATE {0} SET {1} WHERE {2}
            }
            else
            {
                sql = String.Format(sql, String.Join(",", elem.Tables),
                    String.Join(",", elem.Selects),
                    elem.OrderBy,
                    elem.Offset,
                    elem.Limit,
                    String.Join(" ", elem.Joins));
            }

			return sql;
		}

		#endregion
    }
}
