﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheProfessional.Library
{
    /// <summary>
    /// String helper class
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Convert EOL chars in a given string
        /// </summary>
        /// <param name="item">string to be evaluated</param>
        /// <returns>newly formatted string</returns>
        public static string ConvertEOLs(this string item)
        {
            item = item.Replace(@"\r\n", Environment.NewLine);
            item = item.Replace(@"\n", Environment.NewLine);

            return item;
        }

        /// <summary>
        /// Capitalize the first letter of every word
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string Capitalize(this string item)
        {
            // split string into words
            string[] words = item.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            // capitalize the first letter of every word
            for (int i = 0; i < words.Count(); i++)
            {
                if (words[i].Count() > 1)
                {
                    words[i] = words[i][0].ToString().ToUpperInvariant() + words[i].Substring(1);
                }
                else
                {
                    words[i] = words[i].ToUpperInvariant();
                }
            }

            // join string back with spaces
            item = String.Join(" ", words);

            return item;
        }

        /// <summary>
        /// Truncate a string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string Truncate(this string value, int maxLength = 25)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength) + "...";
        }
    }
}
