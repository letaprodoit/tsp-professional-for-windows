﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Globalization;
using System.Windows.Media.Imaging;

namespace TheProfessional.Library
{
	/// <summary>
	/// Global funcs.
	/// </summary>
    public static class LibraryFuncs
    {
        /// <summary>
        /// Update the avatar image via path or gravatar email
        /// <param name="source">Param source: The source of the avatar image, either path or email</param>
        /// <param name="isPath">Param isPath: If the source is not a path, set isPath to false. Default is true.</param>
        /// <returns>Returns: Bitmap image</returns>
        /// </summary>
        public static BitmapImage SetAvatarImage(String source, Boolean isPath = true)
        {
            // Create source.
            BitmapImage bi = new BitmapImage();

            // BitmapImage.UriSource must be in a BeginInit/EndInit block.
            bi.BeginInit();

            if (isPath)
            {
                bi.UriSource = new Uri(source, UriKind.RelativeOrAbsolute);
            }
            else
            {
                Gravatar avatar = new Gravatar();
                String avatarURL = avatar.GetImageSource(source);
                bi.UriSource = new Uri(avatarURL.ToLower());
            }

            bi.EndInit();

            return bi;
        }
    }
}
