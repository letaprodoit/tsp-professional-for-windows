﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Library
{
    /// <summary>
    /// Address class
    /// </summary>
    public class Address : ObservableObject
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.Address"/> class.
		/// </summary>
        public Address()
		{
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Library.Address"/> class.
        /// </summary>
        /// <param name="address1"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zip_code"></param>
        /// <param name="email"></param>
        public Address(string address1, string city, int state, string zip_code, string email)
            : this()
		{
            if (String.IsNullOrEmpty(address1))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, LibraryProperties.Resources.Header_Address1));
            }
            if (String.IsNullOrEmpty(city))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, LibraryProperties.Resources.Header_City));
            }
            if (String.IsNullOrEmpty(zip_code))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, LibraryProperties.Resources.Header_ZipCode));
            }
            if (String.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, LibraryProperties.Resources.Header_Email));
            }
            try
            {
                Address1 = address1;
                City = city;
                State = state;
                ZipCode = zip_code;
                Email = email;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

        #region Properties
        private string _address1;
        /// <summary>
        /// The address.
        /// </summary>
        [Column(Title = LibraryProperties.Resources.Header_Address1)]
        public string Address1
        {
            get { return _address1; }
            set
            {
                if (value != _address1)
                {
                    _address1 = value;
                    RaisePropertyChanged("Address1");
                }
            }
        }

        private string _address2;
        /// <summary>
        /// The address2.
        /// </summary>
        [Column(Title = LibraryProperties.Resources.Header_Address2)]
        public string Address2
        {
            get { return _address2; }
            set
            {
                if (value != _address2)
                {
                    _address2 = value;
                    RaisePropertyChanged("Address2");
                }
            }
        }

        private string _city;
        /// <summary>
        /// The city.
        /// </summary>
        [Column(Title = LibraryProperties.Resources.Header_City)]
        public string City
        {
            get { return _city; }
            set
            {
                if (value != _city)
                {
                    _city = value;
                    RaisePropertyChanged("City");
                }
            }
        }

        private int _state;
        /// <summary>
        /// The state.
        /// </summary>
        [Column(ForeignKey = true, Title = LibraryProperties.Resources.Header_State)]
        public int State
        {
            get { return _state; }
            set
            {
                if (value != _state)
                {
                    _state = value;
                    RaisePropertyChanged("State");
                }
            }
        }

        private string _zipCode;
        /// <summary>
        /// The zip code.
        /// </summary>
        [Column(Title = LibraryProperties.Resources.Header_ZipCode)]
        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                if (value != _zipCode)
                {
                    _zipCode = value;
                    RaisePropertyChanged("ZipCode");
                }
            }
        }

        private string _phone1;
        /// <summary>
        /// The email.
        /// </summary>
        [Column(Field = FieldType.Phone, Title = LibraryProperties.Resources.Header_Phone1)]
        public string Phone1
        {
            get { return _phone1; }
            set
            {
                if (value != _phone1)
                {
                    _phone1 = value;
                    RaisePropertyChanged("Phone1");
                }
            }
        }

        private string _phone2;
        /// <summary>
        /// The email.
        /// </summary>
        [Column(Field = FieldType.Phone, Title = LibraryProperties.Resources.Header_Phone2)]
        public string Phone2
        {
            get { return _phone2; }
            set
            {
                if (value != _phone2)
                {
                    _phone2 = value;
                    RaisePropertyChanged("Phone2");
                }
            }
        }

        private string _fax;
        /// <summary>
        /// The email.
        /// </summary>
        [Column(Field = FieldType.Phone, Title = LibraryProperties.Resources.Header_Fax)]
        public string Fax
        {
            get { return _fax; }
            set
            {
                if (value != _fax)
                {
                    _fax = value;
                    RaisePropertyChanged("Fax");
                }
            }
        }

        private string _email;
        /// <summary>
        /// The email.
        /// </summary>
        [Column(Field = FieldType.Email, Title = LibraryProperties.Resources.Header_Email)]
        public string Email
        {
            get { return _email; }
            set
            {
                if (value != _email)
                {
                    _email = value;
                    RaisePropertyChanged("Email");
                }
            }
        }

        private string _url;
        /// <summary>
        /// The description.
        /// </summary>
        [Column(Field = FieldType.URL, Title = LibraryProperties.Resources.Header_URL)]
        public string URL
        {
            get { return _url; }
            set
            {
                if (value != _url)
                {
                    _url = value;
                    RaisePropertyChanged("URL");
                }
            }
        }
        #endregion
    }
}
