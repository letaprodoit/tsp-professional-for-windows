﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheProfessional.Library
{
	/// <summary>
	/// Page info.
	/// </summary>
    public class PageInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PageInfo"/> class.
        /// </summary>
        /// <param name="offset">The row offset.</param>
        /// <param name="limit">The max number or rows to return.</param>
		public PageInfo(int offset, int limit)
        {
            this.Offset = offset;
            this.Limit = limit;
        }

		/// <summary>
		/// Gets or sets the start index.
		/// </summary>
		/// <value>The start index.</value>
        public int Offset { get; set; }

		/// <summary>
		/// Gets or sets the end index.
		/// </summary>
		/// <value>The end index.</value>
        public int Limit { get; set; }
    }
}
