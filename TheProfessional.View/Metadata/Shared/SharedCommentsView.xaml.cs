﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using TheProfessional.ViewModel;

namespace TheProfessional.View.Metadata.Shared
{
    /// <summary>
    /// Interaction logic for SharedCommentsView.xaml
    /// </summary>
    public partial class SharedCommentsView : DynamicView<SharedCommentsViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Metadata.Shared.SharedCommentsView"/> class.
        /// </summary>
        public SharedCommentsView()
        {
            InitializeComponent();
        }
    }
}
