﻿//using Syncfusion.Windows.Controls.Grid;
//using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;

namespace TheProfessional.View.Metadata.Application
{
    /// <summary>
    /// Interaction logic for LaunchView.xaml
    /// </summary>
    public partial class LaunchView : StaticView
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Metadata.Application.LaunchView"/> class.
		/// </summary>
        public LaunchView()
        {
            InitializeControls();
            InitializeComponent();
        }

        /// <summary>
        /// Sets the style.
        /// </summary>
        private void InitializeControls()
        {
            MinOnExit = ViewProperties.MinOnExit;
            AutoStartDay = ViewProperties.AutoStartDay;
            WinAutoStart = ViewProperties.WinAutoStart;
            TimeCardOnEndDay = ViewProperties.TimeCardOnEndDay;
        }

        private bool _minOnExit;
        /// <summary>
        /// Get/Set Minimize on exit
        /// </summary>
        public bool MinOnExit
        {
            get { return _minOnExit; }
            set
            {
                if (_minOnExit != value)
                {
                    _minOnExit = value;
                    ViewProperties.MinOnExit = value;
                }
            }
        }

        private bool _autoStartDay;
        /// <summary>
        /// Get/Set automatically start day
        /// </summary>
        public bool AutoStartDay
        {
            get { return _autoStartDay; }
            set
            {
                if (_autoStartDay != value)
                {
                    _autoStartDay = value;
                    ViewProperties.AutoStartDay = value;
                }
            }
        }

        private bool _winAutoStart;
        /// <summary>
        /// Get/Set automatically start application on windows startup
        /// </summary>
        public bool WinAutoStart
        {
            get { return _winAutoStart; }
            set
            {
                if (_winAutoStart != value)
                {
                    _winAutoStart = value;
                    ViewProperties.WinAutoStart = value;
                }
            }
        }

        private bool _timeCardOnEndDay;
        /// <summary>
        /// Get/Set automatically complete timecard at end of day
        /// </summary>
        public bool TimeCardOnEndDay
        {
            get { return _timeCardOnEndDay; }
            set
            {
                if (_timeCardOnEndDay != value)
                {
                    _timeCardOnEndDay = value;
                    ViewProperties.TimeCardOnEndDay = value;
                }
            }
        }
    }
}
