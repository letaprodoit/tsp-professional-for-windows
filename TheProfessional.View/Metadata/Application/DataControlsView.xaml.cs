﻿//using Syncfusion.Windows.Controls.Grid;
//using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;

namespace TheProfessional.View.Metadata.Application
{
    /// <summary>
    /// Interaction logic for DataControlsView.xaml
    /// </summary>
    public partial class DataControlsView : StaticView
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Metadata.Application.DataControlsView"/> class.
		/// </summary>
        public DataControlsView()
        {
            InitializeControls();
            InitializeComponent();

            this.Loaded +=DataControls_Loaded;
        }

        /// <summary>
        /// Initialize data controls every time the control has focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataControls_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeControls();
        }

        /// <summary>
        /// Sets the style.
        /// </summary>
        private void InitializeControls()
        {
            // make sure selected datagrid style is up-to-date
            SelectedDataGridStyle = DataGridStyles.FirstOrDefault(Item => Item.Source.Equals(ViewProperties.DataGridStyle));

            // make sure selected page size is up-to-date
            SelectedGridPageSize = ViewProperties.GridPageSize;

            GridRowHeight = ViewProperties.GridRowHeight;
        }


		private List<ListEntry> _dataGridStyles;
		/// <summary>
		/// The DataGrid's styles.
		/// </summary>
		public List<ListEntry> DataGridStyles
		{
			get
			{
				if (_dataGridStyles == null)
				{
					_dataGridStyles = new List<ListEntry>();

					// add the default themes
					_dataGridStyles.Add(new ListEntry { DisplayName = Properties.Resources.DataGrid_Theme_Light, Source = ViewProperties.DataGridLightTheme });
                    _dataGridStyles.Add(new ListEntry { DisplayName = Properties.Resources.DataGrid_Theme_Dark, Source = ViewProperties.DataGridDarkTheme });
                    _dataGridStyles.Add(new ListEntry { DisplayName = Properties.Resources.DataGrid_Theme_ExpressionLight, Source = ViewProperties.DataGridExpressionLightTheme });
                    _dataGridStyles.Add(new ListEntry { DisplayName = Properties.Resources.DataGrid_Theme_ExpressionDark, Source = ViewProperties.DataGridExpressionDarkTheme });
                    _dataGridStyles.Add(new ListEntry { DisplayName = Properties.Resources.DataGrid_Theme_WhislerBlue, Source = ViewProperties.DataGridWhistlerBlueTheme });
				}
				return _dataGridStyles;
			}
		}

		private ListEntry _selectedDataGridStyle;
		/// <summary>
		/// The selected DataGrid's style.
		/// </summary>
		public ListEntry SelectedDataGridStyle
		{
			get { return _selectedDataGridStyle; }
			set
			{
				if (_selectedDataGridStyle != value)
				{
					_selectedDataGridStyle = value;
					ViewProperties.DataGridStyle = (Uri)value.Source;
				}
			}
		}
			

        /// <summary>
        /// The available grid page sizes (the number of records on one page).
        /// </summary>
		public static int[] GridPageSizes
        {
            get { return new [] { 10, 15, 25, 50, 75, 100 }; }
        }


        private int _selectedGridPageSize;
        /// <summary>
        /// The selected grid page size.
        /// </summary>
        public int SelectedGridPageSize
        {
            get { return _selectedGridPageSize; }
            set
            {
				if (!_selectedGridPageSize.Equals(value))
                {
                    _selectedGridPageSize = value;
                    ViewProperties.GridPageSize = value;
                }
            }
        }
        
        private double _gridRowHeight;
        /// <summary>
        /// The selected grid row height.
        /// </summary>
        public double GridRowHeight
        {
            get { return _gridRowHeight; }
            set
            {
				if (!_gridRowHeight.Equals(value))
                {
                    _gridRowHeight = value;
                    ViewProperties.GridRowHeight = value;
                }
            }
        }
    }
}
