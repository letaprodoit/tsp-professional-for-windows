﻿//using Syncfusion.Windows.Controls.Grid;
//using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;

namespace TheProfessional.View.Metadata.Application
{
    /// <summary>
    /// Interaction logic for UploadsAttachmentsView.xaml
    /// </summary>
    public partial class UploadsAttachmentsView : StaticView
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Metadata.Application.UploadsAttachmentsView"/> class.
		/// </summary>
        public UploadsAttachmentsView()
        {
            InitializeControls();
            InitializeComponent();
        }

        /// <summary>
        /// Sets the style.
        /// </summary>
        private void InitializeControls()
        {
            UploadsLocation = ViewProperties.UploadsLocation;
        }

        private string _uploadsLocation;
        /// <summary>
        /// The uploads directory
        /// </summary>
        public string UploadsLocation
        {
            get { return _uploadsLocation; }
            set
            {
                if (_uploadsLocation != value)
                {
                    _uploadsLocation = value;
                    ViewProperties.UploadsLocation = value;
                }
            }
        }
    }
}
