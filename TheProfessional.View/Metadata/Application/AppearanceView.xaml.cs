﻿using FirstFloor.ModernUI.Presentation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheProfessional.Library;

namespace TheProfessional.View.Metadata.Application
{
    /// <summary>
    /// Interaction logic for AppearanceView.xaml
    /// </summary>
    public partial class AppearanceView : StaticView
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Metadata.Application.AppearanceView"/> class.
		/// </summary>
        public AppearanceView()
        {
            InitializeControls();
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Metadata.Application.AppearanceView"/> class.
        /// </summary>
        public void InitializeControls()
        {
            SyncTheme();

            AppearanceManager.Current.PropertyChanged += OnAppearanceManagerPropertyChanged;
        }

        /// <summary>
        /// Syncs the color of the theme and accent color.
        /// </summary>
        private void SyncTheme()
        {
            // synchronizes the selected fontsize theme with the actual fontsize used by the appearance manager.
            SelectedFontSize = FontSizes.FirstOrDefault(Item => Item.Source.Equals(AppearanceManager.Current.FontSize));
            
            // synchronizes the selected theme with the actual theme used by the appearance manager.
            SelectedTheme = Themes.FirstOrDefault(Item => Item.Source.Equals(AppearanceManager.Current.ThemeSource));

            // and make sure accent color is up-to-date
            SelectedAccentColor = AppearanceManager.Current.AccentColor;
        }

        /// <summary>
        /// Raises the appearance manager property changed event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OnAppearanceManagerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ThemeSource" || e.PropertyName == "AccentColor" || e.PropertyName == "FontSize")
            {
                SyncTheme();
            }
        }

        // 9 accent colors from metro design principles
		/*private readonly Color[] _accentColors = {
            Color.FromRgb(0x33, 0x99, 0xff),   // blue
            Color.FromRgb(0x00, 0xab, 0xa9),   // teal
            Color.FromRgb(0x33, 0x99, 0x33),   // green
            Color.FromRgb(0x8c, 0xbf, 0x26),   // lime
            Color.FromRgb(0xf0, 0x96, 0x09),   // orange
            Color.FromRgb(0xff, 0x45, 0x00),   // orange red
            Color.FromRgb(0xe5, 0x14, 0x00),   // red
            Color.FromRgb(0xff, 0x00, 0x97),   // magenta
            Color.FromRgb(0xa2, 0x00, 0xff),   // purple            
        };*/

        // 20 accent colors from Windows Phone 8
		private readonly Color[] _accentColors = {
            Color.FromRgb(0xa4, 0xc4, 0x00),   // lime
            Color.FromRgb(0x60, 0xa9, 0x17),   // green
            Color.FromRgb(0x00, 0x8a, 0x00),   // emerald
            Color.FromRgb(0x00, 0xab, 0xa9),   // teal
            Color.FromRgb(0x1b, 0xa1, 0xe2),   // cyan
            Color.FromRgb(0x00, 0x50, 0xef),   // cobalt
            Color.FromRgb(0x6a, 0x00, 0xff),   // indigo
            Color.FromRgb(0xaa, 0x00, 0xff),   // violet
            Color.FromRgb(0xf4, 0x72, 0xd0),   // pink
            Color.FromRgb(0xd8, 0x00, 0x73),   // magenta
            Color.FromRgb(0xa2, 0x00, 0x25),   // crimson
            Color.FromRgb(0xe5, 0x14, 0x00),   // red
            Color.FromRgb(0xfa, 0x68, 0x00),   // orange
            Color.FromRgb(0xf0, 0xa3, 0x0a),   // amber
            Color.FromRgb(0xe3, 0xc8, 0x00),   // yellow
            Color.FromRgb(0x82, 0x5a, 0x2c),   // brown
            Color.FromRgb(0x6d, 0x87, 0x64),   // olive
            Color.FromRgb(0x64, 0x76, 0x87),   // steel
            Color.FromRgb(0x76, 0x60, 0x8a),   // mauve
            Color.FromRgb(0x87, 0x79, 0x4e),   // taupe
        };

        /// <summary>
        /// Gets the accent colors.
        /// </summary>
        /// <value>The accent colors.</value>
        public Color[] AccentColors
        {
            get { return _accentColors; }
        }

        private Color _selectedAccentColor;
        /// <summary>
        /// The color of the selected accent.
        /// </summary>
        public Color SelectedAccentColor
        {
            get { return _selectedAccentColor; }
            set
            {
                if (_selectedAccentColor != value)
                {
                    _selectedAccentColor = value;
                    ViewProperties.AppAccentColor = value;
                }
            }
        }

        private List<ListEntry> _themes;
        /// <summary>
        /// The themes.
        /// </summary>
        public List<ListEntry> Themes
        {
            get
            {
                if (_themes == null)
                {
                    _themes = new List<ListEntry>();

                    // add the default themes
                    _themes.Add(new ListEntry { DisplayName = Properties.Resources.App_Theme_Light, Source = ViewProperties.AppLightTheme });
                    _themes.Add(new ListEntry { DisplayName = Properties.Resources.App_Theme_Dark, Source = ViewProperties.AppDarkTheme });
                }
                return _themes;
            }
        }

        private ListEntry _selectedTheme;
        /// <summary>
        /// The selected theme.
        /// </summary>
        public ListEntry SelectedTheme
        {
            get { return _selectedTheme; }
            set
            {
                if (_selectedTheme != value && value != null)
                {
                    _selectedTheme = value;
                    ViewProperties.AppThemeSource = (Uri)value.Source;
                }
            }
        }

        private List<ListEntry> _fontSizes;
        /// <summary>
        /// Gets the font sizes.
        /// </summary>
        /// <value>The font sizes.</value>
        public List<ListEntry> FontSizes
        {
            get
            {
                if (_fontSizes == null)
                {
                    _fontSizes = new List<ListEntry>();

                    // add the default font sizes
                    _fontSizes.Add(new ListEntry { DisplayName = "Small", Source = FirstFloor.ModernUI.Presentation.FontSize.Small });
                    _fontSizes.Add(new ListEntry { DisplayName = "Large", Source = FirstFloor.ModernUI.Presentation.FontSize.Large });
                }
                return _fontSizes;
            }
        }

        private ListEntry _selectedFontSize;
        /// <summary>
        /// The size of the selected font.
        /// </summary>
        public ListEntry SelectedFontSize
        {
            get { return _selectedFontSize; }
            set
            {
                if (_selectedFontSize != value)
                {
                    _selectedFontSize = value;
                    ViewProperties.AppFontSize = (FontSize)value.Source;
                }
            }
        }
    }
}
