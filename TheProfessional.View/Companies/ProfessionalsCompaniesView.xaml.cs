﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using TheProfessional.Library;
using TheProfessional.View.Assets.Controls.Grids;
using TheProfessional.View.Assets.Dialogs;
using TheProfessional.ViewModel;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.View.Users.Metadata
{
    /// <summary>
    /// Interaction logic for ProfessionalsCompaniesView.xaml
    /// </summary>
    public partial class ProfessionalsCompaniesView : DynamicView<ProfessionalsCompaniesViewModel>
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Users.Metadata.ProfessionalsCompaniesView"/> class.
		/// </summary>
        public ProfessionalsCompaniesView()
        {
            InitializeComponent();
        }
    }
}
