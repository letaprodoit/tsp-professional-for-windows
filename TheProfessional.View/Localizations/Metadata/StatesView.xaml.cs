﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using TheProfessional.ViewModel;

namespace TheProfessional.View.Localizations.Metadata
{
    /// <summary>
    /// Interaction logic for StatesView.xaml
    /// </summary>
    public partial class StatesView : DynamicView<StatesViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Localizations.Metadata.StatesView"/> class.
        /// </summary>
        public StatesView()
        {
            InitializeComponent();
        }
    }
}
