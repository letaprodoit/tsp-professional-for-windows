﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows.Controls;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using TheProfessional.View.Assets.Controls;
using TheProfessional.View.Assets.Controls.Grids;
using TheProfessional.View.Assets.Dialogs;
using System.Globalization;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.View.Localizations.Metadata
{
    /// <summary>
    /// Interaction logic for CountriesView.xaml
    /// </summary>
    public partial class CountriesView : DynamicView<CountriesViewModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Localizations.Metadata.CountriesView"/> class.
        /// </summary>
        public CountriesView()
        {
            InitializeComponent();
        }
    }
}
