using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using TheProfessional.View.Assets.Controls;
using System.Data;
using System.Globalization;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.View.Assets.Dialogs
{
	/// <summary>
	/// Data dialog type.
	/// </summary>
    public enum DataDialogType
    {
        /// <summary>
        /// Empty enum
        /// </summary>
        [DefaultValue("")]
        None = 0,
        /// <summary>
		/// The Dialog is for editing records.
		/// </summary>
        [DefaultValue("Edit")]
        EDIT = 1,
		/// <summary>
		/// The Dialog is for adding new records.
		/// </summary>
        [DefaultValue("Add")]
        ADD = 2,
    }

    /// <summary>
	/// Interaction logic for DataDialog.xaml
	/// </summary>
    public partial class DataDialog : ModernDialog
	{
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Dialogs.DataDialog"/> class.
		/// </summary>
		public DataDialog()
		{
			InitializeComponent ();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Dialogs.DataDialog"/> class.
		/// </summary>
		/// <param name="record">Record.</param>
        /// <param name="recordTable">The parent relationships.</param>
        /// <param name="dialogType">The type of the dialog.</param>
        /// <param name="instructions">Instructions.</param>
        public DataDialog(ref object record, DataTable recordTable, DataDialogType dialogType, string instructions) : this()
		{
			Record = record;
            DialogType = dialogType;
			Instructions = instructions;
            RecordTable = recordTable;
        }

        /// <summary>
        /// Adds buttons to the dialog based on the record type
        /// </summary>
        public void ConfigureDialog()
        {
            ObservableCollection<Button> buttonList = new ObservableCollection<Button>();

            // Add the necessary buttons to the dialog based on type
            if (DialogType == DataDialogType.EDIT)
            {

                Button saveButton = new Button();
                saveButton.Margin = new Thickness(0, 0, 10, 0);
                saveButton.Content = LibraryProperties.Resources.LabelName_SAVE;
                saveButton.Click += VerifyRecord;
                saveButton.IsDefault = true;
                buttonList.Add(saveButton); // Save

                this.Title = LibraryProperties.Resources.LabelName_TITLE_EDIT;
            }
            else if (DialogType == DataDialogType.ADD)
            {
                Button saveButton = new Button();
                saveButton.Margin = new Thickness(0, 0, 10, 0);
                saveButton.Content = LibraryProperties.Resources.LabelName_SAVE;
                saveButton.Click += VerifyRecord;
                saveButton.IsDefault = true;
                buttonList.Add(saveButton); // Add

                this.Title = LibraryProperties.Resources.LabelName_TITLE_ADD;
            }

            Button cancelButton = new Button();
            cancelButton.Content = LibraryProperties.Resources.LabelName_CANCEL;
            cancelButton.IsCancel = true;
            buttonList.Add(cancelButton); // Cancel

			this.Buttons = buttonList;
        }
       
        /// <summary>
		/// Populates the grid with form fields
		/// </summary>
        public void PopulateGrid()
		{
			try
			{
                AddFieldsToGrid();
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        private void AddFieldsToGrid()
        {
            int rowIndex = 0;

            PropertyInfo[] properties = Record.GetType().GetProperties();
            foreach (PropertyInfo prop in properties)
            {
                bool fieldEnabled = true;

                // skip processing of the properties that should be ignored
                if (prop.GetCustomAttributes(typeof(IgnoreAttribute), true).Any())
                {
                    continue;
                }

                ColumnAttribute columnAtt = (ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).GetValue(0);

                // records that are both a primary key and a foreign key can not be edited
                // auto increment and readonly fields can not be edited
                if ((columnAtt.PrimaryKey && columnAtt.ForeignKey) || columnAtt.AutoIncrement || columnAtt.ReadOnly)
                {
                    fieldEnabled = false;
                }

                // skip processing of the properties that are autoincremented
                if (columnAtt.AutoIncrement && DialogType == DataDialogType.ADD)
                {
                    continue;
                }

                Type type = prop.PropertyType;
                String title = prop.Name;
                String name = title.ToLower(CultureInfo.CurrentCulture);
                object value = null;

                // if the property has an alternate mapping name use it instead
                // of the property name itself
                if (!String.IsNullOrEmpty(columnAtt.Name))
                {
                    name = columnAtt.Name;
                }

                // if the property has an alternate title use it instead
                // of the property name itself
                if (!String.IsNullOrEmpty(columnAtt.Title))
                {
                    title = columnAtt.Title;
                }

                object propValue = prop.GetValue(Record, null);
                if (propValue != null)
                {
                    value = propValue;
                }

                // Set binding to bind the Record to the controls dynamically
                Binding myBinding = new Binding();
                myBinding.Source = Record;
                myBinding.Path = new PropertyPath(prop.Name);
                myBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                myBinding.Mode = BindingMode.TwoWay;

                // if we are adding a record do not show the default values of the
                // initial record and only update the source as the user types in values
                if (DialogType == DataDialogType.ADD)
                {
                    myBinding.Mode = BindingMode.OneWayToSource;
                }

                // Create the label
                TextBlock textBlock = new TextBlock();
                textBlock.Text = title;
                textBlock.Margin = new Thickness(0, 0, 16, 14);
                textBlock.HorizontalAlignment = HorizontalAlignment.Left;

                if (columnAtt.ForeignKey)
                {
                    ListEntry filter = new ListEntry();

                    // skip processing of the properties that should be ignored
                    if (prop.GetCustomAttributes(typeof(FilterAttribute), true).Any())
                    {
                        FilterAttribute filterAtt = (FilterAttribute)prop.GetCustomAttributes(typeof(FilterAttribute), true).GetValue(0);
                        filter.DisplayName = filterAtt.Field;
                        filter.Source = filterAtt.Value;
                    }

                    List<ListEntry> fkList = Reflect.GetForeignKeyPropertiesCollection(RecordTable, name, filter);

                    // If the list returned items then this is a combo box
                    // else set box to textbox
                    if (fkList.Count > 0)
                    {
                        ComboBox combo = new ComboBox();
                        combo.Width = 200;
                        combo.HorizontalAlignment = HorizontalAlignment.Left;
                        combo.ItemsSource = fkList;
                        combo.DisplayMemberPath = ListEntry.DisplayMember;
                        combo.SelectedValuePath = ListEntry.ValueMember;
                        combo.Margin = new Thickness(0, 0, 0, 4);

                        BindingOperations.SetBinding(combo, ComboBox.SelectedValueProperty, myBinding);

                        if (DialogType == DataDialogType.EDIT)
                        {
                            combo.SelectedIndex = ListEntry.GetIndexBySource(combo, value);
                            combo.IsEnabled = fieldEnabled;
                        }

                        Grid.SetColumn(textBlock, 0);
                        Grid.SetColumn(combo, 1);
                        Grid.SetRow(textBlock, rowIndex);
                        Grid.SetRow(combo, rowIndex);

                        this.GridMain.RowDefinitions.Add(new RowDefinition());
                        this.GridMain.Children.Add(textBlock);
                        this.GridMain.Children.Add(combo);
                    }
                    else
                    {
                        TextBox textBox = new TextBox();
                        textBox.Width = 200;
                        textBox.HorizontalAlignment = HorizontalAlignment.Left;
                        textBox.Margin = new Thickness(0, 0, 0, 4);

                        if (columnAtt.Field.Equals(FieldType.Text))
                        {
                            textBox.Height = 100;
                            textBox.Width = 200;
                            textBox.TextWrapping = TextWrapping.Wrap;
                        }

                        BindingOperations.SetBinding(textBox, TextBox.TextProperty, myBinding);

                        if (DialogType == DataDialogType.EDIT)
                        {
                            textBox.Text = value.ToString();
                            textBox.IsEnabled = fieldEnabled;
                        }

                        Grid.SetColumn(textBlock, 0);
                        Grid.SetColumn(textBox, 1);
                        Grid.SetRow(textBlock, rowIndex);
                        Grid.SetRow(textBox, rowIndex);

                        this.GridMain.RowDefinitions.Add(new RowDefinition());
                        this.GridMain.Children.Add(textBlock);
                        this.GridMain.Children.Add(textBox);
                    }

                    rowIndex++;
                }
                else if (columnAtt.Field.Equals(FieldType.File))
                {
                    FileUploader fileBox = new FileUploader();
                    fileBox.Width = 200;
                    fileBox.HorizontalAlignment = HorizontalAlignment.Left;
                    fileBox.Margin = new Thickness(0, 0, 0, 4);

                    BindingOperations.SetBinding(fileBox.FilePath, TextBox.TextProperty, myBinding);

                    if (DialogType == DataDialogType.EDIT)
                    {
                        fileBox.FilePath.Text = value.ToString();
                        fileBox.IsEnabled = fieldEnabled;
                    }

                    Grid.SetColumn(textBlock, 0);
                    Grid.SetColumn(fileBox, 1);
                    Grid.SetRow(textBlock, rowIndex);
                    Grid.SetRow(fileBox, rowIndex);

                    this.GridMain.RowDefinitions.Add(new RowDefinition());
                    this.GridMain.Children.Add(textBlock);
                    this.GridMain.Children.Add(fileBox);

                    rowIndex++;
                }
                // Create the field
                else if (type.BaseType == typeof(Enum) || type == typeof(Enum))
                {
                    ComboBox combo = new ComboBox();
                    combo.Width = 200;
                    combo.HorizontalAlignment = HorizontalAlignment.Left;
                    combo.ItemsSource = EnumToValueConverter.ConvertToList(type, false); //Model stores enum types
                    combo.DisplayMemberPath = ListEntry.DisplayMember;
                    combo.SelectedValuePath = ListEntry.ValueMember;
                    combo.Margin = new Thickness(0, 0, 0, 4);

                    BindingOperations.SetBinding(combo, ComboBox.SelectedValueProperty, myBinding);

                    if (DialogType == DataDialogType.EDIT)
                    {
                        combo.SelectedIndex = ListEntry.GetIndexByName(combo, value.ToString());
                        combo.IsEnabled = fieldEnabled;
                    }

                    Grid.SetColumn(textBlock, 0);
                    Grid.SetColumn(combo, 1);
                    Grid.SetRow(textBlock, rowIndex);
                    Grid.SetRow(combo, rowIndex);

                    this.GridMain.RowDefinitions.Add(new RowDefinition());
                    this.GridMain.Children.Add(textBlock);
                    this.GridMain.Children.Add(combo);

                    rowIndex++;
                }
                else if (type.BaseType == typeof(char) || type == typeof(char))
                {
                    TextBox textBox = new TextBox();
                    textBox.Width = 20;
                    textBox.MaxLength = 1;
                    textBox.HorizontalAlignment = HorizontalAlignment.Left;
                    textBox.Margin = new Thickness(0, 0, 0, 4);

                    BindingOperations.SetBinding(textBox, TextBox.TextProperty, myBinding);

                    if (DialogType == DataDialogType.EDIT)
                    {
                        textBox.Text = value.ToString();
                        textBox.IsEnabled = fieldEnabled;
                    }

                    Grid.SetColumn(textBlock, 0);
                    Grid.SetColumn(textBox, 1);
                    Grid.SetRow(textBlock, rowIndex);
                    Grid.SetRow(textBox, rowIndex);

                    this.GridMain.RowDefinitions.Add(new RowDefinition());
                    this.GridMain.Children.Add(textBlock);
                    this.GridMain.Children.Add(textBox);

                    rowIndex++;
                }
                else if (type.BaseType == typeof(Color) || type == typeof(Color))
                {
                    ColorPicker picker = new ColorPicker();
                    picker.Width = 200;
                    picker.HorizontalAlignment = HorizontalAlignment.Left;
                    picker.Margin = new Thickness(0, 0, 0, 4);

                    // The Syncfusion ColorPicker class does not have a dependencyproperty for the
                    // selected color, created a subclass to add in a dependency property
                    BindingOperations.SetBinding(picker, ColorPicker.SelectedColorProperty, myBinding);

                    if (DialogType == DataDialogType.EDIT)
                    {
                        picker.Color = (Color)ColorConverter.ConvertFromString(value.ToString());
                        picker.IsEnabled = fieldEnabled;
                    }

                    Grid.SetColumn(textBlock, 0);
                    Grid.SetColumn(picker, 1);
                    Grid.SetRow(textBlock, rowIndex);
                    Grid.SetRow(picker, rowIndex);

                    this.GridMain.RowDefinitions.Add(new RowDefinition());
                    this.GridMain.Children.Add(textBlock);
                    this.GridMain.Children.Add(picker);

                    rowIndex++;
                }
                else
                {
                    TextBox textBox = new TextBox();
                    textBox.Width = 200;
                    textBox.HorizontalAlignment = HorizontalAlignment.Left;
                    textBox.Margin = new Thickness(0, 0, 0, 4);

                    if (columnAtt.Field.Equals(FieldType.Text))
                    {
                        textBox.Height = 100;
                        textBox.Width = 200;
                        textBox.TextWrapping = TextWrapping.Wrap;
                    }

                    BindingOperations.SetBinding(textBox, TextBox.TextProperty, myBinding);

                    if (DialogType == DataDialogType.EDIT)
                    {
                        textBox.Text = value.ToString();
                        textBox.IsEnabled = fieldEnabled;
                    }

                    Grid.SetColumn(textBlock, 0);
                    Grid.SetColumn(textBox, 1);
                    Grid.SetRow(textBlock, rowIndex);
                    Grid.SetRow(textBox, rowIndex);

                    this.GridMain.RowDefinitions.Add(new RowDefinition());
                    this.GridMain.Children.Add(textBlock);
                    this.GridMain.Children.Add(textBox);

                    rowIndex++;
                }
            }
        }


		private String _instructions;
        /// <summary>
        /// Gets or sets the instructions.
        /// </summary>
        /// <value>The instructions.</value>
        public String Instructions
		{
			get
			{
				if ( _instructions == null )
				{
                    _instructions = LibraryProperties.Resources.InfoMessage_COMPLETE_SAVE.ConvertEOLs();
				}
				return _instructions;
			}
			set { _instructions = value; }
		}

        /// <summary>
        /// Gets or sets the record type.
        /// </summary>
        /// <value>The type.</value>
        public DataDialogType DialogType { get; set; }

        /// <summary>
        /// Gets or sets the data relationships
        /// </summary>
        /// <value>The data relationships.</value>
        public DataTable RecordTable { get; set; }

        /// <summary>
        /// Gets or sets the record.
        /// </summary>
        /// <value>The record.</value>
        public object Record { get; set; }

        private bool _okToSaveClose;
        /// <summary>
        /// Gets or sets the dialog save and close flag.
        /// </summary>
        private bool OkToSaveClose
        {
            get { return _okToSaveClose; }
            set
            {
                _okToSaveClose = value;

                if (_okToSaveClose)
                {
                    this.DialogResult = true;
                    this.Close();
                }
            }
        }

		/// <summary>
		/// Datas the record control_ loaded.
		/// </summary>
		/// <param name="sender">Sender.</param>
        /// <param name="e">The routed event.</param>
        private void DataDialog_Loaded(object sender, RoutedEventArgs e)
		{
            ConfigureDialog(); 
            PopulateGrid();
		}

        /// <summary>
        /// Verifies the current record
        /// </summary>
        /// <param name="sender">The object.</param>
        /// <param name="e">The routed event.</param>
        private void VerifyRecord(object sender, RoutedEventArgs e)
        {
            OkToSaveClose = Reflect.VerifyRequiredProperties(Record, true);
        }
    }
}
