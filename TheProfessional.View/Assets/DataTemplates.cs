﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace TheProfessional.View
{
	/// <summary>
    /// DataTemplates.
	/// </summary>
	public static class DataTemplates
    {
		/// <summary>
		/// Gets the sync fusion color template.
		/// </summary>
		/// <value>The sync fusion color template.</value>
        public static DataTemplate SyncFusionColorTemplate
        {
            get
            {
                ParserContext pc = new ParserContext();
                pc.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
                pc.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
                pc.XmlnsDictionary.Add("syncfusion", "http://schemas.syncfusion.com/wpf");

                StringBuilder sb = new StringBuilder();
                sb.Append("<DataTemplate x:Key=\"SyncFusionColorTemplate\">");
                sb.Append("<syncfusion:ColorPicker Color=\"{Binding CellBoundValue, Mode=TwoWay}\" />");
                sb.Append("</DataTemplate>");

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString()));
                DataTemplate template = (DataTemplate)XamlReader.Load(ms, pc);
                ms.Dispose();

                return template;
            }
        }

		/// <summary>
		/// Gets the xceed color template.
		/// </summary>
		/// <value>The xceed color template.</value>
        public static DataTemplate XceedColorTemplate
        {
            get
            {
                ParserContext pc = new ParserContext();
                pc.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
                pc.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
                pc.XmlnsDictionary.Add("xctk", "http://schemas.xceed.com/wpf/xaml/toolkit");

                StringBuilder sb = new StringBuilder();
                sb.Append("<DataTemplate x:Key=\"ColorTemplate\">");
                sb.Append("<xctk:ColorPicker SelectedColor=\"{Binding color, Mode=TwoWay}\" />");
                sb.Append("</DataTemplate>");

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString()));
                DataTemplate template = (DataTemplate)XamlReader.Load(ms, pc);
                ms.Dispose();

                return template;
            }
        }

		/// <summary>
		/// Gets the text block template.
		/// </summary>
		/// <value>The text block template.</value>
        public static DataTemplate TextBlockTemplate
        {
            get
            {            
                ParserContext pc = new ParserContext();
                pc.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
                pc.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");

                StringBuilder sb = new StringBuilder();
                sb.Append("<DataTemplate x:Key=\"TextBlockTemplate\">");
                sb.Append("<TextBlock Text=\"{Binding text, Mode=TwoWay}\" />");
                sb.Append("</DataTemplate>");

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString()));
                DataTemplate template = (DataTemplate)XamlReader.Load(ms, pc);
                ms.Dispose();

                return template;
            }
        }

        /// <summary>
        /// Gets the color picker template.
        /// </summary>
        /// <value>The color picker template.</value>
        public static DataTemplate ColorPickerTemplate(string colorPath)
        {
            ParserContext pc = new ParserContext();
            pc.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            pc.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            pc.XmlnsDictionary.Add("controls", "clr-namespace:TheProfessional.View.Assets.Controls;assembly=TheProfessional.View");

            StringBuilder sb = new StringBuilder();
            sb.Append("<DataTemplate x:Key=\"ColorPickerTemplate\">");
            sb.Append("<controls:ColorPicker SelectedColor=\"{Binding " + colorPath + ", Mode=TwoWay}\" />");
            sb.Append("</DataTemplate>");

            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString()));
            DataTemplate template = (DataTemplate)XamlReader.Load(ms, pc);
            ms.Dispose();

            return template;
        }

        /// <summary>
        /// Gets the combobox block template.
        /// </summary>
        /// <value>The combobox block template.</value>
        public static DataTemplate ComboBoxTemplate(string memberPath, string valuePath, string valueBinding)
        {
            ParserContext pc = new ParserContext();
            pc.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            pc.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");

            StringBuilder sb = new StringBuilder();
            sb.Append("<DataTemplate x:Key=\"ComboBoxTemplate\">");
            sb.Append("<ComboBox " +
                "DisplayMemberPath=\"{Binding " + memberPath + ", Mode=TwoWay}\" " +
                "SelectedValuePath=\"{Binding " + valuePath + ", Mode=TwoWay}\" " +
                "SelectedValueBinding=\"{Binding " + valueBinding + ", Mode=TwoWay}\" " +
                " />");
            sb.Append("</DataTemplate>");

            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString()));
            DataTemplate template = (DataTemplate)XamlReader.Load(ms, pc);
            ms.Dispose();

            return template;
        }

        /// <summary>
        /// Gets the combobox block template.
        /// </summary>
        /// <value>The combobox block template.</value>
        public static DataTemplate TextBoxTemplate(string valueBinding)
        {
            ParserContext pc = new ParserContext();
            pc.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            pc.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");

            StringBuilder sb = new StringBuilder();
            sb.Append("<DataTemplate x:Key=\"TextBoxTemplate\">");
            sb.Append("<TextBox " +
                "Text=\"{Binding " + valueBinding + ", Mode=TwoWay}\" " +
                " />");
            sb.Append("</DataTemplate>");

            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString()));
            DataTemplate template = (DataTemplate)XamlReader.Load(ms, pc);
            ms.Dispose();

            return template;
        }
    }
}
