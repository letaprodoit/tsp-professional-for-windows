﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using TheProfessional.Library;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;

namespace TheProfessional.View.Assets.Controls
{
    /// <summary>
    /// Interaction logic for FileUploader.xaml
    /// </summary>
    public partial class FileUploader : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.FileUploader"/> class.
        /// </summary>
        public FileUploader()
        {
            InitializeComponent();

            BrowseButton.Click += ShowOpenDialog;
        }

        /// <summary>
        /// Open File dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowOpenDialog(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog(); 
            fileDialog.Filter = "All Files (*.*)|*.*"; // Optional file extensions

            //To read the content : You will get the filename from the OpenFileDialog and use that to do what IO operation on it.
            Nullable<bool> result = fileDialog.ShowDialog(); 
            
            if (result == true)
            {
                FilePath.Text = UploadeFile(fileDialog.FileName, fileDialog.OpenFile());
            }
        }

        /// <summary>
        /// Upload the file to the server
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="Data"></param>
        private string UploadeFile(string filename, Stream Data)
        {
            String file = filename;

            try
            {
                BinaryReader reader = new BinaryReader(Data);

                string path = ViewProperties.UploadsLocation;
                path = System.IO.Path.GetFullPath(path);

                string fileOnly = System.IO.Path.GetFileName(filename);

				// if the directory doesn't exist create it
				if (!Directory.Exists(path))
				{
					Directory.CreateDirectory(path);
				}

				path += fileOnly;
                file = path;

                FileMode mode = FileMode.CreateNew;

                bool proceed = true;

                // if the file exists ask the user if they would like to overwrite the file
                if (File.Exists(path))
                {
                    String message = String.Format(LibraryProperties.Resources.ConfirmMessage_FILE_EXISTS, ViewProperties.UploadsLocation);
                    
                    MessageBoxResult result = MessageBox.Show(message, LibraryProperties.Resources.Title_ATTENTION, 
                        MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                    if (result == MessageBoxResult.Yes)
                    {
                        mode = FileMode.Truncate;
                    }
                    else
                    {
                        proceed = false;
                    }
                }

                if (proceed)
                {
                    FileStream fstream = new FileStream(path, mode);

                    BinaryWriter wr = new BinaryWriter(fstream);

                    wr.Write(reader.ReadBytes((int)Data.Length));

                    wr.Close();

                    fstream.Close();
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
                MessageBox.ShowError(ex);
            }

            return file;
        }
    }
}
