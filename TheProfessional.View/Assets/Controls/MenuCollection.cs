using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;


namespace TheProfessional.View.Assets.Controls
{
	/// <summary>
	/// Menu collection.
	/// </summary>
	public class MenuCollection : ObservableCollection<MenuItem>
	{
        private static CommandBinding _commandBindings;
		/// <summary>
		/// Gets or sets the command bindings.
		/// </summary>
		/// <value>The command bindings.</value>
        public static CommandBinding CommandBindings
        {
            get
            {
                if (_commandBindings == null)
                {
                    _commandBindings = new CommandBinding();
                }
                return _commandBindings;
            }
            set { _commandBindings = value; }
        }
    }
}
