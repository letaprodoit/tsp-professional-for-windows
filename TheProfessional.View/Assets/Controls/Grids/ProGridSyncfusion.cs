﻿//using Syncfusion.Windows.Controls.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using System.Data;
using System.Collections.ObjectModel;

namespace TheProfessional.View.Assets.Controls.Grids
{
    /// <summary>
    /// Interaction logic for ProGridSyncfusion.cs
    /// </summary>
    public class ProGridSyncfusion : ProGrid, IProGrid, IDisposable
    {
        /// <summary>
        /// Identifies the DemandDataSourceLoadedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent DemandDataSourceLoadedEvent = EventManager.RegisterRoutedEvent("DemandDataSourceLoaded", RoutingStrategy.Bubble, typeof(GridDataOnDemandPageLoadingEventHandler), typeof(ProGrid));

        /// <summary>
        /// The _disposed.
        /// </summary>
        bool _disposed;

        /// <summary>
        /// Has the control loaded completely
        /// </summary>
        bool _loaded = false;

	    /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion"/> class.
	    /// </summary>
        public ProGridSyncfusion()
        {
            // Initialize event handlers
            base.ProGridControl.Loaded += ProGridControl_Loaded;
            base.GridPanel.SizeChanged += GridPanel_SizeChanged;
            this.PropertyChanged += ProGridSyncfusion_PropertyChanged;
        }

	    #region IDisposable Implementation: Methods
        /// <summary>
        /// Releases all resource used by the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion"/> object.
        /// </summary>
        /// <remarks>Call <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion.Dispose()"/> when you are finished using the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion"/>.
        /// The <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion.Dispose()"/> method leaves the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion"/> in an unusable
        /// state. After calling <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion.Dispose()"/>, you must release all references to the
        /// <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion"/> so the garbage collector can reclaim the memory that the
        /// <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion"/> was occupying.</remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridSyncfusion"/> is reclaimed by garbage collection.
        /// </summary>
        ~ProGridSyncfusion()
        {
            Dispose(false);
        }

        /// <summary>
        /// Dispose the specified disposing.
        /// </summary>
        /// <param name="disposing">If set to <c>true</c> disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                if (_gridData != null)
                {
                    _gridData.Dispose();
                    _gridData = null;
                }
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
        #endregion

        #region IProGrid Implementation: Methods
        /// <summary>
        /// Add columns to the datagrid
        /// </summary>
        public void InitializeGrid()
        {
            try
            {
                if (base.GridColumns != null && GridData.Model.ColumnCount == 0)
                {
                    PropertyInfo[] properties = base.GridColumns.GetType().GetProperties();
                    foreach (PropertyInfo prop in properties)
                    {
                        bool fieldEnabled = true;

                        // skip processing of the properties that should be ignored
						if (prop.GetCustomAttributes(typeof(IgnoreAttribute), true).Any() ||
                            prop.GetCustomAttributes(typeof(HideInGridAttribute), true).Any())
                        {
                            continue;
                        }

						ColumnAttribute columnAtt = (ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).GetValue(0);

						// records that are both a primary key and a foreign key can not be edited
						// auto increment and readonly fields can not be edited
						if ((columnAtt.PrimaryKey && columnAtt.ForeignKey) || columnAtt.AutoIncrement || columnAtt.ReadOnly)
						{
							fieldEnabled = false;
						}

                        string title = prop.Name;
                        string name = title.ToLower(CultureInfo.CurrentCulture);
                        Type type = prop.PropertyType;

						// if the property has an alternate mapping name use it instead
						// of the property name itself
						if (!String.IsNullOrEmpty(columnAtt.Name))
						{
							name = columnAtt.Name;
						}

						// if the property has an alternate title use it instead
						// of the property name itself
						if (!String.IsNullOrEmpty(columnAtt.Title))
						{
							title = columnAtt.Title;
						}

                        GridDataVisibleColumn col = new GridDataVisibleColumn();
                        col.HeaderText = title;
                        col.MappingName = name;
                        col.ShowColumnOptions = true;
                        col.AllowFilter = true;
                        col.AllowDrag = true;
                        col.Width = new GridDataControlLength(0, GridControlLengthUnitType.SizeToHeader);
                        col.IsReadOnly = !fieldEnabled;

                        // if the property has an autosize attribute set the column size
                        // to the size of the cells
						if (columnAtt.AutoSize)
                        {
                            col.Width = new GridDataControlLength(0, GridControlLengthUnitType.Auto);
                        }

                        // Make the column sortable if it contains the Sort attribute
						if (columnAtt.Sort)
                        {
                            GridDataSortColumn sortCol = new GridDataSortColumn();
                            sortCol.ColumnName = col.MappingName;
                            GridData.SortColumns.Add(sortCol);
                        }

                        GridDataColumnStyle style = new GridDataColumnStyle();

						if (columnAtt.ForeignKey)
                        {
                            ListEntry filter = new ListEntry();

                            // skip processing of the properties that should be ignored
                            if (prop.GetCustomAttributes(typeof(FilterAttribute), true).Any())
                            {
                                FilterAttribute filterAtt = (FilterAttribute)prop.GetCustomAttributes(typeof(FilterAttribute), true).GetValue(0);
                                filter.DisplayName = filterAtt.Field;
                                filter.Source = filterAtt.Value;
                            }

                            List<ListEntry> fkList = Reflect.GetForeignKeyPropertiesCollection((DataTable)GridData.ItemsSource, name, filter);

                            // If the list returned items then this is a combo box
                            // else do nothing, fields are textbox by default
                            if (fkList.Count > 0)
                            {
                                style.DropDownStyle = GridDropDownStyle.Exclusive;
                                style.ItemsSource = fkList;
                                style.DisplayMember = ListEntry.DisplayMember;
                                style.ValueMember = ListEntry.ValueMember;
                                style.CellType = "ComboBox";

                                // Resize width of the combobox column
                                col.Width = new GridDataControlLength(150, GridControlLengthUnitType.None);
                            }
                        }
                        else if (type.BaseType == typeof(Enum) || type == typeof(Enum))
                        {
                            style.DropDownStyle = GridDropDownStyle.Exclusive;
                            style.ItemsSource = EnumToValueConverter.ConvertToList(type, true); //Database stores one char values
                            style.DisplayMember = ListEntry.DisplayMember;
                            style.ValueMember = ListEntry.ValueMember;
                            style.CellType = "ComboBox";

                            // Resize width of the combobox column
                            col.Width = new GridDataControlLength(150, GridControlLengthUnitType.None);
                        }
                        else if (type.BaseType == typeof(char) || type == typeof(char))
                        {
                            style.HorizontalAlignment = HorizontalAlignment.Center;
                        }
                        else if (type.BaseType == typeof(Color) || type == typeof(Color))
                        {
                            col.CellItemTemplate = Templates.SyncFusionColorTemplate;
                            col.CellEditItemTemplate = Templates.SyncFusionColorTemplate;

                            // Resize width of the color picker column
                            col.Width = new GridDataControlLength(150, GridControlLengthUnitType.None);
                        }
                        
                        col.ColumnStyle = style;
                        GridData.VisibleColumns.Add(col);
                        col.Dispose();
                        style.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Initialize datagrid style
        /// </summary>
        public void InitializeGridStyle()
        {
            if (GridData != null)
            {
                GridData.VisualStyle = ViewProperties.GridVisualStyle;
                GridData.Model.RowHeights.DefaultLineSize = ViewProperties.GridRowHeight;
            }
        }

        /// <summary>
        /// Sets the number of pages.
        /// </summary>
        public void InitializeGridPager()
        {
            try
            {
                if (GridPager != null)
                {
                    PageCount = (int)Math.Ceiling((decimal)RecordCount / (decimal)ViewProperties.GridPageSize);
                    PageSize = ViewProperties.GridPageSize;
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Reinitialize control
        /// </summary>
        public void Refresh()
        {
            // Only allow refresh if the control has loaded completely
            if (_loaded)
            {
                InitializeGridPager();
                InitializeGridStyle();
            }
        }
        #endregion

        #region IProGrid Implementation: Properties
        /// <summary>
        /// Gets or sets a value for the PageSize of the data pager
        /// </summary>
        public double RowHeight
        {
            get { return GridData.Model.RowHeights.DefaultLineSize; }
            set { GridData.Model.RowHeights.DefaultLineSize = value; }
        }


        /// <summary>
        /// Gets or sets a value for the PageSize of the data pager
        /// </summary>
        public int PageSize
        {
            get { return GridPager.PageSize; }
            set { GridPager.PageSize = value; }
        }


        /// <summary>
        /// Gets or sets a value for the PageCount of the data pager
        /// </summary>
        public int PageCount
        {
            get { return GridPager.PageCount; }
            set
            {
                GridPager.PageCount = value;
                GridPager.NumericButtonCount = value;
            }
        }

        /// <summary>
        /// Gets or sets a value for the RecordOffset of the data pager
        /// </summary>
		public int RecordOffset { get; set; }
        #endregion

        #region Properties
		/// <summary>
		/// Gets the grid data.
		/// </summary>
		/// <value>The grid data.</value>
		private GridDataControl _gridData;
        private GridDataControl GridData
        {
            set { _gridData = value; }
            get
            {
                if (_gridData == null)
                {
                    _gridData = new GridDataControl();
                    _gridData.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                    _gridData.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                    _gridData.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
                    _gridData.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;
					_gridData.ItemsSource = base.ItemsSource;
                    _gridData.SelectedItem = base.SelectedItem;
                    _gridData.Margin = new Thickness(0, 0, 10, 0);
                    //_gridData.HeadersVisibility = ShowGridHeaders ? DataGridHeadersVisibility.All : DataGridHeadersVisibility.None;
                    // TODO: Implement show/hide column headers

                    _gridData.Model.ColumnCount = 0;
                    _gridData.EnableContextMenu = true;
                    _gridData.ContextMenuOptions = ContextMenuOptions.Custom;
                    _gridData.UpdateMode = UpdateMode.PropertyChanged;

                    MenuCollection menu = new MenuCollection();
                    menu.Add(base.AddMenu);
                    menu.Add(base.EditMenu);
                    menu.Add(base.DeleteMenu);
                    _gridData.RecordContextMenuItems = menu;

                    Grid.SetRow(_gridData, 2);
                    Grid.SetColumnSpan(_gridData, 2);
                    base.GridPanel.Children.Add(_gridData);

                    _gridData.Model.CommittedCellInfo += GridData_CommittedCellInfo;
                    _gridData.RecordsSelectionChanged += GridData_RecordsSelectionChanged;
                    _gridData.CurrentCellAcceptedChanges += GridData_CurrentCellAcceptedChanges;
                    _gridData.MouseDoubleClick += GridData_MouseDoubleClick;
                    _gridData.KeyDown += GridData_KeyDown;
                }
                return _gridData;
            }
        }

		/// <summary>
		/// Gets the grid pager.
		/// </summary>
		/// <value>The grid pager.</value>
		private DataPagerExt _gridPager;
        private DataPagerExt GridPager
        {
            get
            {
                if (_gridPager == null)
                {
                    _gridPager = new DataPagerExt();
                    _gridPager.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                    _gridPager.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                    _gridPager.AutoEllipsis = true;
                    _gridPager.IsPagingOnDemand = true;
                    _gridPager.DisplayMode = Syncfusion.Windows.Controls.Grid.PagerDisplayMode.FirstLastPreviousNextNumeric;
                    _gridPager.Background = new SolidColorBrush(Colors.Transparent);
                    _gridPager.Margin = new Thickness(0, 0, 10, 0);
                    _gridPager.Visibility = ShowDataPager ? Visibility.Visible : Visibility.Collapsed;

                    Grid.SetRow(_gridPager, 1);
                    Grid.SetColumn( _gridPager, 1 );
                    base.GridPanel.Children.Add(_gridPager);

                    _gridPager.OnDemandDataSourceLoad += GridPager_OnDemandDataSourceLoad;
                }

                return _gridPager;
            }
        }

        /// <summary>
        /// Gets or sets a value for the VisualStyle of the datagrid
        /// </summary>
        private VisualStyle VisualStyle
        {
            get { return GridData.VisualStyle; }
            set { GridData.VisualStyle = value; }
        }

        #endregion

        #region Events
        /// <summary>
        /// Handler for the DataPagerLoaded event
        /// </summary>
        public event GridDataOnDemandPageLoadingEventHandler DemandDataSourceLoaded
        {
            add { this.AddHandler(DemandDataSourceLoadedEvent, value); }
            remove { this.RemoveHandler(DemandDataSourceLoadedEvent, value); }
        }

        #endregion

        #region Listeners
        /// <summary>
        /// Occurs this control is loaded
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void ProGridControl_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeGrid();
            InitializeGridPager();
            InitializeGridStyle();

            _loaded = true;
        }

        /// <summary>
        /// Handle property changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProGridSyncfusion_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("RecordCount"))
            {
                Refresh();
            }
        }

        /// <summary>
        /// Occurs when the data pager is loaded
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void GridPager_OnDemandDataSourceLoad(object sender, Syncfusion.Windows.Controls.Grid.GridDataOnDemandPageLoadingEventArgs e)
        {
            if (sender.GetType() == typeof(DataPagerExt))
            {
                DataPagerExt pager = sender as DataPagerExt;
                // Now notify anyone listening that the pager is loaded on the ProDataGrid
                OnDemandDataSourceLoaded(pager, e);

                InitializeGridPager();
                InitializeGridStyle(); //Reset Gridrow Height
            }
        }

        /// <summary>
        /// Raises the row selection event for data grid control.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void GridData_RecordsSelectionChanged(object sender, GridDataRecordsSelectionChangedEventArgs e)
        {
            if (sender.GetType() == typeof(GridDataControl))
            {
                GridDataControl grid = sender as GridDataControl;

                if (grid.SelectedItems.Count == 1)
                {
                    ToggleButtons(true, true, true);
                }
                else if (grid.SelectedItems.Count > 1)
                {
                    ToggleButtons(true, false, true);
                }
                else
                {
                    ToggleButtons(true, false, false);
                }

                base.SelectedItem = grid.SelectedItem;
                base.SelectedItems = grid.SelectedItems;
            }
        }

        /// <summary>
        /// Event to alert anyone listening that a record has been updated
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">args</param>
        private void GridData_CurrentCellAcceptedChanges(object sender, Syncfusion.Windows.ComponentModel.SyncfusionRoutedEventArgs args)
        {
            // Now notify anyone listening that a record was updated
            base.OnRecordUpdated(sender, args);
        }

        /// <summary>
        /// Event to alert anyone that the mouse double clicked a record
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">e</param>
        private void GridData_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (base.SelectedItems != null)
            {
                base.OnEditButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Event to alert anyone that the delete button was pressed
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">e</param>
        private void GridData_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (base.SelectedItems != null && (e.Key == System.Windows.Input.Key.Delete || e.Key == System.Windows.Input.Key.Back))
            {
                base.OnDeleteButtonClicked(sender, e);
            }
        }
        
        /// <summary>
		/// Event to change the data grid's height as the grid's height changes
		/// This was needed since the data grid would not snap to the grid with column and row
		/// definitions defined 
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void GridPanel_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (base.GridPanel != null)
			{
				GridData.Height = base.GridPanel.RenderSize.Height - 90;
			}
		}

        /// <summary>
        /// Occurs when the content of a cell is changed
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void GridData_CommittedCellInfo(object sender, GridCommitCellInfoEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                GridData.Model.ResizeColumnsToFit ( GridRangeInfo.Col ( e.Cell.ColumnIndex ), GridResizeToFitOptions.None );
            }));
        }
        #endregion

        #region Event Raisers
		/// <summary>
		/// Raises the demand data source loaded event.
		/// </summary>
		/// <param name="pager">Pager.</param>
		/// <param name="e">E.</param>
        private void OnDemandDataSourceLoaded(DataPagerExt pager, Syncfusion.Windows.Controls.Grid.GridDataOnDemandPageLoadingEventArgs e)
        {
            try
            {
                RecordOffset = e.PagedRows;

                GridDataOnDemandPageLoadingEventArgs args = new GridDataOnDemandPageLoadingEventArgs(RecordOffset, PageSize);
                args.RoutedEvent = DemandDataSourceLoadedEvent;
                RaiseEvent(args);
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        #endregion
    }
}
