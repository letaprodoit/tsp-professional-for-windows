﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheProfessional.View.Assets.Controls.Grids
{
    /// <summary>
    /// Interface for ProGrid control
    /// </summary>
    interface IProGrid
    {
		/// <summary>
		/// Gets or sets the height of the row.
		/// </summary>
		/// <value>The height of the row.</value>
        double RowHeight { get; set; }

		/// <summary>
		/// Gets or sets the size of the page.
		/// </summary>
		/// <value>The size of the page.</value>
        int PageSize { get; set; }

		/// <summary>
		/// Gets or sets the page count.
		/// </summary>
		/// <value>The page count.</value>
        int PageCount { get; set; }

		/// <summary>
		/// Gets or sets the record offset.
		/// </summary>
		/// <value>The record offset.</value>
        int RecordOffset { get; set; }

		/// <summary>
		/// Initializes the grid.
		/// </summary>
        void InitializeGrid();

		/// <summary>
		/// Initializes the grid pager.
		/// </summary>
        void InitializeGridPager();

		/// <summary>
		/// Initializes the grid style.
		/// </summary>
        void InitializeGridStyle();

		/// <summary>
		/// Refresh this instance.
		/// </summary>
        void Refresh();
    }
}
