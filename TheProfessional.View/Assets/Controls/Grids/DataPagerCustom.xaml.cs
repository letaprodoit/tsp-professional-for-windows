using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using System.ComponentModel;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Collections;
using System.Data;
using FirstFloor.ModernUI.Windows.Controls;
using System.Text.RegularExpressions;

namespace TheProfessional.View.Assets.Controls.Grids
{
    /// <summary>
    /// Data pager display modes
    /// </summary>
    public enum PagerDisplayMode 
	{
        ///<summary>
        ///     Shows the First and Last buttons + the numeric display
        ///</summary>
        [DefaultValue("FirstLastNumeric")]
        FirstLastNumeric = 0,
        ///<summary>
        ///     Shows the First, Last, Previous, Next buttons
        ///</summary>
        [DefaultValue("FirstLastPreviousNext")]
        FirstLastPreviousNext = 1,
        ///<summary>
        ///     Shows the First, Last, Previous, Next buttons + the numeric display
        ///</summary>
        [DefaultValue("FirstLastPreviousNextNumeric")]
        FirstLastPreviousNextNumeric = 2,
        ///<summary>
        ///     Shows the numeric display
        ///</summary>
        [DefaultValue("Numeric")]
        Numeric = 3,
        ///<summary>
        ///     Shows the Previous and Next buttons
        ///</summary>
        [DefaultValue("PreviousNext")]
        PreviousNext = 4,
        ///<summary>
        ///     Shows the Previous and Next buttons + the numeric display
        ///</summary>
        [DefaultValue("PreviousNextNumeric")]
        PreviousNextNumeric = 5,
    };

    /// <summary>
	/// Grid data on demand page loading event handler.
	/// </summary>
	public delegate void GridDataOnDemandPageLoadingEventHandler(object sender, GridDataOnDemandPageLoadingEventArgs e);

	/// <summary>
    /// Event args for the ProGridEventHandler
    /// </summary>
    public class GridDataOnDemandPageLoadingEventArgs : RoutedEventArgs
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.GridDataOnDemandPageLoadingEventArgs"/> class.
		/// </summary>
		/// <param name="pagedRows">Paged rows.</param>
		/// <param name="maximumRows">Maximum rows.</param>
        public GridDataOnDemandPageLoadingEventArgs( int pagedRows, int maximumRows)
		{
            PagedRows = pagedRows;
            MaximumRows = maximumRows;
        }

		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.GridDataOnDemandPageLoadingEventArgs"/> class.
		/// </summary>
		/// <param name="routedEvent">Routed event.</param>
        public GridDataOnDemandPageLoadingEventArgs(RoutedEvent routedEvent)
            : base(routedEvent)
        {
        }

		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.GridDataOnDemandPageLoadingEventArgs"/> class.
		/// </summary>
		/// <param name="routedEvent">Routed event.</param>
		/// <param name="source">Source.</param>
        public GridDataOnDemandPageLoadingEventArgs(RoutedEvent routedEvent, object source)
            : base(routedEvent, source)
        {
        }

		/// <summary>
		/// Gets or sets the paged rows.
		/// </summary>
		/// <value>The paged rows.</value>
        public int PagedRows { get; set; }

		/// <summary>
		/// Gets or sets the maximum rows.
		/// </summary>
		/// <value>The maximum rows.</value>
        public int MaximumRows { get; set; }
    }

    /// <summary>
    /// Interaction logic for DataPagerExt.xaml
    /// </summary>
    public partial class DataPagerCustom : ControlView
    {        
        /// <summary>
        /// Identifies the AutoEllipsisProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty AutoEllipsisProperty = DependencyProperty.Register("AutoEllipsis", typeof(bool), typeof(DataPagerCustom), new PropertyMetadata(true));
        /// <summary>
        /// Identifies the IsPagingOnDemandProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty IsPagingOnDemandProperty = DependencyProperty.Register("IsPagingOnDemand", typeof(bool), typeof(DataPagerCustom), new PropertyMetadata(true));
        /// <summary>
        /// Identifies the DisplayModeProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty DisplayModeProperty = DependencyProperty.Register("DisplayMode", typeof(PagerDisplayMode), typeof(DataPagerCustom), new PropertyMetadata(PagerDisplayMode.FirstLastPreviousNextNumeric));
        /// <summary>
        /// Identifies the PageCountProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty PageCountProperty = DependencyProperty.Register("PageCount", typeof(int), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the PageSizeProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty PageSizeProperty = DependencyProperty.Register("PageSize", typeof(int), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the RecordOffsetProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty RecordOffsetProperty = DependencyProperty.Register("RecordOffset", typeof(int), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the SourceProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register("Source", typeof(object), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the NumericButtonCountProperty dependency property.
        /// </summary>
        public static readonly DependencyProperty NumericButtonCountProperty = DependencyProperty.Register("NumericButtonCount", typeof(int), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the AddButtonClickedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent OnDemandDataSourceLoadEvent = EventManager.RegisterRoutedEvent("OnDemandDataSourceLoad", RoutingStrategy.Bubble, typeof(GridDataOnDemandPageLoadingEventHandler), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the FirstButtonClickedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent FirstButtonClickedEvent = EventManager.RegisterRoutedEvent("FirstButtonClicked", RoutingStrategy.Bubble, typeof(GridDataOnDemandPageLoadingEventHandler), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the LastButtonClickedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent LastButtonClickedEvent = EventManager.RegisterRoutedEvent("LastButtonClicked", RoutingStrategy.Bubble, typeof(GridDataOnDemandPageLoadingEventHandler), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the PreviousButtonClickedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent PreviousButtonClickedEvent = EventManager.RegisterRoutedEvent("PreviousButtonClicked", RoutingStrategy.Bubble, typeof(GridDataOnDemandPageLoadingEventHandler), typeof(DataPagerCustom));
        /// <summary>
        /// Identifies the NextButtonClickedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent NextButtonClickedEvent = EventManager.RegisterRoutedEvent("NextButtonClicked", RoutingStrategy.Bubble, typeof(GridDataOnDemandPageLoadingEventHandler), typeof(DataPagerCustom));

		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.DataPagerCustom"/> class.
		/// </summary>
        public DataPagerCustom()
        {
            InitializeComponent();

            InitializeNavMenuControls();

            // Add a listener to DisplayMode changes
            DependencyPropertyDescriptor pd1 = DependencyPropertyDescriptor.FromProperty(DisplayModeProperty, typeof(DataPagerCustom));
            pd1.AddValueChanged(this, DisplayModeProperty_Changed);

            // Add a listener to Source changes
            DependencyPropertyDescriptor pd2 = DependencyPropertyDescriptor.FromProperty(SourceProperty, typeof(DataPagerCustom));
            pd2.AddValueChanged(this, SourceProperty_Changed);

            // Add a listener to PageCount changes
            DependencyPropertyDescriptor pd3 = DependencyPropertyDescriptor.FromProperty(PageCountProperty, typeof(DataPagerCustom));
            pd3.AddValueChanged(this, PageCountProperty_Changed);

            // Add a listener to PageSize changes
            DependencyPropertyDescriptor pd4 = DependencyPropertyDescriptor.FromProperty(PageSizeProperty, typeof(DataPagerCustom));
            pd4.AddValueChanged(this, PageSizeProperty_Changed);
        }
  
        #region Methods
        /// <summary>
        /// Initialize Nav menu controls
        /// </summary>
        private void InitializeNavMenuControls()
        {
            // Add listeners to main buttons
            this.FirstButton.Click += FirstButton_Click;
            this.LastButton.Click += LastButton_Click;
            this.PreviousButton.Click += PreviousButton_Click;
            this.NextButton.Click += NextButton_Click;
        }

        /// <summary>
        /// Set visibiliteis of nav buttons
        /// </summary>
        /// <param name="firstButton"></param>
        /// <param name="lastButton"></param>
        /// <param name="prevButton"></param>
        /// <param name="nextButton"></param>
        /// <param name="numButtons"></param>
        private void SetNavMenuVisibilities(bool firstButton, bool lastButton, bool prevButton, bool nextButton, bool numButtons)
        {
            this.FirstButton.Visibility = firstButton ? Visibility.Visible : Visibility.Collapsed;
            this.LastButton.Visibility = lastButton ? Visibility.Visible : Visibility.Collapsed;
            this.PreviousButton.Visibility = prevButton ? Visibility.Visible : Visibility.Collapsed;
            this.NextButton.Visibility = nextButton ? Visibility.Visible : Visibility.Collapsed;
            this.NumericButtons.Visibility = numButtons ? Visibility.Visible : Visibility.Collapsed;

            FirstEnabled = firstButton;
            LastEnabled = lastButton;
            PrevEnabled = prevButton;
            NextEnabled = nextButton;
        }

        /// <summary>
        /// Emphasis the current page
        /// </summary>
        /// <param name="page"></param>
        private void EmphasisCurrentPage(int page)
        {
            // loop to pagecount and add in buttons with onclick events
            for (int i = 1; i <= PageCount; i++)
            {
                ModernButton button = this.NumericButtons.Children[i - 1] as ModernButton;

                if (i == page)
                {
                    button.FontWeight = FontWeights.Bold;
                }
                else
                {
                    button.FontWeight = FontWeights.Normal;
                }
            }
        }
        #endregion

        #region Properties
        private int _currentPage;
        /// <summary>
        /// Gets or sets the current page
        /// </summary>
        private int CurrentPage
        {
            get 
            {
                if (_currentPage == 0)
                {
                    _currentPage = 1;
                }

                return _currentPage; 
            }
            set
            {
                _currentPage = value;
                EmphasisCurrentPage(_currentPage);
            }

        }

        /// <summary>
        /// Gets or sets a value indicating whether the first record button is enabled
        /// </summary>
        private bool FirstEnabled
        {
            get { return this.FirstButton.IsEnabled; }
            set { this.FirstButton.IsEnabled = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the last record button is enabled
        /// </summary>
        private bool LastEnabled
        {
            get { return this.LastButton.IsEnabled; }
            set { this.LastButton.IsEnabled = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the previous record button is enabled
        /// </summary>
        private bool PrevEnabled
        {
            get { return this.PreviousButton.IsEnabled; }
            set { this.PreviousButton.IsEnabled = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the next record button is enabled
        /// </summary>
        private bool NextEnabled
        {
            get { return this.NextButton.IsEnabled; }
            set { this.NextButton.IsEnabled = value; }
        }

        /// <summary>
        ///     Gets or sets the data collection that the System.Windows.Controls.DataPagerCustom
        ///     controls paging for.
        /// </summary>
        /// <returns>The data collection associated with this pager control.</returns>
        public object Source
        {
            get { return (object)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the pager shows autoelipsis
        /// </summary>
        public bool AutoEllipsis
        {
            get { return (bool)GetValue(AutoEllipsisProperty); }
            set { SetValue(AutoEllipsisProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the pager loads data on demand
        /// </summary>
        public bool IsPagingOnDemand
        {
            get { return (bool)GetValue(IsPagingOnDemandProperty); }
            set { SetValue(IsPagingOnDemandProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating the navigation control type
        /// </summary>
        public PagerDisplayMode DisplayMode
        {
            get { return (PagerDisplayMode)GetValue(DisplayModeProperty); }
            set { SetValue(DisplayModeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating page size
        /// </summary>
        public int PageSize
        {
            get { return (int)GetValue(PageSizeProperty); }
            set { SetValue(PageSizeProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating page count
        /// </summary>
        public int PageCount
        {
            get { return (int)GetValue(PageCountProperty); }
            set { SetValue(PageCountProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating page count
        /// </summary>
        public int NumericButtonCount
        {
            get { return (int)GetValue(NumericButtonCountProperty); }
            set { SetValue(NumericButtonCountProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating record offset
        /// </summary>
        public int RecordOffset
        {
            get { return (int)GetValue(RecordOffsetProperty); }
            set { SetValue(RecordOffsetProperty, value); }
        }
        #endregion

        #region Events
        /// <summary>
        /// Handler for the OnDemandDataSourceLoad event
        /// </summary>
        public event GridDataOnDemandPageLoadingEventHandler OnDemandDataSourceLoad
        {
            add
            {
                this.AddHandler(OnDemandDataSourceLoadEvent, value);
            }
            remove
            {
                this.RemoveHandler(OnDemandDataSourceLoadEvent, value);
            }
        }

        /// <summary>
        /// Handler for the FirstButtonClicked event
        /// </summary>
        public event GridDataOnDemandPageLoadingEventHandler FirstButtonClicked
        {
            add
            {
                // Buttons
                this.FirstButton.AddHandler(FirstButtonClickedEvent, value);
            }
            remove
            {
                // Buttons
                this.FirstButton.RemoveHandler(FirstButtonClickedEvent, value);
            }
        }


        /// <summary>
        /// Handler for the LastButtonClicked event
        /// </summary>
        public event GridDataOnDemandPageLoadingEventHandler LastButtonClicked
        {
            add
            {
                // Buttons
                this.LastButton.AddHandler(LastButtonClickedEvent, value);
            }
            remove
            {
                // Buttons
                this.LastButton.RemoveHandler(LastButtonClickedEvent, value);
            }
        }


        /// <summary>
        /// Handler for the PreviousButtonClicked event
        /// </summary>
        public event GridDataOnDemandPageLoadingEventHandler PreviousButtonClicked
        {
            add
            {
                // Buttons
                this.PreviousButton.AddHandler(PreviousButtonClickedEvent, value);
            }
            remove
            {
                // Buttons
                this.PreviousButton.RemoveHandler(PreviousButtonClickedEvent, value);
            }
        }


        /// <summary>
        /// Handler for the NextButtonClicked event
        /// </summary>
        public event GridDataOnDemandPageLoadingEventHandler NextButtonClicked
        {
            add
            {
                // Buttons
                this.NextButton.AddHandler(NextButtonClickedEvent, value);
            }
            remove
            {
                // Buttons
                this.NextButton.RemoveHandler(NextButtonClickedEvent, value);
            }
        }
        #endregion

        #region Event Raisers
        /// <summary>
        /// Raises the data source loaded event
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OnDemandDataSourceLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PageSize != 0)
                {
                    GridDataOnDemandPageLoadingEventArgs args = new GridDataOnDemandPageLoadingEventArgs(OnDemandDataSourceLoadEvent);
                    args.PagedRows = RecordOffset;
                    args.MaximumRows = PageSize;

                    RaiseEvent(args);
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Raises the page button clicked event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OnPageButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender.GetType() == typeof(ModernButton))
                {
                    ModernButton button = sender as ModernButton;
                    Match match = Regex.Match(button.Name, @"Page(\d+)$");
                    if (match.Success)
                    {
                        //int pageNum = Int32.Parse(match.Groups[1].Value);
                        int pageNum = Int32.Parse(button.Tag.ToString());

                        CurrentPage = pageNum;

                        // subtract 1 from page number in order to get the record offset
                        if (pageNum != 0)
                        {
                            pageNum -= 1;
                        }

                        RecordOffset = pageNum * PageSize;

                        OnDemandDataSourceLoaded(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Raises the first button clicked event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OnFirstButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                // if not on the first page then its ok to reload the grid
                // there is no need to reload the grid since we are already on the first page
                if (CurrentPage != 1)
                {
                    RecordOffset = 0;

                    OnDemandDataSourceLoaded(sender, e);

                    CurrentPage = 1;
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }


        /// <summary>
        /// Raises the last button clicked event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OnLastButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                // if not on the last page then its ok to reload the grid
                // there is no need to reload the grid since we are already on the last page
                if (CurrentPage != PageCount)
                {
                    RecordOffset = (PageCount - 1) * PageSize;

                    OnDemandDataSourceLoaded(sender, e);

                    CurrentPage = PageCount;
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Raises the previous button clicked event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OnPreviousButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                // if not on the first page then its ok to reload the grid
                // there is no previous record on the first record
                if (RecordOffset != 0)
                {
                    RecordOffset = (CurrentPage - 1) * PageSize;

                    OnDemandDataSourceLoaded(sender, e);

                    CurrentPage -= 1;
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }


        /// <summary>
        /// Raises the previous button clicked event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void OnNextButtonClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                // if not on the last page then its ok to reload the grid
                // there is no next record on the last record
                if (CurrentPage != PageCount)
                {
                    RecordOffset = CurrentPage * PageSize;

                    OnDemandDataSourceLoaded(sender, e);

                    CurrentPage += 1;
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }
        #endregion

        #region Listeners
        /// <summary>
        /// Raises display mode changed when DisplayModeProperty changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayModeProperty_Changed(object sender, EventArgs e)
        {
            switch (DisplayMode)
            {
                case PagerDisplayMode.FirstLastNumeric:
                    SetNavMenuVisibilities(true, true, false, false, true);
                    break;
                case PagerDisplayMode.FirstLastPreviousNext:
                    SetNavMenuVisibilities(true, true, true, true, false);
                    break;
                case PagerDisplayMode.FirstLastPreviousNextNumeric:
                    SetNavMenuVisibilities(true, true, true, true, true);
                    break;
                case PagerDisplayMode.Numeric:
                    SetNavMenuVisibilities(false, false, false, false, true);
                    break;
                case PagerDisplayMode.PreviousNext:
                    SetNavMenuVisibilities(false, false, true, true, false);
                    break;
                case PagerDisplayMode.PreviousNextNumeric:
                    SetNavMenuVisibilities(false, false, true, true, true);
                    break;
            }
        }

        /// <summary>
        /// Raises source changed when SourceProperty changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourceProperty_Changed(object sender, EventArgs e)
        {
			// if page size does not equal zero and the source has records
			if ( PageSize != 0 && Source != null )
			{
                PageCount = (int)Math.Ceiling((decimal)((DataTable)Source).Rows.Count / (decimal)PageSize);
			}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageSizeProperty_Changed(object sender, EventArgs e)
        {
			// if page size does not equal zero and the source has records
            if (PageSize != 0 && Source != null)
            {
                try
                {
                    PageCount = (int)Math.Ceiling((decimal)((DataTable)Source).Rows.Count / (decimal)PageSize);

                    if (RecordOffset == 0)
                    {
                        CurrentPage = 1;
                    }

                    OnDemandDataSourceLoaded(sender, new RoutedEventArgs());
                }
                catch (Exception ex)
                {
                    // Get call stack
                    StackTrace stackTrace = new StackTrace();
                    String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                    // Get calling method name
                    Trace.WriteLine(methodName + ": " + ex.Message);
                }
            }
       }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageCountProperty_Changed(object sender, EventArgs e)
        {
			// only manipulate the numeric buttons if they are being displayed
			if ( !DisplayMode.Equals( PagerDisplayMode.FirstLastPreviousNext ) &&
			     !DisplayMode.Equals( PagerDisplayMode.PreviousNext ) )
			{
                try
                {
                    // Clear Numeric buttons stack panel
                    this.NumericButtons.Children.Clear();

                    // loop to pagecount and add in buttons with onclick events
                    for (int i = 1; i <= PageCount; i++)
                    {
                        ModernButton page = new ModernButton();
                        page.Name = "Page" + i.ToString();
                        page.Tag = i;
                        page.Content = i.ToString();
                        page.EllipseDiameter = 0;
                        page.Padding = new Thickness(5, 0, 5, 0);
                        page.Click += Page_Click;

                        this.NumericButtons.Children.Add(page);
                    }

                    CurrentPage = 1;
                    OnDemandDataSourceLoaded(sender, new RoutedEventArgs());
                }
                catch (Exception ex)
                {
                    // Get call stack
                    StackTrace stackTrace = new StackTrace();
                    String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                    // Get calling method name
                    Trace.WriteLine(methodName + ": " + ex.Message);
                }
			}
        }

        /// <summary>
        /// Occurs when a page/numeric button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Click(object sender, RoutedEventArgs e)
        {
            OnPageButtonClicked(sender, e);
        }

        /// <summary>
        /// Occurs when the add button is clicked
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void FirstButton_Click(object sender, RoutedEventArgs e)
        {
            // Now notify anyone listening that the button was clicked on the DataGridMenu
            OnFirstButtonClicked(sender, e);
        }


        /// <summary>
        /// Occurs when the last button is clicked
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void LastButton_Click(object sender, RoutedEventArgs e)
        {
            // Now notify anyone listening that the button was clicked on the DataGridMenu
            OnLastButtonClicked(sender, e);
        }


        /// <summary>
        /// Occurs when the prev button is clicked
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            // Now notify anyone listening that the button was clicked on the DataGridMenu
            OnPreviousButtonClicked(sender, e);
        }

        /// <summary>
        /// Occurs when the prev button is clicked
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            // Now notify anyone listening that the button was clicked on the DataGridMenu
            OnNextButtonClicked(sender, e);
        }

        #endregion
    }
}
