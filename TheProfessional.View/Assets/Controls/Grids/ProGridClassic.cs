using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using System.Data;
using System.Collections.ObjectModel;

namespace TheProfessional.View.Assets.Controls.Grids
{
    /// <summary>
    /// Interaction logic for ProGridClassic.cs
    /// </summary>
    public class ProGridClassic : ProGrid, IProGrid, IDisposable
    {
        /// <summary>
        /// Identifies the DemandDataSourceLoadedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent DemandDataSourceLoadedEvent = EventManager.RegisterRoutedEvent("DemandDataSourceLoaded", RoutingStrategy.Bubble, typeof(GridDataOnDemandPageLoadingEventHandler), typeof(ProGrid));

        /// <summary>
        /// The _disposed.
        /// </summary>
        bool _disposed;

        /// <summary>
        /// Has the control loaded completely
        /// </summary>
        bool _loaded = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic"/> class.
		/// </summary>
        public ProGridClassic()
        {
            // Initialize event handlers
            base.ProGridControl.Loaded += ProGridControl_Loaded;
            base.GridPanel.SizeChanged += GridPanel_SizeChanged;
            this.PropertyChanged += ProGridClassic_PropertyChanged;
        }

        #region IDisposable Implementation: Methods
        /// <summary>
        /// Releases all resource used by the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic"/> object.
        /// </summary>
        /// <remarks>Call <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic.Dispose()"/> when you are finished using the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic"/>.
        /// The <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic.Dispose()"/> method leaves the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic"/> in an unusable
        /// state. After calling <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic.Dispose()"/>, you must release all references to the
        /// <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic"/> so the garbage collector can reclaim the memory that the
        /// <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic"/> was occupying.</remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridClassic"/> is reclaimed by garbage collection.
        /// </summary>
        ~ProGridClassic()
        {
            Dispose(false);
        }

        /// <summary>
        /// Dispose the specified disposing.
        /// </summary>
        /// <param name="disposing">If set to <c>true</c> disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                if (_gridData != null)
                {
                    _gridData = null;
                }
            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
        #endregion

        #region IProGrid Implementation: Methods
        /// <summary>
		/// Add columns to the datagrid
		/// </summary>
		public void InitializeGrid()
		{
			try
			{
				if ( base.GridColumns != null && GridData.Columns.Count == 0 )
				{
					PropertyInfo[] properties = base.GridColumns.GetType ().GetProperties ();
					foreach ( PropertyInfo prop in properties )
					{
                        bool fieldEnabled = true;

                        // skip processing of the properties that should be ignored
                        if (prop.GetCustomAttributes(typeof(IgnoreAttribute), true).Any() ||
                            prop.GetCustomAttributes(typeof(HideInGridAttribute), true).Any())
                        {
                            continue;
                        }

						ColumnAttribute columnAtt = (ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).GetValue(0);

						// records that are both a primary key and a foreign key can not be edited
						// auto increment and readonly fields can not be edited
						if ((columnAtt.PrimaryKey && columnAtt.ForeignKey) || columnAtt.AutoIncrement || columnAtt.ReadOnly)
						{
							fieldEnabled = false;
						}

                        string title = prop.Name;
                        string name = title.ToLower(CultureInfo.CurrentCulture);
                        Type type = prop.PropertyType;

						// if the property has an alternate mapping name use it instead
						// of the property name itself
						if (!String.IsNullOrEmpty(columnAtt.Name))
						{
							name = columnAtt.Name;
						}

						// if the property has an alternate title use it instead
						// of the property name itself
						if (!String.IsNullOrEmpty(columnAtt.Title))
						{
							title = columnAtt.Title;
						}

                        Binding binder = new Binding();
                        binder.Path = new PropertyPath(name);
                        binder.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                        binder.Mode = BindingMode.TwoWay;

						if (columnAtt.ForeignKey)
                        {
                            ListEntry filter = new ListEntry();

                            // skip processing of the properties that should be ignored
                            if (prop.GetCustomAttributes(typeof(FilterAttribute), true).Any())
                            {
                                FilterAttribute filterAtt = (FilterAttribute)prop.GetCustomAttributes(typeof(FilterAttribute), true).GetValue(0);
                                filter.DisplayName = filterAtt.Field;
                                filter.Source = filterAtt.Value;
                            }

                            List<ListEntry> fkList = Reflect.GetForeignKeyPropertiesCollection((DataTable)base.ItemsSource, name, filter);
                            
                            // If the list returned items then this is a combo box
                            // else set box to textbox
                            if (fkList.Count > 0)
                            {
                                DataGridComboBoxColumn col = new DataGridComboBoxColumn();
                                col.Header = title;
                                col.Width = new DataGridLength(0, DataGridLengthUnitType.SizeToHeader);
                                col.IsReadOnly = !fieldEnabled;

                                // if the property has an autosize attribute set the column size
                                // to the size of the cells
								if (columnAtt.AutoSize)
                                {
                                    col.Width = new DataGridLength(150, DataGridLengthUnitType.Auto);
                                }
                                // Make the column sortable if it contains the Sort attribute
                                if (columnAtt.Sort)
                                {
                                    col.SortMemberPath = name;
                                    col.SortDirection = ListSortDirection.Ascending;
                                }

                                col.ItemsSource = Reflect.GetForeignKeyPropertiesCollection((DataTable)base.ItemsSource, name, filter);
                                col.DisplayMemberPath = ListEntry.DisplayMember;
                                col.SelectedValuePath = ListEntry.ValueMember;
                                col.SelectedValueBinding = binder;

                                GridData.Columns.Add(col);
                            }
                            else
                            {
                                DataGridTextColumn col = new DataGridTextColumn();
                                col.Header = title;
                                col.Binding = binder;
                                col.Width = new DataGridLength(0, DataGridLengthUnitType.SizeToHeader);
                                col.IsReadOnly = !fieldEnabled;

                                // if the property has an autosize attribute set the column size
                                // to the size of the cells
								if (columnAtt.AutoSize)
                                {
                                    col.Width = new DataGridLength(0, DataGridLengthUnitType.SizeToCells);
                                }
                                // Make the column sortable if it contains the Sort attribute
                                if (columnAtt.Sort)
                                {
                                    col.SortMemberPath = name;
                                    col.SortDirection = ListSortDirection.Ascending;
                                }

                                GridData.Columns.Add(col);
                            }

                        }
                        else if (type.BaseType == typeof(Enum) || type == typeof(Enum))
						{
							DataGridComboBoxColumn col = new DataGridComboBoxColumn ();
                            col.Header = title;
							col.Width = new DataGridLength ( 0, DataGridLengthUnitType.SizeToHeader );
                            col.IsReadOnly = !fieldEnabled;

                            // if the property has an autosize attribute set the column size
                            // to the size of the cells
							if (columnAtt.AutoSize)
                            {
                                col.Width = new DataGridLength(150, DataGridLengthUnitType.Auto);
                            }
                            // Make the column sortable if it contains the Sort attribute
                            if (columnAtt.Sort)
                            {
                                col.SortMemberPath = name;
                                col.SortDirection = ListSortDirection.Ascending;
                            }

                            col.ItemsSource = EnumToValueConverter.ConvertToList(type, true); //Database stores one char values
                            col.DisplayMemberPath = ListEntry.DisplayMember;
                            col.SelectedValuePath = ListEntry.ValueMember;
                            col.SelectedValueBinding = binder;

							GridData.Columns.Add ( col );
						}
						else if ( type.BaseType == typeof( Color ) || type == typeof( Color ) )
						{
							DataGridTemplateColumn col = new DataGridTemplateColumn ();
                            col.Header = title;
							col.Width = new DataGridLength ( 0, DataGridLengthUnitType.SizeToHeader );
                            col.IsReadOnly = !fieldEnabled;

                            // if the property has an autosize attribute set the column size
                            // to the size of the cells
							if (columnAtt.AutoSize)
                            {
                                col.Width = new DataGridLength(150, DataGridLengthUnitType.Auto);
                            }
                            // Make the column sortable if it contains the Sort attribute
                            if (columnAtt.Sort)
                            {
                                col.SortMemberPath = name;
                                col.SortDirection = ListSortDirection.Ascending;
                            }

                            col.CellTemplate = DataTemplates.ColorPickerTemplate(name);
                            col.CellEditingTemplate = DataTemplates.ColorPickerTemplate(name);

							GridData.Columns.Add ( col );
						}
                        else
						{
							DataGridTextColumn col = new DataGridTextColumn ();
                            col.Header = title;
                            col.Binding = binder;
							col.Width = new DataGridLength ( 0, DataGridLengthUnitType.SizeToHeader );
                            col.IsReadOnly = !fieldEnabled;

                            // if the property has an autosize attribute set the column size
                            // to the size of the cells
							if (columnAtt.AutoSize)
                            {
                                col.Width = new DataGridLength(0, DataGridLengthUnitType.SizeToCells);
                            }
                            // Make the column sortable if it contains the Sort attribute
                            if (columnAtt.Sort)
                            {
                                col.SortMemberPath = name;
                                col.SortDirection = ListSortDirection.Ascending;
                            }

                            GridData.Columns.Add(col);
						}
					}
				}
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

		/// <summary>
		/// Initialize datagrid style
		/// </summary>
		public void InitializeGridStyle()
		{
			GridData.RowHeight = ViewProperties.GridRowHeight;
		}

		/// <summary>
		/// Sets the number of pages.
		/// </summary>
		public void InitializeGridPager()
		{
			try
			{
				PageCount = ( int )Math.Ceiling ( ( decimal )RecordCount / ( decimal )ViewProperties.GridPageSize );
				PageSize = ViewProperties.GridPageSize;
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

		/// <summary>
		/// Reinitialize control
		/// </summary>
		public void Refresh()
		{
            // Only allow refresh if the control has loaded completely
            if (_loaded)
            {
                InitializeGridPager();
                InitializeGridStyle();
            }
        }
		#endregion

        #region IProGrid Implementation: Properties
        /// <summary>
        /// Gets or sets a value for the PageSize of the data pager
		/// </summary>
		public double RowHeight
		{
			get { return GridData.RowHeight; }
			set { GridData.RowHeight = value; }
		}


		/// <summary>
		/// Gets or sets a value for the PageSize of the data pager
		/// </summary>
		public int PageSize
		{
			get { return GridPager.PageSize; }
			set { GridPager.PageSize = value; }
		}


		/// <summary>
		/// Gets or sets a value for the PageCount of the data pager
		/// </summary>
		public int PageCount
		{
			get { return GridPager.PageCount; }
			set
			{ 
				GridPager.PageCount = value;
				GridPager.NumericButtonCount = value;
			}
		}

		/// <summary>
		/// Gets or sets a value for the RecordOffset of the data pager
		/// </summary>
		public int RecordOffset
		{
            get { return GridPager.RecordOffset; }
            set { GridPager.RecordOffset = value; }
		}

		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the grid data.
		/// </summary>
		/// <value>The grid data.</value>
		private DataGrid _gridData;
		private DataGrid GridData
		{
			set { _gridData = value; }
			get
			{
				if ( _gridData == null )
				{
					_gridData = new DataGrid ();
					_gridData.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
					_gridData.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
					_gridData.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
					_gridData.VerticalContentAlignment = System.Windows.VerticalAlignment.Top;

                    Binding binder = new Binding();
                    binder.Source = this;
                    binder.Path = new PropertyPath("ItemsSource");
                    binder.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    _gridData.SetBinding(DataGrid.ItemsSourceProperty, binder);
                    
                    _gridData.SelectedItem = base.SelectedItem;
					_gridData.Margin = new Thickness ( 0, 0, 10, 0 );
                    _gridData.HeadersVisibility = ShowGridHeaders ? DataGridHeadersVisibility.All : DataGridHeadersVisibility.None;

					ContextMenu menu = new ContextMenu ();
					menu.Items.Add ( base.AddMenu );
					menu.Items.Add ( base.EditMenu );
					menu.Items.Add ( base.DeleteMenu );
					_gridData.ContextMenu = menu;

					Grid.SetRow ( _gridData, 2 );
                    Grid.SetColumnSpan(_gridData, 2);
					base.GridPanel.Children.Add ( _gridData );

					_gridData.CellEditEnding += GridData_CellEditEnding;
					_gridData.SelectedCellsChanged += GridData_SelectedCellsChanged;
                    _gridData.MouseDoubleClick += GridData_MouseDoubleClick;
                    _gridData.KeyDown += GridData_KeyDown;
                }
				return _gridData;
			}
		}

        /// <summary>
		/// Gets or sets the grid pager.
		/// </summary>
		/// <value>The grid pager.</value>
		private DataPagerCustom _gridPager;
        private DataPagerCustom GridPager
		{
			get
			{
				if ( _gridPager == null )
				{
                    _gridPager = new DataPagerCustom();
					_gridPager.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
					_gridPager.VerticalAlignment = System.Windows.VerticalAlignment.Top;
					_gridPager.AutoEllipsis = true;
					_gridPager.IsPagingOnDemand = true;
                    _gridPager.DisplayMode = PagerDisplayMode.FirstLastPreviousNextNumeric;
					_gridPager.Background = new SolidColorBrush ( Colors.Transparent );
					_gridPager.Margin = new Thickness ( 0, 0, 30, 0 );
                    _gridPager.Visibility = ShowDataPager ? Visibility.Visible : Visibility.Collapsed;

                    Binding binder = new Binding();
                    binder.Source = GridData;
                    binder.Path = new PropertyPath("ItemsSource");
                    binder.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    _gridPager.SetBinding(DataPagerCustom.SourceProperty, binder);
                    
                    Grid.SetRow(_gridPager, 1);
                    Grid.SetColumn( _gridPager, 1 );
                    base.GridPanel.Children.Add(_gridPager);

					_gridPager.OnDemandDataSourceLoad += GridPager_OnDemandDataSourceLoad;
				}

				return _gridPager;
			}
		}

        /// <summary>
        /// flag used to prevent re-entry into GridData_CellEditEnding when the
        /// DataGrid is manually committed 
        /// </summary>
        private bool _isManualEditCommit { get; set; }
		#endregion

		#region Events
		/// <summary>
		/// Handler for the DataPagerLoaded event
		/// </summary>
		public event GridDataOnDemandPageLoadingEventHandler DemandDataSourceLoaded
		{
            add { this.AddHandler(DemandDataSourceLoadedEvent, value); }
            remove { this.RemoveHandler(DemandDataSourceLoadedEvent, value); }
		}

		#endregion

		#region Listeners
		/// <summary>
		/// Occurs this control is loaded
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void ProGridControl_Loaded( object sender, RoutedEventArgs e )
		{
			InitializeGrid ();
			InitializeGridPager ();
			InitializeGridStyle ();

            _loaded = true;
        }

        /// <summary>
        /// Handle property changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProGridClassic_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("RecordCount"))
            {
                Refresh();
            }
        }
        
        /// <summary>
		/// Occurs when the data pager is loaded
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void GridPager_OnDemandDataSourceLoad( object sender, GridDataOnDemandPageLoadingEventArgs e )
		{
            if (sender.GetType() == typeof(DataPagerCustom))
			{
                DataPagerCustom pager = sender as DataPagerCustom;
				// Now notify anyone listening that the pager is loaded on the ProDataGrid
				OnDemandDataSourceLoaded ( pager, e );

				InitializeGridStyle (); //Reset Gridrow Height
			}
		}

		/// <summary>
		/// Raises the row selection event for data grid control.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void GridData_SelectedCellsChanged( object sender, SelectedCellsChangedEventArgs e )
		{
            if (sender.GetType() == typeof(DataGrid))
            {
                DataGrid grid = sender as DataGrid;

                if (grid.SelectedItems.Count == 1)
                {
                    ToggleButtons(true, true, true);
                }
                else if (grid.SelectedItems.Count > 1)
                {
                    ToggleButtons(true, false, true);
                }
                else
                {
                    ToggleButtons(true, false, false);
                }

                base.SelectedItem = grid.SelectedItem;
                base.SelectedItems = grid.SelectedItems;
            }
		}

        /// <summary>
        /// Event to alert anyone that the mouse double clicked a record
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">e</param>
        private void GridData_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (base.SelectedItems != null)
            {
                base.OnEditButtonClicked(sender, e);
            }
        }

        /// <summary>
        /// Event to alert anyone that the delete button was pressed
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">e</param>
        private void GridData_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (base.SelectedItems != null && (e.Key == System.Windows.Input.Key.Delete || e.Key == System.Windows.Input.Key.Back))
            {
                base.OnDeleteButtonClicked(sender, e);
            }
        }
      
        /// <summary>
		/// Event to change the data grid's height as the grid's height changes
		/// This was needed since the data grid would not snap to the grid with column and row
		/// definitions defined 
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void GridPanel_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (base.GridPanel != null)
			{
				GridData.Height = base.GridPanel.RenderSize.Height - 90;
			}
		}

        /// <summary>
		/// Occurs when the content of a cell is changed
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void GridData_CellEditEnding( object sender, DataGridCellEditEndingEventArgs e )
		{
            if (!_isManualEditCommit)
            {
                _isManualEditCommit = true;
                DataGrid grid = (DataGrid)sender;
                grid.CommitEdit(DataGridEditingUnit.Row, true); // Force commit
                base.OnRecordUpdated(sender, e); //Update the record in the database
                _isManualEditCommit = false;
            }
        }
		#endregion

		#region Event Raisers
		/// <summary>
		/// Raises the demand data source loaded event.
		/// </summary>
		/// <param name="pager">Pager.</param>
		/// <param name="e">E.</param>
        private void OnDemandDataSourceLoaded(DataPagerCustom pager, GridDataOnDemandPageLoadingEventArgs e)
		{
			try
			{
				RecordOffset = e.PagedRows;

				GridDataOnDemandPageLoadingEventArgs args = new GridDataOnDemandPageLoadingEventArgs ( RecordOffset, PageSize );
				args.RoutedEvent = DemandDataSourceLoadedEvent;
				RaiseEvent ( args );
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

		#endregion
	}
}
