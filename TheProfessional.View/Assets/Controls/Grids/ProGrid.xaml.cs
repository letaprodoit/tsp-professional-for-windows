using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Data;
using System.Windows.Data;
using FirstFloor.ModernUI.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using TheProfessional.ViewModel;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using System.Globalization;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.View.Assets.Controls.Grids
{
	/// <summary>
	/// Pro grid event handler.
	/// </summary>
    public delegate void ProGridEventHandler(object sender, ProGridEventArgs e);
    
    /// <summary>
	/// Event args for the ProGridEventHandler
	/// </summary>
	public class ProGridEventArgs : RoutedEventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridEventArgs"/> class.
		/// </summary>
		/// <param name="routedEvent">Routed event.</param>
		public ProGridEventArgs( RoutedEvent routedEvent ) : base ( routedEvent )
		{
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGridEventArgs"/> class.
		/// </summary>
		/// <param name="routedEvent">Routed event.</param>
		/// <param name="source">Source.</param>
		public ProGridEventArgs( RoutedEvent routedEvent, object source ) : base ( routedEvent, source )
		{
		}
		/// <summary>
		/// Gets or sets the seleted items.
		/// </summary>
		/// <value>The seleted items.</value>
		public object SeletedItems { get; set; }
	}

	/// <summary>
	/// Commands associated with this control
	/// </summary>
	public static class ProGridCommands
	{
        
		private static InputGestureCollection _gestures;
		/// <summary>
		/// Gets the gestures.
		/// </summary>
		/// <value>The gestures.</value>
        private static InputGestureCollection Gestures
        {
            get
            {
                if (_gestures == null)
                {
                    // Initialize the command.
                    _gestures = new InputGestureCollection();
                    _gestures.Add(new MouseGesture(MouseAction.RightClick));
                }
                return _gestures;
            }
        }

		private static RoutedUICommand _add;
		/// <summary>
		/// Gets or sets the add command.
		/// </summary>
		/// <value>The add command.</value>
		public static RoutedUICommand Add
		{
			get 
			{
				if (_add == null)
				{
                    _add = new RoutedUICommand(LibraryProperties.Resources.LabelName_ADD,
                        LibraryProperties.Resources.LabelName_ADD, typeof(ProGridCommands), Gestures);
				}
				return _add; 
			}
			set { _add = value; }
		}

		private static RoutedUICommand _edit;
		/// <summary>
		/// Gets or sets the edit command.
		/// </summary>
		/// <value>The edit command.</value>
		public static RoutedUICommand Edit
		{
			get
			{
				if (_edit == null)
				{
                    _edit = new RoutedUICommand(LibraryProperties.Resources.LabelName_EDIT,
                        LibraryProperties.Resources.LabelName_EDIT, typeof(ProGridCommands), Gestures);
				}
				return _edit;
			}
			set { _edit = value; }
		}

		private static RoutedUICommand _delete;
		/// <summary>
		/// Gets or sets the delete command.
		/// </summary>
		/// <value>The delete command.</value>
		public static RoutedUICommand Delete
		{
			get
			{
				if (_delete == null)
				{
                    _delete = new RoutedUICommand(LibraryProperties.Resources.LabelName_DELETE,
                        LibraryProperties.Resources.LabelName_DELETE, typeof(ProGridCommands), Gestures);
				}
				return _delete;
			}
			set { _delete = value; }
		}
	}

	/// <summary>
	/// Interaction logic for ProGrid.xaml
	/// </summary>
    public partial class ProGrid : ControlView
	{
		/// <summary>
		/// Identifies the ShowAddRecord dependency property.
		/// </summary>
		public static readonly DependencyProperty ShowAddButtonProperty = DependencyProperty.Register ( "ShowAddButton", typeof( bool ), typeof( ProGrid ), new PropertyMetadata ( true ) );
		/// <summary>
		/// Identifies the ShowEditButton dependency property.
		/// </summary>
		public static readonly DependencyProperty ShowEditButtonProperty = DependencyProperty.Register ( "ShowEditButton", typeof( bool ), typeof( ProGrid ), new PropertyMetadata ( true ) );
		/// <summary>
		/// Identifies the ShowDeleteRecord dependency property.
		/// </summary>
		public static readonly DependencyProperty ShowDeleteButtonProperty = DependencyProperty.Register ( "ShowDeleteButton", typeof( bool ), typeof( ProGrid ), new PropertyMetadata ( true ) );
        /// <summary>
        /// Identifies the ShowDataPager dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowDataPagerProperty = DependencyProperty.Register("ShowDataPager", typeof(bool), typeof(ProGrid), new PropertyMetadata(true));
        /// <summary>
        /// Identifies the ShowGridHeaders dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowGridHeadersProperty = DependencyProperty.Register("ShowGridHeaders", typeof(bool), typeof(ProGrid), new PropertyMetadata(true));
        /// <summary>
		/// Identifies the TitleProperty dependency property.
		/// </summary>
		public static readonly DependencyProperty TitleProperty = DependencyProperty.Register ( "Title", typeof( string ), typeof( ProGrid ) );
		/// <summary>
		/// Identifies the TextProperty dependency property.
		/// </summary>
		public static readonly DependencyProperty TextProperty = DependencyProperty.Register ( "Text", typeof( string ), typeof( ProGrid ) );
		/// <summary>
		/// Identifies the RecordCountProperty dependency property.
		/// </summary>
		public static readonly DependencyProperty RecordCountProperty = DependencyProperty.Register ( "RecordCount", typeof( int ), typeof( ProGrid ) );
		/// <summary>
		/// Identifies the ItemsSourceProperty dependency property.
		/// </summary>
		public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register ( "ItemsSource", typeof( object ), typeof( ProGrid ) );
        /// <summary>
		/// Identifies the SelectedItemProperty dependency property.
		/// </summary>
		public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register ( "SelectedItem", typeof( object ), typeof( ProGrid ) );
		/// <summary>
		/// Identifies the GridColumnsProperty dependency property.
		/// </summary>
		public static readonly DependencyProperty GridColumnsProperty = DependencyProperty.Register ( "GridColumns", typeof( object ), typeof( ProGrid ) );
		/// <summary>
		/// Identifies the AddButtonClickedEvent routed event.
		/// </summary>
		public static readonly RoutedEvent AddButtonClickedEvent = EventManager.RegisterRoutedEvent ( "AddButtonClicked", RoutingStrategy.Bubble, typeof( ProGridEventHandler ), typeof( ProGrid ) );
		/// <summary>
		/// Identifies the EditButtonClickedEvent routed event.
		/// </summary>
		public static readonly RoutedEvent EditButtonClickedEvent = EventManager.RegisterRoutedEvent ( "EditButtonClicked", RoutingStrategy.Bubble, typeof( ProGridEventHandler ), typeof( ProGrid ) );
		/// <summary>
		/// Identifies the DeleteButtonClickedEvent routed event.
		/// </summary>
		public static readonly RoutedEvent DeleteButtonClickedEvent = EventManager.RegisterRoutedEvent ( "DeleteButtonClicked", RoutingStrategy.Bubble, typeof( ProGridEventHandler ), typeof( ProGrid ) );
        /// <summary>
        /// Identifies the RecordUpdatedEvent routed event.
        /// </summary>
        public static readonly RoutedEvent RecordUpdatedEvent = EventManager.RegisterRoutedEvent("RecordUpdated", RoutingStrategy.Bubble, typeof(ProGridEventHandler), typeof(ProGrid));

		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.Grids.ProGrid"/> class.
		/// </summary>
		public ProGrid()
		{
			InitializeComponent ();

			this.AddButton.Click += AddButton_Click;
			this.EditButton.Click += EditButton_Click;
			this.DeleteButton.Click += DeleteButton_Click;

			// Initialize commands
            this.AddMenu.Command = ProGridCommands.Add;
            this.EditMenu.Command = ProGridCommands.Edit;
            this.DeleteMenu.Command = ProGridCommands.Delete;

			// Initialize command bindings
            this.AddMenu.CommandBindings.Add(new CommandBinding(ProGridCommands.Add, AddButton_Click, AddCommand_CanExecute));
            this.EditMenu.CommandBindings.Add(new CommandBinding(ProGridCommands.Edit, EditButton_Click, EditCommand_CanExecute));
            this.DeleteMenu.CommandBindings.Add(new CommandBinding(ProGridCommands.Delete, DeleteButton_Click, DeleteCommand_CanExecute));
            
            // Add a listener to RecordCount changes
            DependencyPropertyDescriptor pd = DependencyPropertyDescriptor.FromProperty(RecordCountProperty, typeof(ProGrid)); 
            pd.AddValueChanged(this, RecordCountProperty_Changed);
        }

		#region Methods
		/// <summary>
		/// Toggles the buttons.
		/// </summary>
		/// <param name="addStatus">If set to <c>true</c> add status.</param>
		/// <param name="editStatus">If set to <c>true</c> edit status.</param>
		/// <param name="deleteStatus">If set to <c>true</c> delete status.</param>
		protected void ToggleButtons( bool addStatus, bool editStatus, bool deleteStatus )
		{
			AddEnabled = addStatus;
			EditEnabled = editStatus;
			DeleteEnabled = deleteStatus;
		} 
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether the add record button is enabled
		/// </summary>
		private bool AddEnabled
		{
			get { return this.AddButton.IsEnabled; }
			set { this.AddButton.IsEnabled = value; }
		}


		/// <summary>
		/// Gets or sets a value indicating whether the edit record button is enabled
		/// </summary>
		private bool EditEnabled
		{
			get { return this.EditButton.IsEnabled; }
			set { this.EditButton.IsEnabled = value; }
		}


		/// <summary>
		/// Gets or sets a value indicating whether the delete record button is enabled
		/// </summary>
		private bool DeleteEnabled
		{
			get { return this.DeleteButton.IsEnabled; }
			set { this.DeleteButton.IsEnabled = value; }
		}
			

		private MenuItem _addMenu;
        /// <summary>
        /// Gets or sets the add menu.
        /// </summary>
        /// <value>The add menu.</value>
        protected MenuItem AddMenu
		{
			set { _addMenu = value; }
			get
			{
				if ( _addMenu == null )
				{
					_addMenu = new MenuItem ();
					_addMenu.Header = "Add";

					Uri uri = new Uri ( @"../../Assets/Images/Add.png", UriKind.Relative );
					BitmapImage bitmap = new BitmapImage ( uri );
					Image image = new Image ();
					image.Source = bitmap;

					_addMenu.Icon = image;
				}
				return _addMenu;
			}
		}


		private MenuItem _editMenu;
        /// <summary>
        /// Gets or sets the edit menu.
        /// </summary>
        /// <value>The edit menu.</value>
        protected MenuItem EditMenu
		{
			set { _editMenu = value; }
			get
			{
				if ( _editMenu == null )
				{
					_editMenu = new MenuItem ();
					_editMenu.Header = "Edit";

					Uri uri = new Uri ( @"../../Assets/Images/Edit.png", UriKind.Relative );
					BitmapImage bitmap = new BitmapImage ( uri );
					Image image = new Image ();
					image.Source = bitmap;

					_editMenu.Icon = image;
				}
				return _editMenu;
			}
		}


		private MenuItem _deleteMenu;
        /// <summary>
        /// Gets or sets the delete menu.
        /// </summary>
        /// <value>The delete menu.</value>
        protected MenuItem DeleteMenu
		{
			set { _deleteMenu = value; }
			get
			{
				if ( _deleteMenu == null )
				{
					_deleteMenu = new MenuItem ();
					_deleteMenu.Header = "Delete";

					Uri uri = new Uri ( @"../../Assets/Images/Delete.png", UriKind.Relative );
					BitmapImage bitmap = new BitmapImage ( uri );
					Image image = new Image ();
					image.Source = bitmap;

					_deleteMenu.Icon = image;
				}
				return _deleteMenu;
			}
		}


        /// <summary>
		/// Gets or sets the selected items.
		/// </summary>
		/// <value>The selected items.</value>
		protected IList SelectedItems { get; set; }


		/// <summary>
		/// Gets or sets a value for the title of the datagrid
		/// </summary>
		public string Title
		{
			get { return ( string )GetValue ( TitleProperty ); }
			set { SetValue ( TitleProperty, value ); }
		}


		/// <summary>
		/// Gets or sets a value for the text of the datagrid
		/// </summary>
		public string Text
		{
			get { return ( string )GetValue ( TextProperty ); }
			set { SetValue ( TextProperty, value ); }
		}


		/// <summary>
		/// Gets or sets a value for the recordcount of the datagrid
		/// </summary>
		public int RecordCount
		{
			get { return ( int )GetValue ( RecordCountProperty ); }
			set { SetValue ( RecordCountProperty, value ); }
		}


		/// <summary>
		/// Gets or sets a value indicating whether the add record button is shown
		/// </summary>
		public bool ShowAddButton
		{
			get { return ( bool )GetValue ( ShowAddButtonProperty ); }
			set { SetValue ( ShowAddButtonProperty, value ); }
		}


		/// <summary>
		/// Gets or sets a value indicating whether the edit record button is shown
		/// </summary>
		public bool ShowEditButton
		{
			get { return ( bool )GetValue ( ShowEditButtonProperty ); }
			set { SetValue ( ShowEditButtonProperty, value ); }
		}


		/// <summary>
		/// Gets or sets a value indicating whether the delete record button is shown
		/// </summary>
		public bool ShowDeleteButton
		{
			get { return ( bool )GetValue ( ShowDeleteButtonProperty ); }
			set { SetValue ( ShowDeleteButtonProperty, value ); }
		}


        /// <summary>
        /// Gets or sets a value indicating whether the datapager is shown
        /// </summary>
        public bool ShowDataPager
        {
            get { return (bool)GetValue(ShowDataPagerProperty); }
            set { SetValue(ShowDataPagerProperty, value); }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the grid headers should be shown
        /// </summary>
        public bool ShowGridHeaders
        {
            get { return (bool)GetValue(ShowGridHeadersProperty); }
            set { SetValue(ShowGridHeadersProperty, value); }
        }
        
        
        /// <summary>
		/// Gets or sets a value for the ItemsSource of the datagrid
		/// </summary>
		public object ItemsSource
		{
			get { return GetValue ( ItemsSourceProperty ); }
			set { SetValue ( ItemsSourceProperty, value ); }
		}


		/// <summary>
		/// Gets or sets a value for the SelectedItem of the datagrid
		/// </summary>
		public object SelectedItem
		{
			get { return GetValue ( SelectedItemProperty ); }
			set { SetValue ( SelectedItemProperty, value ); }
		}


		/// <summary>
		/// Gets or sets a value for the GridColumns of the datagrid
		/// </summary>
		public object GridColumns
		{
			get { return GetValue ( GridColumnsProperty ); }
			set { SetValue ( GridColumnsProperty, value ); }
		}

		#endregion

		#region Events

		/// <summary>
		/// Handler for the AddButtonClicked event
		/// </summary>
        public event ProGridEventHandler AddButtonClicked
		{
			add
			{
				// Buttons
				this.AddButton.AddHandler ( AddButtonClickedEvent, value );

				// Right click menus
				this.AddMenu.AddHandler ( AddButtonClickedEvent, value );
			}
			remove
			{
				// Buttons
				this.AddButton.RemoveHandler ( AddButtonClickedEvent, value );

				// Right click menus
				this.AddMenu.RemoveHandler ( AddButtonClickedEvent, value );
			}
		}


		/// <summary>
		/// Handler for the EditButtonClicked event
		/// </summary>
        public event ProGridEventHandler EditButtonClicked
		{
			add
			{
				// Buttons
				this.EditButton.AddHandler ( EditButtonClickedEvent, value );

				// Right click menus
				this.EditMenu.AddHandler ( EditButtonClickedEvent, value );
			}
			remove
			{
				// Buttons
				this.EditButton.RemoveHandler ( EditButtonClickedEvent, value );

				// Right click menus
				this.EditMenu.RemoveHandler ( EditButtonClickedEvent, value );
			}
		}


		/// <summary>
		/// Handler for the DeleteButtonClicked event
		/// </summary>
        public event ProGridEventHandler DeleteButtonClicked
		{
			add
			{
				// Buttons
				this.DeleteButton.AddHandler ( DeleteButtonClickedEvent, value );

				// Right click menus
				this.DeleteMenu.AddHandler ( DeleteButtonClickedEvent, value );
			}
			remove
			{
				// Buttons
				this.DeleteButton.RemoveHandler ( DeleteButtonClickedEvent, value );

				// Right click menus
				this.DeleteMenu.RemoveHandler ( DeleteButtonClickedEvent, value );
			}
		}


        /// <summary>
        /// Handler for the RecordUpdated event
        /// </summary>
        public event ProGridEventHandler RecordUpdated
        {
            add
            {
                this.AddHandler(RecordUpdatedEvent, value);
            }
            remove
            {
                this.RemoveHandler(RecordUpdatedEvent, value);
            }
        }

		#endregion

		#region Listeners
        /// <summary>
        /// Raises property changed when RecordCount changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecordCountProperty_Changed(object sender, EventArgs e)
        {
            RecordCountLabel.Text = String.Format(LibraryProperties.Resources.LabelName_RECORD_COUNT, RecordCount);
            RaisePropertyChanged("RecordCount");
        }
        
        /// <summary>
		/// Determines if the add button can be executed
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void AddCommand_CanExecute( object sender, CanExecuteRoutedEventArgs e )
		{
			e.CanExecute = true;
		}


		/// <summary>
		/// Occurs when the add button is clicked
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void AddButton_Click( object sender, RoutedEventArgs e )
		{
			// Now notify anyone listening that the button was clicked on the DataGridMenu
			OnAddButtonClicked ( sender, e );
		}


		/// <summary>
		/// Determines if the edit button can be executed
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void EditCommand_CanExecute( object sender, CanExecuteRoutedEventArgs e )
		{
			e.CanExecute = EditEnabled;
		}


		/// <summary>
		/// Occurs when the edit button is clicked
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void EditButton_Click( object sender, RoutedEventArgs e )
		{
			// Now notify anyone listening that the button was clicked on the DataGridMenu
			OnEditButtonClicked ( sender, e );
		}


		/// <summary>
		/// Determines if the delete button can be executed
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void DeleteCommand_CanExecute( object sender, CanExecuteRoutedEventArgs e )
		{
			e.CanExecute = DeleteEnabled;
		}


		/// <summary>
		/// Occurs when the delete button is clicked
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void DeleteButton_Click( object sender, RoutedEventArgs e )
		{
			// Now notify anyone listening that the button was clicked on the DataGridMenu
			OnDeleteButtonClicked ( sender, e );
		}

		#endregion

		#region Event Raisers
		/// <summary>
		/// Raises the add button clicked event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
        internal void OnAddButtonClicked(object sender, RoutedEventArgs e)
		{
			try
			{
				ProGridEventArgs args = new ProGridEventArgs ( AddButtonClickedEvent );
				RaiseEvent ( args );
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }


		/// <summary>
		/// Raises the edit button clicked event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
        internal void OnEditButtonClicked(object sender, RoutedEventArgs e)
		{
			try
			{
				ProGridEventArgs args = new ProGridEventArgs ( EditButtonClickedEvent );
				args.SeletedItems = SelectedItems;
				RaiseEvent ( args );
			}
            catch (Exception ex) 
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message); 
            }
		}


		/// <summary>
		/// Raises the delete button clicked event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
        internal void OnDeleteButtonClicked(object sender, RoutedEventArgs e)
		{
			try
			{
				ProGridEventArgs args = new ProGridEventArgs ( DeleteButtonClickedEvent );
				args.SeletedItems = SelectedItems;
				RaiseEvent ( args );
			}
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }


        /// <summary>
        /// Raises the record updated event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Type EventArgs.</param>
        internal void OnRecordUpdated(object sender, EventArgs e)
        {
            try
            {
                ProGridEventArgs args = new ProGridEventArgs(RecordUpdatedEvent);
                args.SeletedItems = SelectedItems;
                RaiseEvent(args);
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Raises the record updated event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Type RoutedEventArgs.</param>
        internal void OnRecordUpdated(object sender, RoutedEventArgs e)
        {
            try
            {
                ProGridEventArgs args = new ProGridEventArgs(RecordUpdatedEvent);
                args.SeletedItems = SelectedItems;
                RaiseEvent(args);
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }
        #endregion
	}
}