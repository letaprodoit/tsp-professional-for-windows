﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace TheProfessional.View.Assets.Controls
{
	/// <summary>
	/// Color picker.
	/// </summary>
    public class ColorPicker : Microsoft.Windows.Controls.ColorPicker
    {
        /// <summary>
        /// Identifies the ColorProperty dependency property.
        /// Colors the picker_ color changed.
        /// </summary>
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(Color), typeof(ColorPicker), new PropertyMetadata(Colors.Transparent));

        /// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.Assets.Controls.ColorPicker"/> class.
		/// </summary>
        public ColorPicker()
		{
            base.SelectedColorChanged += ColorPicker_SelectedColorChanged;
            base.MinWidth = 150;
            base.HorizontalAlignment = HorizontalAlignment.Left;
            base.DisplayColorAndName = true;
            base.ShowAvailableColors = false;
            base.ShowRecentColors = true;
            base.ShowStandardColors = true;
        }

        /// <summary>
        /// Colors the picker_ color changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">E.</param>
        private void ColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {
            base.SelectedColor = (Color)e.NewValue;
            Color = base.SelectedColor;
        }

        /// <summary>
        /// Gets or sets a value for the color of the colorpicker
        /// </summary>
        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set 
            { 
                SetValue(ColorProperty, value);
                base.SelectedColor = value;
            }
        }
    }
}
