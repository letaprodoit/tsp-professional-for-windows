﻿//using Syncfusion.Windows.Controls.Grid;
//using Syncfusion.Windows.Shared;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.ComponentModel;
using System.Windows.Media;
using System.Globalization;
using System.Resources;
using System.Windows.Media.Imaging;

namespace TheProfessional.View
{
	/// <summary>
	/// View properties.
	/// </summary>
    public class ViewProperties
    {
        /// <summary>
        /// The Primary professional's name throughout the application
        /// </summary>
        public string PrimaryProfessionalName
        {
            get { return "Sharron Denice"; }
        }

        /// <summary>
        /// The Primary professional's company throughout the application
        /// </summary>
        public string PrimaryProfessionalCompany
        {
            get { return "The Software People"; }
        }

        /// <summary>
        /// The Primary professional's avatar used throughout the application
        /// </summary>
        public BitmapImage PrimaryProfessionalAvatar
        {
            get { return Library.LibraryFuncs.SetAvatarImage( "sharron@me.com", false); }
        }

        /// <summary>
        /// The Primary professional ID throughout the application
        /// </summary>
        public int PrimaryProfessional { get; set; }

        /// <summary>
        /// The Current company ID throughout the application
        /// </summary>
        public int CurrentCompany { get; set; }

        /// <summary>
        /// The Current professional ID throughout the application
        /// </summary>
        public int CurrentProfessional { get; set; }

        /// <summary>
        /// The Current project ID throughout the application
        /// </summary>
        public int CurrentProject { get; set; }

        /// <summary>
        /// The Current task throughout the application
        /// </summary>
        public int CurrentTask { get; set; }

        private static double _gridRowHeight;
        /// <summary>
        /// The default row height for grids
        /// </summary>
        public static double GridRowHeight
        {
            get { return _gridRowHeight; }
            set
            {
                _gridRowHeight = value;

                if (!_gridRowHeight.Equals(Properties.Settings.Default.DataControls_GridRowHeight))
                {
                    Properties.Settings.Default.DataControls_GridRowHeight = _gridRowHeight;
                }
            }
        }

        private static double _windowWidth;
        /// <summary>
        /// The window's default width
        /// </summary>
        public static double WindowWidth
        {
            get { return _windowWidth; }
            set
            {
                _windowWidth = value;

                if (!_windowWidth.Equals(Properties.Settings.Default.MainWindow_DefaultWidth))
                {
                    Properties.Settings.Default.MainWindow_DefaultWidth = _windowWidth;
                }
            }
        }

        private static double _windowHeight;
        /// <summary>
        /// The window's default height
        /// </summary>
        public static double WindowHeight
        {
            get { return _windowHeight; }
            set
            {
                _windowHeight = value;

				if (!_windowHeight.Equals(Properties.Settings.Default.MainWindow_DefaultHeight))
                {
                    Properties.Settings.Default.MainWindow_DefaultHeight = _windowHeight;
                }
            }
        }
        
        private static int _gridPageSize;
        /// <summary>
        /// The page size
        /// </summary>
        public static int GridPageSize
        {
            get { return _gridPageSize; }
            set
            {
                _gridPageSize = value;

				if (!_gridPageSize.Equals(Properties.Settings.Default.DataControls_GridPageSize))
                {
                    Properties.Settings.Default.DataControls_GridPageSize = _gridPageSize;
                }
            }
        }

		private static Uri _dataGridStyle;
        /// <summary>
        /// The grid visual style 
        /// </summary>
		public static Uri DataGridStyle
        {
			get { return _dataGridStyle; }
            set
            {
				_dataGridStyle = value;

				if (!_dataGridStyle.Equals(Properties.Settings.Default.DataControls_DataGrid_Style))
                {
					Properties.Settings.Default.DataControls_DataGrid_Style = _dataGridStyle;
                }
            }
        }


        private static FontSize _appFontSize;
        /// <summary>
        /// The font size
        /// </summary>
        public static FontSize AppFontSize
        {
            get { return _appFontSize; }
            set
            {
                _appFontSize = value;

				if (!_appFontSize.Equals(Properties.Settings.Default.Appearance_FontSize))
                {
                    Properties.Settings.Default.Appearance_FontSize = _appFontSize;
                }
            }
        }

        private static Uri _appThemeSource;
        /// <summary>
        /// The application's theme
        /// </summary>
        public static Uri AppThemeSource
        {
            get { return _appThemeSource; }
            set
            {
                _appThemeSource = value;

				if (!_appThemeSource.Equals(Properties.Settings.Default.Apperance_Theme))
                {
                    Properties.Settings.Default.Apperance_Theme = _appThemeSource;
                }
            }
        }

        /// <summary>
        /// The application's light theme
        /// </summary>
        public static Uri AppLightTheme
        {
            get { return Properties.Settings.Default.Appearance_Light_Theme; }
        }

        /// <summary>
        /// The application's dark theme
        /// </summary>
        public static Uri AppDarkTheme
        {
            get { return Properties.Settings.Default.Appearance_Dark_Theme; }
        }

		/// <summary>
		/// The application's DataGrid's dark theme
		/// </summary>
		public static Uri DataGridDarkTheme
		{
            get { return Properties.Settings.Default.DataGrid_Dark_Theme; }
		}

		/// <summary>
		/// The application's DataGrid's light theme
		/// </summary>
		public static Uri DataGridLightTheme
		{
            get { return Properties.Settings.Default.DataGrid_Light_Theme; }
		}


		/// <summary>
		/// The application's DataGrid's ExpressionDark theme
		/// </summary>
		public static Uri DataGridExpressionDarkTheme
		{
            get { return Properties.Settings.Default.DataGrid_ExpressionDark_Theme; }
		}


		/// <summary>
		/// The application's DataGrid's ExpressionLight theme
		/// </summary>
		public static Uri DataGridExpressionLightTheme
		{
            get { return Properties.Settings.Default.DataGrid_ExpressionLight_Theme; }
		}


		/// <summary>
		/// The application's DataGrid's WhistlerBlue theme
		/// </summary>
		public static Uri DataGridWhistlerBlueTheme
		{
            get { return Properties.Settings.Default.DataGrid_WhistlerBlue_Theme; }
		}

		private static Color _appAccentColor;
        /// <summary>
        /// The accent color
        /// </summary>
        public static Color AppAccentColor
        {
            get { return _appAccentColor; }
            set
            {
                _appAccentColor = value;

                if (!String.Equals(Properties.Settings.Default.Apperance_AccentColor, _appAccentColor.ToString()))
                {
                    Properties.Settings.Default.Apperance_AccentColor = _appAccentColor.ToString();
                }
            }
        }

        private static string _uploadsLocation;
        /// <summary>
        /// Upload directory
        /// </summary>
        public static string UploadsLocation
        {
            get { return _uploadsLocation; }
            set
            {
                _uploadsLocation = value;

                if (!String.Equals(Properties.Settings.Default.UploadsAttachments_Location, _uploadsLocation))
                {
                    Properties.Settings.Default.UploadsAttachments_Location = _uploadsLocation;
                }
            }
        }

        private static bool _firstRun;
        /// <summary>
        /// Is this the first time the application has run
        /// </summary>
        public static bool FirstRun
        {
            get { return _firstRun; }
            set
            {
                _firstRun = value;
                Properties.Settings.Default.FirstRun = _firstRun;
            }
        }

        private static bool _minOnExit;
        /// <summary>
        /// Get/Set Minimize on exit
        /// </summary>
        public static bool MinOnExit
        {
            get { return _minOnExit; }
            set
            {
                if (_minOnExit != value)
                {
                    _minOnExit = value;
                    Properties.Settings.Default.Launch_MinimizeOnExit = value;
                }
            }
        }

        private static bool _autoStartDay;
        /// <summary>
        /// Get/Set automatically start day
        /// </summary>
        public static bool AutoStartDay
        {
            get { return _autoStartDay; }
            set
            {
                if (_autoStartDay != value)
                {
                    _autoStartDay = value;
                    Properties.Settings.Default.Launch_AutoStartDay = value;
                }
            }
        }

        private static bool _winAutoStart;
        /// <summary>
        /// Get/Set automatically start application on windows startup
        /// </summary>
        public static bool WinAutoStart
        {
            get { return _winAutoStart; }
            set
            {
                if (_winAutoStart != value)
                {
                    _winAutoStart = value;
                    Properties.Settings.Default.Launch_WinAutoStart = value;
                }
            }
        }

        private static bool _timeCardOnEndDay;
        /// <summary>
        /// Get/Set automatically complete timecard at end of day
        /// </summary>
        public static bool TimeCardOnEndDay
        {
            get { return _timeCardOnEndDay; }
            set
            {
                if (_timeCardOnEndDay != value)
                {
                    _timeCardOnEndDay = value;
                    Properties.Settings.Default.Launch_TimeCardOnEndDay = value;
                }
            }
        }
    }
}
