﻿//using Syncfusion.Windows.Controls.Grid;
//using Syncfusion.Windows.Shared;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TheProfessional.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.MainWindow"/> class.
		/// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Do not load your data at design time.
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                Properties.Settings.Default.PropertyChanged += SaveSettingsProperty;
                
                this.SizeChanged += ViewWindow_SizeChanged;

                SetWindowSize();

                LoadAppTheme();

                LoadControlsTheme();

                LoadOtherSettings();
            }
        }

        #region Listeners
        /// <summary>
        /// Saves the settings property.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void SaveSettingsProperty(object sender, PropertyChangedEventArgs e)
        {
            Properties.Settings.Default.Save();

            switch (e.PropertyName)
            {
                case "MainWindow_DefaultWidth":
                    this.Width = ViewProperties.WindowWidth;
                    break;
                case "MainWindow_DefaultHeight":
                    this.Height = ViewProperties.WindowHeight;
                    break;
                case "Apperance_AccentColor":
                    AppearanceManager.Current.AccentColor = ViewProperties.AppAccentColor;
                    break;
                case "Apperance_Theme":
                    AppearanceManager.Current.ThemeSource = ViewProperties.AppThemeSource;
                    break;
                case "Appearance_FontSize":
                    AppearanceManager.Current.FontSize = ViewProperties.AppFontSize;
                    break;
                case "FirstRun":
                case "Launch_MinimizeOnExit":
                case "Launch_AutoStartDay":
                case "Launch_WinAutoStart":
                case "Launch_TimeCardOnEndDay":
                case "UploadsAttachments_Location": // Handled by Shared Attachments & the UploadsAttachments class
                case "DataControls_DataGrid_Style":
                    // Now load the new resource
                    ResourceDictionary gridStyle = new ResourceDictionary();
                    gridStyle.Source = new Uri(@"pack://application:,,," + ViewProperties.DataGridStyle.ToString());

                    // Clear all other resources
                    if (Resources.MergedDictionaries.Any())
                    {
                        Resources.MergedDictionaries.Clear();
                    }
                    Resources.MergedDictionaries.Add(gridStyle);
                    break;
                case "DataControls_GridDataControl_Style":  // data grid style updated on load
                case "DataControls_GridPageSize":   // grid page size updated on load
                case "DataControls_GridRowHeight":  // grid row height updated on load
                    break;
            }
        }

        /// <summary>
        /// Saves the settings property for the window size.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void ViewWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // Only store the window width and height when the window is
            // in a normal state
            if (this.WindowState == System.Windows.WindowState.Normal)
            {
                ViewProperties.WindowHeight = e.NewSize.Height;
                ViewProperties.WindowWidth = e.NewSize.Width;
            }
        }
        #endregion

        /// <summary>
        /// Set's the main window's size
        /// </summary>
        private void SetWindowSize()
        {
            // Save the window size
            ViewProperties.WindowHeight = Properties.Settings.Default.MainWindow_DefaultHeight;
            ViewProperties.WindowWidth = Properties.Settings.Default.MainWindow_DefaultWidth;

            this.Width = ViewProperties.WindowWidth;
            this.Height = ViewProperties.WindowHeight;
        }

        /// <summary>
        /// Loads the controls theme.
        /// </summary>
        private void LoadControlsTheme()
        {
            // Save control theme
            ViewProperties.GridPageSize = Properties.Settings.Default.DataControls_GridPageSize;
            ViewProperties.GridRowHeight = Properties.Settings.Default.DataControls_GridRowHeight;

            // Save datagrid styles
            ViewProperties.DataGridStyle = Properties.Settings.Default.DataControls_DataGrid_Style;
        }

        /// <summary>
        /// Loads the app theme.
        /// </summary>
        private static void LoadAppTheme()
        {
            // Save app theme, accent color and fontsize
            ViewProperties.AppThemeSource = Properties.Settings.Default.Apperance_Theme;
            ViewProperties.AppFontSize = Properties.Settings.Default.Appearance_FontSize;
            ViewProperties.AppAccentColor = (Color)ColorConverter.ConvertFromString(Properties.Settings.Default.Apperance_AccentColor);

            // Store values in appearance manager for ModernUI
            AppearanceManager.Current.ThemeSource = ViewProperties.AppThemeSource;
            AppearanceManager.Current.FontSize = ViewProperties.AppFontSize;
            AppearanceManager.Current.AccentColor = ViewProperties.AppAccentColor;
        }

        /// <summary>
        /// Set's all other settings
        /// </summary>
        private void LoadOtherSettings()
        {
            // Save the uploads location
            ViewProperties.UploadsLocation = Properties.Settings.Default.UploadsAttachments_Location;
            ViewProperties.FirstRun = Properties.Settings.Default.FirstRun;
            ViewProperties.MinOnExit = Properties.Settings.Default.Launch_MinimizeOnExit;
            ViewProperties.AutoStartDay = Properties.Settings.Default.Launch_AutoStartDay;
            ViewProperties.WinAutoStart = Properties.Settings.Default.Launch_WinAutoStart;
            ViewProperties.TimeCardOnEndDay = Properties.Settings.Default.Launch_TimeCardOnEndDay;
        }
    }
}
