﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheProfessional.View
{
    /// <summary>
    /// Class initializations for static controls
    /// </summary>
    public class StaticView : BaseView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.StaticView"/> class.
        /// </summary>
        public StaticView()
        {
        }
    }
}
