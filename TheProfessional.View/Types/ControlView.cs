﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheProfessional.View
{
    /// <summary>
    /// Class initializations for custom controls
    /// </summary>
    public class ControlView : BaseView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.View.ControlView"/> class.
        /// </summary>
        public ControlView()
        {
        }
    }
}
