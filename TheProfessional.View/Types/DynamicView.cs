﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheProfessional.ViewModel;
using TheProfessional.View.Assets.Controls.Grids;
using TheProfessional.View.Assets.Dialogs;
using TheProfessional.Library;
using FirstFloor.ModernUI.Windows.Controls;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using System.Windows;
using System.Data;

namespace TheProfessional.View
{
    /// <summary>
    /// Class initializations for dynamic controls
    /// </summary>
	public class DynamicView<T> : BaseView
        where T : class, new()
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.DynamicView{T}"/> class.
		/// </summary>
        public DynamicView()
        {
        }

		#region Properties
        private DataTable _viewModelTable;
        /// <summary>
        /// Get/Set the View's ViewModel table
        /// </summary>
        private DataTable ViewModelTable
        {
            get
            {
                if (_viewModelTable == null)
                {
					_viewModelTable = Reflect<T>.GetPropertyValue("AllRecords") as DataTable;
                }

                return _viewModelTable;
            }
            set { _viewModelTable = value; }
        }
		#endregion

		#region Listeners
		/// <summary>
		/// Occurs when the add button is clicked on the grid
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		public void ProGrid_AddButtonClicked(object sender, ProGridEventArgs e)
		{
			object record = Reflect<T>.GetPropertyValue("NewRecord");

			DataDialog addDialog = new DataDialog(ref record, ViewModelTable, DataDialogType.ADD, LibraryProperties.Resources.InfoMessage_COMPLETE_SAVE);
			// If the dialog was canceled do not save the record
			// else save the changes
			if ((bool)addDialog.ShowDialog())
			{
				Reflect<T>.InvokeMethod("AddRecord", new[] { record } );
			}
		}


		/// <summary>
		/// Occurs when the edit button is clicked on the grid
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
        public void ProGrid_EditButtonClicked(object sender, ProGridEventArgs e)
		{
			object record = Reflect<T>.InvokeMethod("CurrentRecord", new[] { e.SeletedItems});

			DataDialog editDialog = new DataDialog(ref record, ViewModelTable, DataDialogType.EDIT, LibraryProperties.Resources.InfoMessage_COMPLETE_SAVE);
			// If the dialog was canceled do not save the record
			// else save the changes
			if ((bool)editDialog.ShowDialog())
			{
				Reflect<T>.InvokeMethod("EditRecord", new[] { record } );
			}
		}


		/// <summary>
		/// Occurs when the delete button is clicked on the grid
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
        public void ProGrid_DeleteButtonClicked(object sender, ProGridEventArgs e)
		{
			var result = MessageBox.ShowQuestion(LibraryProperties.Resources.ConfirmMessage_CONFIRM_DELETE, LibraryProperties.Resources.LabelName_TITLE_DELETE, true);

			if (result == MessageBoxResult.OK || result == MessageBoxResult.Yes)
			{
				Reflect<T>.InvokeMethod("DeleteRecord", new[] { e.SeletedItems } );
			}
		}

		/// <summary>
		/// Occurs when the data pager is loaded
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
        public void ProGrid_DemandDataSourceLoaded(object sender, GridDataOnDemandPageLoadingEventArgs e)
		{
			Reflect<T>.InvokeMethod("OnDemandDataSourceLoad", new[] { new PageInfo(e.PagedRows, e.MaximumRows) } );
		}

		/// <summary>
		/// Occurs when a record is updated in the grid
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
        public void ProGrid_RecordUpdated(object sender, ProGridEventArgs e)
		{
			object record = Reflect<T>.InvokeMethod("CurrentRecord", new[] { e.SeletedItems } );

			Reflect<T>.InvokeMethod("EditRecord", new[] { record } );
		}
		#endregion
    }
}
