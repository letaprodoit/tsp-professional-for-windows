using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Threading;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using TheProfessional.Library;
using System.Windows.Controls;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;

namespace TheProfessional.View
{    
    /// <summary>
	/// Base view model.
	/// </summary>
	public class BaseView : UserControl, INotifyPropertyChanged
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.BaseView"/> class.
        /// </summary>
		public BaseView()
        {
            // Add all children to the queue not the parent
            if (!this.ToString().Equals("TheProfessional.View.BaseView"))
            {
                // Make sure all children are listening for the control loaded
                this.Loaded += ChildView_Loaded;

                Queue.Add(new ListEntry { DisplayName = this.ToString(), Source = this });
            }

            // And have the parent class monitor the changes
            Queue.CollectionChanged += Queue_CollectionChanged;
        }

        private bool _viewBusy;
        /// <summary>
        /// The status of the control.
        /// </summary>
		public bool ViewBusy
        {
            set
            {
                _viewBusy = value;
				RaisePropertyChanged( "ViewBusy" );
            }
            get { return _viewBusy; }

        }

		private static ObservableCollection<ListEntry> _queue;
		/// <summary>
		/// The queue of children who are busy working
		/// </summary>
		private static ObservableCollection<ListEntry> Queue
		{
			set
			{ 
				_queue = value;
			}
			get
			{
				if (_queue == null)
				{
					_queue = new ObservableCollection<ListEntry>();
				}
				return _queue;
			}

		}

        /// <summary>
        /// Perform actions once the view is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChildView_Loaded(object sender, EventArgs e)
        {
            Queue.Remove(new ListEntry { DisplayName = sender.ToString(), Source = sender });
        }

        /// <summary>
        /// Perform actions once the view is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Queue_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            // If there are no more items in the queue
            // then set the view to ready
            if (!Queue.Any())
            {
				ViewBusy = false;
            }
            else
            {
				ViewBusy = true;
            }
        }

        #region Inteface Implementations
        /// <summary>
        /// Occurs when property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <param name="prop">Property.</param>
        protected virtual void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
        #endregion
    }
}
