﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TheProfessional.View
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : StaticView
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.View.About"/> class.
		/// </summary>
        public About()
        {
            InitializeComponent();
        }
    }
}
