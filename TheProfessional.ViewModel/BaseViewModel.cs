using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Threading;
using TheProfessional.Model;
using TheProfessional.Library.Database;
using TheProfessional.Library;
using System.Diagnostics;
using System.Globalization;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.ViewModel
{    
    /// <summary>
	/// Base view model.
	/// </summary>
    public class BaseViewModel<T> : ObservableViewModel, IViewModel<T>
        where T : class, new()
    {
		/// <summary>
        /// The name of the column that will be used to count the number of records 
        /// </summary>
        protected const string CounterCol = "_row_count";

        /// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.ViewModel.BaseViewModel{T}"/> class.
        /// </summary>
        public BaseViewModel()
        {
            TableName = Reflect<T>.GetPropertyValue("TableName") as string;
            TableSortBy = Reflect<T>.GetPropertyValue("TableSortBy") as string;
        }

        /// <summary>
        /// Gets or sets the current limit
        /// </summary>
        /// <value>the current limit.</value>
		protected static int Limit { get; set; }

        /// <summary>
        /// Gets or sets the current offset
        /// </summary>
        /// <value>the current offset.</value>
		protected static int Offset { get; set; }

        #region IViewModel<T> Implementation: Properities
        /// <summary>
		/// Returns a new blank model
		/// </summary>
		public T NewRecord
		{
			get 
            { 
                // where T: class
                // return null so the type
                // had to be class, new()
                return new T();
            }
		}

		private DataTable _allRecords;
        /// <summary>
        /// Gets or sets all records.
        /// </summary>
        /// <value>All records.</value>
        public DataTable AllRecords
        {
            get 
            {
                if (_allRecords == null)
                {
                    _allRecords = GetAllRecords();
                    NumberRecords = _allRecords.Rows.Count;
                    RaisePropertyChanged("AllRecords");
                }

                return _allRecords; 
            }
            set
            {
                _allRecords = value;
                NumberRecords = _allRecords.Rows.Count;
                RaisePropertyChanged("AllRecords");
            }
        }

		private DataRowView _selectedRecord;
        /// <summary>
        /// Gets or sets the selected record.
        /// </summary>
        /// <value>The selected record.</value>
        public virtual DataRowView SelectedRecord
		{
			get { return _selectedRecord; }
			set
			{
                _selectedRecord = value;
                RaisePropertyChanged("SelectedRecord");
                RaiseSelectedRecordChanged(new ListEntry { DisplayName = TableName, Source = SelectedRecord });
            }
		}

		private int _numberRecords;
        /// <summary>
        /// Gets the number of records - read only
        /// </summary>
        /// <value>The number of records.</value>
        public virtual int NumberRecords
		{
			get { return _numberRecords; }
			set
			{
				_numberRecords = value;
				RaisePropertyChanged ( "NumberRecords" );

			}
		}

		private string _tableName;
		/// <summary>
		/// Gets or sets the model table.
		/// </summary>
		/// <value>The model table.</value>
		public string TableName
		{
			get { return _tableName; }
			set
			{
				_tableName = value;
				RaisePropertyChanged("TableName");

			}
		}


		private string _tableSortBy;
		/// <summary>
		/// Gets or sets the sortby element for the model table.
		/// </summary>
		/// <value>The sortbye element for the model table.</value>
		public string TableSortBy
		{
			get { return _tableSortBy; }
			set
			{
				_tableSortBy = value;
				RaisePropertyChanged("TableSortBy");

			}
		}
        #endregion

        #region IViewModel<T> Implementation: Methods
        /// <summary>
		/// Filter Records
		/// </summary>
		public virtual void FilterRecords()
		{
            String filter = String.Format("SELECT * FROM {0}{1} ORDER BY {2} LIMIT {3}, {4}", TableName, "", TableSortBy, Offset, Limit);

            AllRecords = BaseModel.Database.GetDataTableByAdapter(TableName, filter);
		}

		/// <summary>
        /// Gets the states with description (override base class)
        /// </summary>
        /// <returns>The states.</returns>
		public virtual DataTable GetAllRecords()
        {
            DataTable records = new DataTable();
            records.Locale = CultureInfo.CurrentCulture;

            try
            {
                SQLElement elem = new SQLElement();

                elem.Tables = new[] { TableName };
                elem.OrderBy = TableSortBy;
                elem.Offset = -1;
                elem.Limit = -1;

                // It is important to use this method to return table data because using the table 
                // adapter directly does not yeild Parent and Child relations
                // only direct queries reveal this information
                // Sort table by type
                string query = BaseModel.Database.QueryBuilder(elem);
                records = BaseModel.Database.GetDataTableByAdapter(TableName, query);

                if (!records.Equals(null))
                {
                    PreloadRelatedTables(records);
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return records;
        }

        /// <summary>
        /// Gets the statuses within a certain range.
        /// </summary>
        /// <param name="Offset">The start index.</param>
        /// <param name="Limit">The end index.</param>
        /// <returns></returns>
        internal DataTable GetRecordsFromRange(int Offset, int Limit)
        {
            DataTable records = new DataTable();
            records.Locale = CultureInfo.CurrentCulture;

            try
            {
                SQLElement elem = new SQLElement();

                elem.Tables = new[] { TableName };
                elem.OrderBy = TableSortBy;
                elem.Offset = Offset;
                elem.Limit = Limit;

                // Sort table by type
                string query = BaseModel.Database.QueryBuilder(elem);
                records = BaseModel.Database.GetDataTableByAdapter(TableName, query);

                if (!records.Equals(null))
                {
                    PreloadRelatedTables(records);
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return records;
        }

        /// <summary>
		/// Adds the record handler.
		/// </summary>
        /// <param name="rec">The model.</param>
		public virtual bool AddRecord( object rec )
		{
            if (rec == null)
				return false;

			bool recordModified = false;

			try
			{
                T model = rec as T;

                List<ListEntry> recordsUpdated = new List<ListEntry>();

				// if the record is valid then add it
				// Do not show the dialog for valid record the second verification
				// is done here, the first was done in the view but as a precaution
				// we are double checking
				if (IsValidRecord(model, false) && !IsDuplicateRecord(model, true))
				{
					// Add to record details
                    object adapter = Reflect<T>.GetPropertyValue("TableAdapter");

                    // Insert the record into the database
                    Reflect<object>.InvokeMethodByInstance(adapter, "Insert",
                        Reflect.GetPropertyInfoByFlag(model, false, true, false, false));

                    // Add the record to the list
                    recordsUpdated.Add(new ListEntry { DisplayName = model.ToString(), Source = model });
				}

                // Fire off the event to let others know a record has been updated
                // Refresh the record count from the database
                if (recordsUpdated.Any())
                {
                    recordModified = true; 
                    RaiseRecordsUpdated(recordsUpdated);

                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return recordModified;
		}

		/// <summary>
        /// Adds the record and returns the identifier.
		/// </summary>
		/// <returns>The record return identifier.</returns>
        /// <param name="rec">Model.</param>
		public virtual int AddRecordReturnId( object rec )
		{
			int recordID = 0;

            if (rec == null)
				return recordID;
				
			try
			{
                T model = rec as T;

                List<ListEntry> recordsUpdated = new List<ListEntry>();

                // if the record is valid then add it
				// Do not show the dialog for valid record the second verification
				// is done here, the first was done in the view but as a precaution
				// we are double checking
				if (IsValidRecord(model, false) && !IsDuplicateRecord(model, true))
				{
					// Add to record details
                    object adapter = Reflect<T>.GetPropertyValue("TableAdapter");

                    // Insert the record into the database and get the return ID
                    recordID = (int)Reflect<object>.InvokeMethodByInstance(adapter, "InsertAndReturnID",
                        Reflect.GetPropertyInfoByFlag(model, false, true, false, false));

                    // Update the record with the returned ID
                    Reflect<object>.SetPropertyValueByInstance(model, "ID", recordID);

                    // Add the record to the list
                    recordsUpdated.Add(new ListEntry { DisplayName = model.ToString(), Source = model });
                }

                // Fire off the event to let others know a record has been updated
                // Refresh the record count from the database
                if (recordsUpdated.Any())
                {
                    RaiseRecordsUpdated(recordsUpdated);

                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
            }
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return recordID;
		}

		/// <summary>
		/// Updates the record handler.
		/// </summary>
        /// <param name="rec">The model.</param>
		public virtual bool EditRecord( object rec )
		{
            if (rec == null)
				return false;

			bool recordModified = false;

			try
			{
                T model = rec as T;

                List<ListEntry> recordsUpdated = new List<ListEntry>();

                // if the record is valid then add it
				// Do not show the dialog for valid record the second verification
				// is done here, the first was done in the view but as a precaution
				// we are double checking
				if (IsValidRecord(model, false))
				{
					DataRow row = Reflect<T>.InvokeMethodByInstance(model,"CopyToDataRow", 
						new[] { SelectedRecord.Row }) as DataRow;

                    object adapter = Reflect<T>.GetPropertyValue("TableAdapter");

                    // Update the record in the database
                    Reflect<object>.InvokeOverloadedMethodByInstance(adapter,
                        "Update", new[] { row }, new[] { typeof(DataRow) });

                    // Add the record to the list
                    recordsUpdated.Add(new ListEntry { DisplayName = model.ToString(), Source = model });
                }

                // Fire off the event to let others know a record has been updated
                // Refresh the record count from the database
                if (recordsUpdated.Any())
                {
                    recordModified = true;
                    RaiseRecordsUpdated(recordsUpdated);

                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
            }
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return recordModified;
		}

		/// <summary>
		/// Deletes the record handler.
        /// There is no need for a model to override this method because on delete
        /// all related records will be deleted based on foreign key
		/// </summary>
		/// <param name="rec">The record.</param>
		public bool DeleteRecord( object rec )
		{
			if ( rec == null )
				return false;

			bool recordModified = false;

			try
			{
				ObservableCollection<object> records = rec as ObservableCollection<object>;

				if (records != null)
				{
					int recordsProcessed = 0;
					int recordToDelete = records.Count;

                    List<ListEntry> recordsDeleted = new List<ListEntry>();
                    
                    while (recordsProcessed < recordToDelete)
					{
						// index will always be 0 since the records are being deleted from top to bottom
						// and the records collection is decremented automatically with each delete
						DataRowView view = records[recordsProcessed] as DataRowView;
						T model = NewRecord;
						Reflect<T>.InvokeMethodByInstance( model, "UpdateFromView", new[] { view } );

                        object adapter = Reflect<T>.GetPropertyValue("TableAdapter");

                        // Delete the record in the database
                        Reflect<object>.InvokeMethodByInstance(adapter, "Delete",
                            Reflect.GetPropertyInfoByFlag(model, false, false, false, true));

                        // Add the record to the list
                        recordsDeleted.Add(new ListEntry { DisplayName = model.ToString(), Source = model });
                        
                        recordsProcessed++;
					}

                    // Fire off the event to let others know a record has been updated
                    // Refresh the record count from the database
                    if (recordsDeleted.Any())
                    {
                        recordModified = true;
                        RaiseRecordsDeleted(recordsDeleted);

                        // TODO: Once testing is done remove all these line, currently if an update is not made 
                        // properly pulling records from the database and updating the controls will let me know 
                        // that the update was unsuccessful
                        AllRecords = GetAllRecords();
                    }
                }
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return recordModified;
		}

        /// <summary>
        /// Returns the currently selected record
        /// </summary>
        public T CurrentRecord(object rec)
        {
            T currentRecord = NewRecord;
            ObservableCollection<object> records = rec as ObservableCollection<object>;

			try
			{
				if (records.Any())
				{
					SelectedRecord = records[0] as DataRowView;
					Reflect<T>.InvokeMethodByInstance(currentRecord, "UpdateFromView", new[] { SelectedRecord });

					RaiseSelectedRecordChanged(new ListEntry { DisplayName = SelectedRecord.ToString(), Source = SelectedRecord });
				}
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

            return currentRecord;
        }

		/// <summary>
        /// Gets all records by string query
        /// </summary>
        /// <returns>A datatable of records.</returns>
        public DataTable GetRecordsFromQuery(string query)
        {
            DataTable records = new DataTable();
            records.Locale = CultureInfo.CurrentCulture;

            try
            {
                // Sort table by type
                records = BaseModel.Database.GetDataTableFast(query);
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return records;
        }

        /// <summary>
        /// Updates all records by string query
        /// </summary>
        /// <returns>A datatable of records.</returns>
        public bool UpdateRecordsFromQuery(string query)
        {
            bool updated = false;

            try
            {
                updated = BaseModel.Database.UpdateTableFast(query);
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return updated;
        }

        /// <summary>
        /// Called when [demand data source load].
        /// </summary>
        /// <param name="pageInfo">The page info.</param>
        public void OnDemandDataSourceLoad(PageInfo pageInfo)
        {
            try
            {
                Limit = pageInfo.Limit;
                Offset = pageInfo.Offset;

                FilterRecords();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }
        
        /// <summary>
		/// Determines whether this instance [can add record] the specified record.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this instance [can add record] the specified record; otherwise, <c>false</c>.
		/// </returns>
		public bool CanAddRecord()
		{
			return true;
		}

		/// <summary>
		/// Determines whether this instance [can update/delete record] the specified record.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this instance [can update/delete record] the specified record; otherwise, <c>false</c>.
		/// </returns>
        public bool CanUpdateRecord()
		{
			return SelectedRecord != null;
		}

		/// <summary>
		/// Determines if the record already exists based on primary keys
		/// </summary>
		/// <param name="model">The model.</param>
		/// <param name="showDialog">On error, show dialog with message</param>
		/// <returns>flag indicated whether the record was found or not</returns>
		public bool IsDuplicateRecord(T model, bool showDialog)
		{
            bool duplicateFound = AllRecords.Rows.Contains(Reflect.GetPropertyInfoByFlag(model,true,false,false,false));

			if (duplicateFound && showDialog)
			{
                object[] labels = Reflect.GetPropertyInfoByFlag(model, true, false, false, false, true);

                if (labels.Count() > 0)
                {
                    string message = LibraryProperties.Resources.ErrorMessage_PKEY1_FOUND;

                    if (labels.Count() == 2)
                        message = LibraryProperties.Resources.ErrorMessage_PKEY2_FOUND;
                    else if (labels.Count() == 3)
                        message = LibraryProperties.Resources.ErrorMessage_PKEY3_FOUND;

                    string errorMessage = LibraryProperties.Resources.ErrorMessage_ERROR_ON_SAVE;
                    errorMessage += "\n\n" + String.Format(message, labels);
                    errorMessage = errorMessage.ConvertEOLs();

                    MessageBox.ShowError(errorMessage, LibraryProperties.Resources.LabelName_TITLE_ERROR);
                }
			}

			return duplicateFound;
		}

		/// <summary>
		/// Verifies the record's content.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <param name="showDialog">On error, show dialog with message</param>
		/// <returns>flag indicated whether the record is valid or not</returns>
		public bool IsValidRecord(T model, bool showDialog)
		{
			return Reflect.VerifyRequiredProperties(model, showDialog);
		}
		#endregion

        #region Methods
        /// <summary>
        /// Preload related tables
        /// </summary>
        /// <param name="records"></param>
        protected void PreloadRelatedTables(DataTable records)
        {
            try
            {
                // loop through parent relations
                foreach (DataRelation relation in records.ParentRelations)
                {
                    // if the parent table is empty then preload the table
                    //  into memory
                    if (relation.ParentTable.Rows.Count == 0)
                    {
                        DataTable table = new DataTable();
                        table.Locale = CultureInfo.CurrentCulture;
                        table = BaseModel.Database.GetDataTableByAdapter(relation.ParentTable.TableName, "SELECT * FROM " + relation.ParentTable.TableName);
                    }
                }

                // loop through child relations
                foreach (DataRelation relation in records.ChildRelations)
                {
                    // if the child table is empty then preload the table
                    //  into memory
                    if (relation.ChildTable.Rows.Count == 0)
                    {
                        DataTable table = new DataTable();
                        table.Locale = CultureInfo.CurrentCulture;
                        table = BaseModel.Database.GetDataTableByAdapter(relation.ChildTable.TableName, "SELECT * FROM " + relation.ChildTable.TableName);
                    }
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
       }

        /// <summary>
        /// Add a counter column to the current datatable
        /// </summary>
        protected void AddCounterColumn()
        {
            // To properly manage offset and limit WITHOUT querying the database
            // add a faux column to the DataTable to track the row counts
            if (!AllRecords.Columns.Contains(CounterCol))
            {
                DataColumn column = new DataColumn(CounterCol, typeof(uint));
                AllRecords.Columns.Add(column);

                // Count the rows and store them in the new column
                for (int i = 0; i < AllRecords.Rows.Count; i++)
                {
                    AllRecords.Rows[i][CounterCol] = i;
                }
            }
        }

        /// <summary>
        /// Clears the table of counter column and filters
        /// </summary>
        protected void InitDataTable()
        {
            // If the counter column exists remove it
            if (AllRecords.Columns.Contains(CounterCol))
            {
                AllRecords.Columns.Remove(CounterCol);
            }

            // Clear row filter
            AllRecords.DefaultView.RowFilter = "";
        }

        /// <summary>
        /// Clears the table of counter column and filters
        /// </summary>
        protected void InitDataTable(ref DataTable table)
        {
            // If the counter column exists remove it
            if (table.Columns.Contains(CounterCol))
            {
                table.Columns.Remove(CounterCol);
            }

            // Clear row filter
            table.DefaultView.RowFilter = "";
        }
        #endregion
    }
}
