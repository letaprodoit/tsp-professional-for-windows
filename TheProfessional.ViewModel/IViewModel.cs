﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using TheProfessional.Library;
using TheProfessional.Model;

namespace TheProfessional.ViewModel
{
    /// <summary>
    /// Interface for all view models
    /// </summary>
    public interface IViewModel<T>
    {
        /// <summary>
        /// Gets the a new blank database model
        /// </summary>
        /// <value>The new record.</value>
		T NewRecord { get; }

        /// <summary>
        /// Gets or sets all records.
        /// </summary>
        /// <value>All records.</value>
        DataTable AllRecords { get; set; }
            
        /// <summary>
        /// Gets or sets the selected record.
        /// </summary>
        /// <value>The selected record.</value>
        DataRowView SelectedRecord { get; set; }

        /// <summary>
        /// Gets the number of records - read only
        /// </summary>
        /// <value>The number of records.</value>
        int NumberRecords { get; set; }

		/// <summary>
		/// Gets or sets the model table.
		/// </summary>
		/// <value>The model table.</value>
		string TableName { get; set; }

		/// <summary>
		/// Gets or sets the sortby element for the model table.
		/// </summary>
		/// <value>The sortbye element for the model table.</value>
		string TableSortBy { get; set; }
            
        /// <summary>
        /// Returns a model object of the current object
        /// </summary>
        /// <param name="rec">Records currently selected in grid.</param>
        T CurrentRecord(object rec);

		/// <summary>
		/// Adds the record.
		/// </summary>
        /// <param name="model">Record to save</param>
        bool AddRecord(object model);

		/// <summary>
		/// Adds the record and returns the identifier.
		/// </summary>
		/// <returns>The record return identifier.</returns>
		/// <param name="model">Model.</param>
        int AddRecordReturnId(object model);

		/// <summary>
		/// Edits the record.
		/// </summary>
        /// <param name="model">Model to edit</param>
        bool EditRecord(object model);

		/// <summary>
		/// Deletes the record.
		/// </summary>
		/// <param name="rec">Records currently selected in grid.</param>
        bool DeleteRecord(object rec);

		/// <summary>
		/// Gets all records
		/// </summary>
		/// <returns>A datatable of records.</returns>
		DataTable GetAllRecords();

        /// <summary>
        /// Gets all records by string query
        /// </summary>
        /// <returns>A datatable of records.</returns>
        DataTable GetRecordsFromQuery(string query);

        /// <summary>
        /// Filter Records
        /// </summary>
		void FilterRecords();

		/// <summary>
		/// Called when [demand data source load].
		/// </summary>
		/// <param name="pageInfo">The page info.</param>
        void OnDemandDataSourceLoad(PageInfo pageInfo);

		/// <summary>
        /// Determines if the record already exists based on primary keys
        /// </summary>
        /// <param name="model">The model</param>
        /// <param name="showDialog">On error, show dialog with message</param>
        /// <returns>flag indicated whether the record was found or not</returns>
        bool IsDuplicateRecord(T model, bool showDialog);

        /// <summary>
        /// Verifies the record's content.
        /// </summary>
        /// <param name="model">The model</param>
        /// <param name="showDialog">On error, show dialog with message</param>
        /// <returns>flag indicated whether the record is valid or not</returns>
        bool IsValidRecord(T model, bool showDialog);

		/// <summary>
		/// Determines whether this instance [can add record] the specified record.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this instance [can add record] the specified record; otherwise, <c>false</c>.
		/// </returns>
		bool CanAddRecord();

		/// <summary>
		/// Determines whether this instance [can update/delete record] the specified record.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this instance [can update/delete record] the specified record; otherwise, <c>false</c>.
		/// </returns>
		bool CanUpdateRecord();
    }
}
