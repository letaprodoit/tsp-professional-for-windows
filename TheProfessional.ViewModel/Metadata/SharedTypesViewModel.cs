using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Controls;
using System.Diagnostics;
using System.Globalization;
using TheProfessional.Model;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.ViewModel
{
	/// <summary>
	/// Shared types view model.
	/// </summary>
    public class SharedTypesViewModel : BaseViewModel<SharedTypesModel>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.ViewModel.SharedTypesViewModel"/> class.
		/// </summary>
        public SharedTypesViewModel()
		{
        }
    }
}
