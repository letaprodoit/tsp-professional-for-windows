using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Controls;
using System.Diagnostics;
using System.Globalization;
using TheProfessional.Model;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.ViewModel
{
	/// <summary>
	/// Shared types view model.
	/// </summary>
    public class SharedCommentsViewModel : BaseViewModel<SharedCommentsModel>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.ViewModel.SharedCommentsViewModel"/> class.
		/// </summary>
        public SharedCommentsViewModel()
        {
            OwnersList = GetOwnersList();
            this.PropertyChanged += SharedCommentsViewModel_PropertyChanged;
        }

        /// <summary>
        /// Gets or sets the selected owner type value.
        /// </summary>
        /// <value>The selected owner type value.</value>
		private static string _selectedOwnerType { get; set; }

        #region Properites
        private List<ListEntry> _ownersList;
        /// <summary>
        /// Gets or sets all the countries.
        /// </summary>
        /// <value>All countries.</value>
        public List<ListEntry> OwnersList
        {
            get { return _ownersList; }
            set
            {
                _ownersList = value;
                RaisePropertyChanged("OwnersList");
            }
        }

        private ListEntry _selectedOwner;
        /// <summary>
        /// Gets or sets the selected country.
        /// </summary>
        /// <value>The selected country.</value>
        public ListEntry SelectedOwner
        {
            get { return _selectedOwner; }
            set
            {
                if (_selectedOwner != value)
                {
                    // if we are changing countries start with
                    // the first record
                    Offset = 0;

                    if (value != null)
                    {
                        _selectedOwnerType = value.Source.ToString();
                    }

                    _selectedOwner = value;
                    RaisePropertyChanged("SelectedOwner");
                }

            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the countries available to the state
        /// </summary>
        /// <returns>The countries list.</returns>
        private List<ListEntry> GetOwnersList()
        {
            List<ListEntry> records = new List<ListEntry>();
            records.Add(new ListEntry { DisplayName = "", Source = "" });

            try
            {
                records = EnumToValueConverter.ConvertToList(typeof(SharedCommentsModel.CommentOwner), true);
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return records;
        }
        #endregion

        #region Listeners
        /// <summary>
        /// Filter records based on country
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void SharedCommentsViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SelectedOwner"))
            {
                FilterRecords();
            }
        }
        #endregion

        #region Overrides

        /// <summary>
        /// Gets the states with description (override base class)
        /// </summary>
        /// <returns>The states.</returns>
        public override DataTable GetAllRecords()
        {
            DataTable records = new DataTable();
            records.Locale = CultureInfo.CurrentCulture;

            try
            {
                if (!String.IsNullOrEmpty(_selectedOwnerType))
                {
                    // Get Records with description by country code
                    records = SharedCommentsModel.TableAdapter.GetDataByOwner(_selectedOwnerType);
                }
                else
                {
                    // Get Records with description
                    records = SharedCommentsModel.TableAdapter.GetData();
                }

                if (!records.Equals(null))
                {
                    PreloadRelatedTables(records);
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return records;
        }

        /// <summary>
        /// Filter Records
        /// </summary>
        public override void FilterRecords()
        {
            if (!String.IsNullOrEmpty(_selectedOwnerType))
            {
                AllRecords = SharedCommentsModel.TableAdapter.GetDataByOwner(_selectedOwnerType, Offset, Limit);
            }
            else
            {
                AllRecords = SharedCommentsModel.TableAdapter.GetData(Offset, Limit);
            }
        }
        #endregion
    }
}
