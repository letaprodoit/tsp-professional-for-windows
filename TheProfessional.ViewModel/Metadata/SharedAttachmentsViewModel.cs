using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Controls;
using System.Diagnostics;
using System.Globalization;
using TheProfessional.Model;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using LibraryResources = TheProfessional.Library.Properties.Resources;

namespace TheProfessional.ViewModel
{
	/// <summary>
	/// Shared types view model.
	/// </summary>
    public class SharedAttachmentsViewModel : BaseViewModel<SharedAttachmentsModel>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.ViewModel.SharedAttachmentsViewModel"/> class.
		/// </summary>
        public SharedAttachmentsViewModel()
        {
        }

        /// <summary>
        /// Adds the record handler.
        /// </summary>
        /// <param name="rec">The model.</param>
        public override bool AddRecord(object rec)
        {
            if (rec == null)
                return false;

            bool recordModified = false;

            try
            {
                SharedAttachmentsModel model = rec as SharedAttachmentsModel;

                List<ListEntry> recordsUpdated = new List<ListEntry>();

                // if the record is valid then add it
                // Do not show the dialog for valid record the second verification
                // is done here, the first was done in the view but as a precaution
                // we are double checking
                if (IsValidRecord(model, false) && !IsDuplicateRecord(model, true))
                {
                    // Add to record details
                    object adapter = Reflect<SharedAttachmentsModel>.GetPropertyValue("TableAdapter");

                    // Insert the record into the database
                    Reflect<object>.InvokeMethodByInstance(adapter, "Insert",
                        Reflect.GetPropertyInfoByFlag(model, false, true, false, false));

                    // Add the record to the list
                    recordsUpdated.Add(new ListEntry { DisplayName = model.ToString(), Source = model });
                }

                // Fire off the event to let others know a record has been updated
                // Refresh the record count from the database
                if (recordsUpdated.Any())
                {
                    recordModified = true;
                    RaiseRecordsUpdated(recordsUpdated);

                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return recordModified;
        }
    }
}
