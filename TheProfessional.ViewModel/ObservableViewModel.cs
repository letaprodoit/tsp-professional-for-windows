using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Threading;
using TheProfessional.Library;

namespace TheProfessional.ViewModel
{
    /// <summary>
    /// View model event handler.
    /// </summary>
    public delegate void ViewModelEventHandler(object sender, ViewModelEventArgs e);

    /// <summary>
    /// Event args for the ViewModelEventHandler
    /// </summary>
    public class ViewModelEventArgs : RoutedEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.ViewModel.ViewModelEventArgs"/> class.
        /// </summary>
        /// <param name="items"></param>
        public ViewModelEventArgs(List<ListEntry> items)
        {
            UpdatedItems = items;
        }

        /// <summary>
        /// Gets or sets the updated items.
        /// </summary>
        /// <value>The updated items.</value>
        public List<ListEntry> UpdatedItems { get; set; }
    }
    
    /// <summary>
	/// Observable view model.
	/// </summary>
    public class ObservableViewModel : ObservableObject, INotifyPropertyChanged
	{
        /// <summary>
        /// Occurs when record updated.
        /// </summary>
        public event ViewModelEventHandler RecordsUpdated;

        /// <summary>
        /// Occurs when record deleted.
        /// </summary>
        public event ViewModelEventHandler RecordsDeleted;

        /// <summary>
        /// Occurs when the selected record changes
        /// </summary>
        public event ViewModelEventHandler SelectedRecordChanged;

        #region Event Raisers
        /// <summary>
        /// Raises the record updated event
        /// </summary>
        /// <param name="recs">Records.</param>
        protected virtual void RaiseRecordsUpdated(List<ListEntry> recs)
        {
            if (RecordsUpdated != null)
            {
                RecordsUpdated(this, new ViewModelEventArgs(recs));
            }
        }

        /// <summary>
        /// Raises the record updated event
        /// </summary>
        /// <param name="recs">Records.</param>
        protected virtual void RaiseRecordsDeleted(List<ListEntry> recs)
        {
            if (RecordsDeleted != null)
            {
                RecordsDeleted(this, new ViewModelEventArgs(recs));
            }
        }

        /// <summary>
        /// Raises the selected recorded changed event
        /// </summary>
        /// <param name="rec">Records.</param>
        protected virtual void RaiseSelectedRecordChanged(ListEntry rec)
        {
            if (SelectedRecordChanged != null)
            {
                SelectedRecordChanged(this, new ViewModelEventArgs(new List<ListEntry> { rec }));
            }
        }
        #endregion
    }
}
