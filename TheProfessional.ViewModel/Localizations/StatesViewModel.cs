using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Controls;
using System.Diagnostics;
using System.Globalization;
using TheProfessional.Model;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using TheProfessional.Library.Database;

namespace TheProfessional.ViewModel
{
	/// <summary>
	/// Professions view model.
	/// </summary>
    public class StatesViewModel : BaseViewModel<StatesModel>
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.ViewModel.StatesViewModel"/> class.
		/// </summary>
        public StatesViewModel()
		{
            CountriesList = GetCountriesList();
            this.PropertyChanged += StatesViewModel_PropertyChanged;
        }
        /// <summary>
        /// Gets or sets the selected country code value.
        /// </summary>
        /// <value>The selected country code value.</value>
		private static string _selectedCountryCode { get; set; }

        #region Properites
        private List<ListEntry> _countriesList;
        /// <summary>
        /// Gets or sets all the countries.
        /// </summary>
        /// <value>All countries.</value>
        public List<ListEntry> CountriesList
        {
            get { return _countriesList; }
            set
            {
                _countriesList = value;
                RaisePropertyChanged("CountriesList");
            }
        }

        private ListEntry _selectedCountry;
        /// <summary>
        /// Gets or sets the selected country.
        /// </summary>
        /// <value>The selected country.</value>
        public ListEntry SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                if (_selectedCountry != value)
                {
                    // if we are changing countries start with
                    // reset the offset and limit
                    Offset = -1;
                    Limit = -1;

                    if (value != null)
                    {
                        _selectedCountryCode = value.Source.ToString();
                    }

                    _selectedCountry = value;
                    RaisePropertyChanged("SelectedCountry");
                }

            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the countries available to the state
        /// </summary>
        /// <returns>The countries list.</returns>
        private List<ListEntry> GetCountriesList()
        {
            List<ListEntry> records = new List<ListEntry>();
            records.Add(new ListEntry { DisplayName = "", Source = "" });

            try
            {
                // Sort table by type
                DataTable table = base.GetRecordsFromQuery(StatesModel.TableAdapter.ModelQueryCollection[2]);
                table.Locale = CultureInfo.CurrentCulture;

                foreach (DataRow row in table.Rows)
                {
                    string country = row[1].ToString();
                    string code = row[0].ToString();

                    ListEntry entry = new ListEntry { DisplayName = String.Format("{0} ({1})", country, code), Source = code };
                    records.Add(entry);
                }

                table.Dispose();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return records;
        }
        #endregion

        #region Listeners
        /// <summary>
        /// Filter records based on country
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void StatesViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SelectedCountry"))
            {
                FilterRecords();
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Filter Records
        /// </summary>
        public override void FilterRecords()
        {
            if (!String.IsNullOrEmpty(_selectedCountryCode))
            {
                AllRecords = StatesModel.TableAdapter.GetDataByCountryCodeWithDescriptions(_selectedCountryCode, Offset, Limit);
            }
            else
            {
                AllRecords = StatesModel.TableAdapter.GetDataWithDescriptions(Offset, Limit);
            }
        }

        /// <summary>
		/// Adds the record handler.
		/// </summary>
        /// <param name="rec">The model.</param>
        public override int AddRecordReturnId(object rec)
		{
            int recordID = 0;

            if (rec == null)
                return recordID;

            try
            {
                StatesModel model = rec as StatesModel;

                // if the record is valid then add it
                // Do not show the dialog for valid record the second verification
                // is done here, the first was done in the view but as a precaution
                // we are double checking
                if (IsValidRecord(model, false) && !IsDuplicateRecord(model, true))
                {
                    // Add to record details
                    object adapter = Reflect<StatesModel>.GetPropertyValue("TableAdapter");

                    recordID = (int)Reflect<object>.InvokeMethodByInstance(adapter, "InsertAndReturnID",
                        Reflect.GetPropertyInfoByFlag(model, false, true, false, false));

                    if (recordID > 0)
                    {
                        object dadapter = Reflect<StatesDescriptionsModel>.GetPropertyValue("TableAdapter");

                        StatesDescriptionsModel dmodel = new StatesDescriptionsModel(rec as StatesModel);
                        dmodel.ID = recordID;

                        bool recordModified = (int)Reflect<object>.InvokeMethodByInstance(adapter, "Insert",
                            Reflect.GetPropertyInfoByFlag(dmodel, false, true, false, false)) == 1;
                    }
                }

                // Refresh the record count from the database
                if (recordID > 0)
                {
                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return recordID;
		}

		/// <summary>
		/// Updates the record handler.
		/// </summary>
		/// <param name="rec">The model.</param>
        public override bool EditRecord(object rec)
		{
            if (rec == null)
                return false;

            bool recordModified = false;

            try
            {
                StatesModel model = rec as StatesModel;

                // if the record is valid then add it
                // Do not show the dialog for valid record the second verification
                // is done here, the first was done in the view but as a precaution
                // we are double checking
                if (IsValidRecord(model, false))
                {
                    StatesDescriptionsModel oldModel = new StatesDescriptionsModel();
                    oldModel.UpdateFromView(SelectedRecord);
                    object[] oldValues = Reflect.GetPropertyInfoByFlag(oldModel, false, false, true, false);

                    DataRow row = model.CopyToDataRow(SelectedRecord.Row);

                    recordModified = (int)StatesModel.TableAdapter.Update(row) == 1;

                    if (recordModified)
                    {
                        StatesDescriptionsModel newModel = new StatesDescriptionsModel();
                        newModel.UpdateFromView(SelectedRecord);
                        object[] newValues = Reflect.GetPropertyInfoByFlag(newModel, false, false, true, false);

                        object[] updates = newValues.Concat(oldValues).ToArray();

                        Type[] oldTypes = Reflect.GetPropertyInfoByFlag(oldModel, false, false, true, false, false, true) as Type[];
                        Type[] newTypes = oldTypes;
                        Type[] types = newTypes.Concat(oldTypes).ToArray();

                        object adapter = Reflect<StatesDescriptionsModel>.GetPropertyValue("TableAdapter");

                        recordModified = (int)Reflect<object>.InvokeOverloadedMethodByInstance(adapter, "Update",
                            updates, types) == 1;
                    }
                }

                // Refresh the record count from the database
                if (recordModified)
                {
                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return recordModified;
        }

		/// <summary>
		/// Gets the states with description (override base class)
		/// </summary>
		/// <returns>The states.</returns>
        public override DataTable GetAllRecords()
		{
			DataTable records = new DataTable();
			records.Locale = CultureInfo.CurrentCulture;

			try
			{
                if (!String.IsNullOrEmpty(_selectedCountryCode))
                {
                    // Get Records with description by country code
                    records = StatesModel.TableAdapter.GetDataByCountryCodeWithDescriptions(_selectedCountryCode);
                }
                else
                {
                    // Get Records with description
                    records = StatesModel.TableAdapter.GetDataWithDescriptions();
                }

                if (!records.Equals(null))
                {
                    PreloadRelatedTables(records);
                }
            }
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return records;
		}
        #endregion
    }
}
