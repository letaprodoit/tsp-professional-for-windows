using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Controls;
using System.Diagnostics;
using System.Globalization;
using TheProfessional.Model;
using TheProfessional.Library;
using TheProfessional.Library.Converters;
using MessageBox = TheProfessional.Library.Dialogs.MessageBox;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using TheProfessional.Library.Database;

namespace TheProfessional.ViewModel
{
	/// <summary>
	/// Professions view model.
	/// </summary>
    public class CountriesViewModel : BaseViewModel<CountriesModel>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.ViewModel.CountriesViewModel"/> class.
		/// </summary>
        public CountriesViewModel()
		{
        }

		#region Overrides
        /// <summary>
        /// Filter Records
        /// </summary>
        public override void FilterRecords()
        {
            AllRecords = CountriesModel.TableAdapter.GetDataWithDescriptions(Offset, Limit);
        }
        
        /// <summary>
		/// Adds the record handler.
		/// </summary>
        /// <param name="rec">The model.</param>
        public override bool AddRecord(object rec)
		{
            if (rec == null)
                return false;

            bool recordModified = false;

            try
            {
                CountriesModel model = rec as CountriesModel;

                // if the record is valid then add it
                // Do not show the dialog for valid record the second verification
                // is done here, the first was done in the view but as a precaution
                // we are double checking
                if (IsValidRecord(model, false) && !IsDuplicateRecord(model, true))
                {
                    // Add to record details
                    object adapter = Reflect<CountriesModel>.GetPropertyValue("TableAdapter");

                    recordModified = (int)Reflect<object>.InvokeMethodByInstance(adapter, "Insert",
                        Reflect.GetPropertyInfoByFlag(model, false, true, false, false)) == 1;

                    if (recordModified)
                    {
                        object dadapter = Reflect<CountriesDescriptionsModel>.GetPropertyValue("TableAdapter");

                        CountriesDescriptionsModel dmodel = new CountriesDescriptionsModel(rec as CountriesModel);

                        recordModified = (int)Reflect<object>.InvokeMethodByInstance(dadapter, "Insert",
                            Reflect.GetPropertyInfoByFlag(dmodel, false, true, false, false)) == 1;
                    }
                }

                // Refresh the record count from the database
                if (recordModified)
                {
                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return recordModified;
		}

		/// <summary>
		/// Updates the record handler.
		/// </summary>
        /// <param name="rec">The model.</param>
        public override bool EditRecord(object rec)
		{
            if (rec == null)
                return false;

            bool recordModified = false;

            try
            {
                CountriesModel model = rec as CountriesModel;

                // if the record is valid then add it
                // Do not show the dialog for valid record the second verification
                // is done here, the first was done in the view but as a precaution
                // we are double checking
                if (IsValidRecord(model, false))
                {
                    CountriesDescriptionsModel oldModel = new CountriesDescriptionsModel();
                    oldModel.UpdateFromView(SelectedRecord);
                    object[] oldValues = Reflect.GetPropertyInfoByFlag(oldModel, false, false, true, false);

                    DataRow row = model.CopyToDataRow(SelectedRecord.Row);

                    recordModified = (int)CountriesModel.TableAdapter.Update(row) == 1;

                    if (recordModified)
                    {
                        CountriesDescriptionsModel newModel = new CountriesDescriptionsModel();
                        newModel.UpdateFromView(SelectedRecord);
                        object[] newValues = Reflect.GetPropertyInfoByFlag(newModel, false, false, true, false);

                        object[] updates = newValues.Concat(oldValues).ToArray();

                        Type[] oldTypes = Reflect.GetPropertyInfoByFlag(oldModel, false, false, true, false, false, true) as Type[];
                        Type[] newTypes = oldTypes;
                        Type[] types = newTypes.Concat(oldTypes).ToArray();

                        object adapter = Reflect<CountriesDescriptionsModel>.GetPropertyValue("TableAdapter");

                        recordModified = (int)Reflect<object>.InvokeOverloadedMethodByInstance(adapter, "Update", 
                            updates, types) == 1;
                    }
                }

                // Refresh the record count from the database
                if (recordModified)
                {
                    // TODO: Once testing is done remove all these line, currently if an update is not made 
                    // properly pulling records from the database and updating the controls will let me know 
                    // that the update was unsuccessful
                    AllRecords = GetAllRecords();
                }
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }

            return recordModified;
		}

		/// <summary>
		/// Gets the states with description (override base class)
		/// </summary>
		/// <returns>The states.</returns>
		public override DataTable GetAllRecords()
		{
			DataTable records = new DataTable();
			records.Locale = CultureInfo.CurrentCulture;

			try
			{
				// Get Records with description
				records = CountriesModel.TableAdapter.GetDataWithDescriptions();

                if (!records.Equals(null))
                {
                    PreloadRelatedTables(records);
                }
            }
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}

			return records;
		}
		#endregion
    }
}
