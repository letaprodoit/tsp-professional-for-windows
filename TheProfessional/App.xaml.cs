using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Resources;
using System.Globalization;
using TheProfessional.View;

namespace TheProfessional
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        [STAThread]
        public static void Main()  
        {  
            //Application app = new Application();  
            //TheProfessional.View.MainWindow win = new TheProfessional.View.MainWindow();
            //app.Run(win);
        }
    }
}
