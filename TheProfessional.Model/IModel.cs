﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheProfessional.Library;
using System.Data;

namespace TheProfessional.Model
{
    /// <summary>
    /// The model interface
    /// </summary>
    interface IModel
    {
		/// <summary>
		/// Gets or sets the model table.
		/// </summary>
		/// <value>The model table.</value>
		[Ignore]
		string TableName { get; set; }


		/// <summary>
		/// Gets or sets the sortby element for the model table.
		/// </summary>
		/// <value>The sortbye element for the model table.</value>
		[Ignore]
		string TableSortBy { get; set; }

		/// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        void UpdateFromView(DataRowView record);

		/// <summary>
		/// Copies the current data to the DataRow.
		/// </summary>
		/// <returns>The data row.</returns>
		/// <param name="row">Row.</param>
        DataRow CopyToDataRow(DataRow row);
    }
}
