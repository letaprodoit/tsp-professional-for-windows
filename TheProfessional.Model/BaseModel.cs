using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using System.Reflection;
using System.Globalization;
using TheProfessional.Library.Database;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
	public class BaseModel : ObservableObject
    {

        private static DatabaseManager _database;
        /// <summary>
        /// The database manager.
        /// </summary>
        [Ignore]
        public static DatabaseManager Database
        {
            get
            {
                if (_database == null)
                {
                    _database = new DatabaseManager(Properties.Settings.Default.the_professionalConnectionString, new Data.TheProfessionalDataSet());
                }

                return _database;
            }
        }

		/// <summary>
		/// Gets the name of the property.
		/// </summary>
		/// <returns>The property name.</returns>
		/// <param name="record">Record.</param>
        /// <param name="dbColName">Database column name.</param>
		public string GetPropertyName(object record, string dbColName)
		{
			PropertyInfo[] properties = record.GetType().GetProperties();
			foreach ( PropertyInfo prop in properties )
			{
				// skip processing of the properties that should be ignored
				if ( prop.GetCustomAttributes( typeof( IgnoreAttribute ), true ).Any() )
				{
					continue;
				}

				ColumnAttribute columnAtt = ( ColumnAttribute )prop.GetCustomAttributes( typeof( ColumnAttribute ), true ).GetValue( 0 );

				// If the property has a Name attribute and it equals the column name
				// then return the property name
				if ( !String.IsNullOrEmpty( columnAtt.Name ) )
				{
					if ( columnAtt.Name.Equals( dbColName ) )
					{
						return prop.Name;
					}
				}
			}

			// else return the string with the first letter uppercased
			// which means the column name is the same as the propery name
			// except that the property name is capitalized
			return dbColName[0].ToString().ToUpperInvariant() + dbColName.Substring(1);
		}

        #region Properties
		/// <summary>
		/// Gets or sets the model table.
		/// </summary>
		/// <value>The model table.</value>
		[Ignore]
		public string TableName { get; set; }


		/// <summary>
		/// Gets or sets the sortby element for the model table.
		/// </summary>
		/// <value>The sortbye element for the model table.</value>
		[Ignore]
		public string TableSortBy { get; set; }
        #endregion
    }
}