using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class StatesModel : BaseModel, IModel
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.StatesModel"/> class.
		/// </summary>
		public StatesModel()
		{
			TableName = "states";
			TableSortBy = "state_id";
		}

        /// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.StatesModel"/> class.
        /// </summary>
		/// <param name="stateId"></param>
		/// <param name="stateCode"></param>
		/// <param name="countryCode"></param>
		/// <param name="state">virtual property</param>
		public StatesModel(int stateId, string stateCode, string countryCode, string state):this()
		{
			if (String.IsNullOrEmpty(stateCode))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_StateCode));
            }
			if (String.IsNullOrEmpty(countryCode))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_CountryCode));
            }
            if (String.IsNullOrEmpty(state))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_State));
            }
            try
            {
				ID = stateId;
				StateCode = stateCode;
				CountryCode = countryCode;
                State = state;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private int _id;
        /// <summary>
        /// Gets the state id
        /// </summary>
        /// <value>The state ID.</value>
        [Column(Sort = true, AutoIncrement = true, PrimaryKey = true, Name = "state_id")] 
        public int ID
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        private string _stateCode;
        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>The country code.</value>
        [Column(Required = true, Title = ModelProperties.Header_StateCode, Name = "state_code")]
        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                if (value != _stateCode)
                {
                    _stateCode = value;
                    RaisePropertyChanged("StateCode");
                }
            }
        }

        private string _countryCode;
        /// <summary>
		/// Gets or sets the country code.
		/// </summary>
		/// <value>The country code.</value>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_CountryCode, ForeignKey = true, Name = "country_code")]
        public string CountryCode
        {
            get { return _countryCode; }
            set
            {
                if (value != _countryCode)
                {
                    _countryCode = value;
                    RaisePropertyChanged("Code");
                }
            }
        }

        private string _state;
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        [Column(Required = true, Virtual = true, AutoSize = true, Title = ModelProperties.Header_State)]
        public string State
        {
            get { return _state; }
            set
            {
                if (value != _state)
                {
                    _state = value;
                    RaisePropertyChanged("State");
                }
            }
        }

        private static TableAdapterExt _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TableAdapterExt TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TableAdapterExt();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = (int)record["state_id"];
                StateCode = record["state_code"].ToString();
                CountryCode = record["country_code"].ToString();
                State = record["state"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["state_id"] = this.ID;
                row["state_code"] = this.StateCode;
                row["country_code"] = this.CountryCode;
                row["state"] = this.State;
            }

            return row;
        }
        #endregion

        /// <summary>
		/// Child class of table adapter to perform other table tasks
		/// </summary>
        public sealed class TableAdapterExt : Data.TheProfessionalDataSetTableAdapters.statesTableAdapter
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TheProfessional.Model.StatesModel.TableAdapterExt"/> class.
            /// </summary>
            public TableAdapterExt()
            {
            }

            /// <summary>
            /// Gets/Sets a collection of commands this adapater is responsible for
            /// </summary>
            public string[] ModelQueryCollection
            {
                get
                {
                    return new string[] { 
                        "SELECT states.*, states_descriptions.state FROM states " +
                            "INNER JOIN states_descriptions ON states.state_id = states_descriptions.state_id " +
                            "ORDER BY states.state_id",

                        "SELECT states.*, states_descriptions.state FROM states " +
                            "INNER JOIN states_descriptions ON states.state_id = states_descriptions.state_id " +
                            "WHERE states.country_code = '@country_code' " +
                            "ORDER BY states.state_id",
                            
                        "SELECT countries_descriptions.* FROM countries_descriptions " + 
                            "INNER JOIN countries ON countries.country_code = countries_descriptions.country_code "+
                            "ORDER BY countries_descriptions.country",
                    };
                }
            }

            /// <summary>
            /// Gets a command collection based on the given query index
            /// </summary>
            /// <param name="index"></param>
            /// <returns></returns>
            public global::MySql.Data.MySqlClient.MySqlCommand ModelCommand(int index)
            {
                return new global::MySql.Data.MySqlClient.MySqlCommand
                {
                    Connection = base.Connection,
                    CommandText = ModelQueryCollection[index],
                    CommandType = global::System.Data.CommandType.Text
                };
            }

            /// <summary>
            /// Gets the data with descriptions.
            /// </summary>
            /// <param name="offset"></param>
            /// <param name="limit"></param>
            /// <returns>The data with descriptions.</returns>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, false)]
            public DataTable GetDataWithDescriptions(int offset = -1, int limit = -1)
            {
                base.Adapter.SelectCommand = ModelCommand(0);

                if (offset != -1 && limit != -1)
                {
                    this.Adapter.SelectCommand.CommandText += String.Format(" LIMIT {0}, {1}", offset, limit);
                }

                DataTable dataTable = BaseModel.Database.GetDataTableByAdapter("states", base.Adapter.SelectCommand.CommandText);
                return dataTable;
            }
            /// <summary>
            /// Gets the data by country code
            /// </summary>
            /// <param name="country_code"></param>
            /// <param name="offset"></param>
            /// <param name="limit"></param>
            /// <returns></returns>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, false)]
            public DataTable GetDataByCountryCodeWithDescriptions(string country_code, int offset = -1, int limit = -1)
            {
                this.Adapter.SelectCommand = ModelCommand(1);
                if ((country_code == null))
                {
                    throw new global::System.ArgumentNullException("country_code");
                }
                else
                {
                    this.Adapter.SelectCommand.CommandText = this.Adapter.SelectCommand.CommandText.Replace("@country_code", country_code);
                }

                if (offset != -1 && limit != -1)
                {
                    this.Adapter.SelectCommand.CommandText += String.Format(" LIMIT {0}, {1}", offset, limit);
                }

                DataTable dataTable = BaseModel.Database.GetDataTableByAdapter("states", base.Adapter.SelectCommand.CommandText);
                return dataTable;
            }

            /// <summary>
            /// Inserts and return ID.
            /// </summary>
            /// <returns>The record ID that was creating during insert.</returns>
            /// <param name="state_code">State_code.</param>
            /// <param name="country_code">Country_code.</param>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Insert, false)]
            public object InsertAndReturnID(string state_code, string country_code)
            {
                if ((state_code == null))
                {
                    throw new global::System.ArgumentNullException("state_code");
                }
                else
                {
                    this.Adapter.SelectCommand.CommandText = this.Adapter.SelectCommand.CommandText.Replace("@state_code", state_code);
                }
                if ((country_code == null))
                {
                    throw new global::System.ArgumentNullException("country_code");
                }
                else
                {
                    this.Adapter.SelectCommand.CommandText = this.Adapter.SelectCommand.CommandText.Replace("@country_code", country_code);
                }
                global::System.Data.ConnectionState previousConnectionState = base.Adapter.InsertCommand.Connection.State;
                if (((base.Adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                            != global::System.Data.ConnectionState.Open))
                {
                    base.Adapter.InsertCommand.Connection.Open();
                }
                object returnValue;
                try
                {
                    base.Adapter.InsertCommand.ExecuteNonQuery();

                    returnValue = base.Adapter.InsertCommand.LastInsertedId;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        base.Adapter.InsertCommand.Connection.Close();
                    }
                }
                if (((returnValue == null)
                            || (returnValue.GetType() == typeof(global::System.DBNull))))
                {
                    return null;
                }
                else
                {
                    return ((object)(returnValue));
                }
            }
        }
    }
}