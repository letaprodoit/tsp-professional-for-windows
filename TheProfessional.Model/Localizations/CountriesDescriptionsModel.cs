using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class CountriesDescriptionsModel : BaseModel, IModel
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.CountriesDescriptionsModel"/> class.
		/// </summary>
		public CountriesDescriptionsModel()
		{
			TableName = "countries_descriptions";
			TableSortBy = "country_code";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.CountriesDescriptionsModel"/> class.
        /// </summary>
		/// <param name="countryCode"></param>
		/// <param name="country">virtual property</param>
		public CountriesDescriptionsModel(string countryCode, string country ):this()
		{
            if (String.IsNullOrEmpty(country))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Country));
            }
            try
            {
                Code = countryCode;
                Country = country;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

        /// <summary>
        /// Intialize a new description model based on parent
        /// </summary>
        /// <param name="model"></param>
        public CountriesDescriptionsModel(CountriesModel model)
        {
            Code = model.Code;
            Country = model.Country;
        }

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private string _code;
		/// <summary>
		/// Gets or sets the country code.
		/// </summary>
		/// <value>The country code.</value>
        [Column(PrimaryKey = true, Sort = true, Required = true, Title = ModelProperties.Header_Code, Name = "country_code")] 
        public string Code
        {
            get { return _code; }
            set
            {
                if (value != _code)
                {
                    _code = value;
                    RaisePropertyChanged("Code");
                }
            }
        }

        private string _country;
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        [Column(AutoSize = true, Required = true, Title = ModelProperties.Header_Country)]
        public string Country
        {
            get { return _country; }
            set
            {
                if (value != _country)
                {
                    _country = value;
                    RaisePropertyChanged("Country");
                }
            }
        }

        private static Data.TheProfessionalDataSetTableAdapters.countries_descriptionsTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's description adapter
        /// </summary>
        /// <value>The model table's description adapter.</value>
        [Ignore]
        public static Data.TheProfessionalDataSetTableAdapters.countries_descriptionsTableAdapter TableAdapter
        {
            get
            {
				if (_tableAdapter == null)
                {

                    _tableAdapter = new Data.TheProfessionalDataSetTableAdapters.countries_descriptionsTableAdapter();
                }
				return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
				Code = record["country_code"].ToString();
                Country = record["country"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
				row["country_code"] = this.Code;
                row["country"] = this.Country;
            }

            return row;
        }
        #endregion
    }
}