using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class CountriesModel : BaseModel, IModel
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.CountriesModel"/> class.
		/// </summary>
		public CountriesModel()
		{
			TableName = "countries";
			TableSortBy = "country_code";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.CountriesModel"/> class.
        /// </summary>
		/// <param name="countryCode"></param>
		/// <param name="codeA3"></param>
		/// <param name="codeN3"></param>
        /// <param name="region"></param>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
		/// <param name="country">virtual property</param>
		public CountriesModel(string countryCode, string codeA3, string codeN3, 
			string region, double lat, double lon, string country ):this()
		{
            if (String.IsNullOrEmpty(countryCode))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_CountryCode));
            }
            if (String.IsNullOrEmpty(codeA3))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_ISOA3));
            }
            if (String.IsNullOrEmpty(codeN3))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_ISON3));
            }
            if (String.IsNullOrEmpty(region))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Region));
            }
            if (String.IsNullOrEmpty(country))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Country));
            }
            try
            {
                Code = countryCode;
                ISOA3 = codeA3;
                ISON3 = codeN3;
                Region = region;
                Lat = lat;
                Lon = lon;
                Country = country;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
		{
			get
			{
				string propertyName = base.GetPropertyName( this, dbColName );
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
			}
			set
			{
				string propertyName = GetPropertyName( this, dbColName );
				PropertyInfo prop = this.GetType().GetProperty(propertyName);

				prop.SetValue(this, value);
			}
		}

        #region Properties
        private string _code;
		/// <summary>
		/// Gets or sets the country code.
		/// </summary>
		/// <value>The country code.</value>
        [Column(Required = true, Sort = true, PrimaryKey = true, Title = ModelProperties.Header_Code, Name = "country_code")] 
        public string Code
        {
            get { return _code; }
            set
            {
                if (value != _code)
                {
                    _code = value;
                    RaisePropertyChanged("Code");
                }
            }
        }

        private string _country;
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        [Column(Virtual = true, Required = true, AutoSize = true, Title = ModelProperties.Header_Country)] 
        public string Country
        {
            get { return _country; }
            set
            {
                if (value != _country)
                {
                    _country = value;
                    RaisePropertyChanged("Country");
                }
            }
        }

        private string _isoA3;
		/// <summary>
		/// Gets or sets the ISO code.
		/// </summary>
		/// <value>The ISO code.</value>
        [Column(HasDefault = true, Title = ModelProperties.Header_ISOA3, Name = "code_A3")] 
        public string ISOA3
        {
            get { return _isoA3; }
            set
            {
                if (value != _isoA3)
                {
                    _isoA3 = value;
                    RaisePropertyChanged("ISOA3");
                }
            }
        }

		private string _isoN3;
		/// <summary>
		/// Gets or sets the numeric code.
		/// </summary>
		/// <value>The numeric code.</value>
        [Column(HasDefault = true, Title = ModelProperties.Header_ISON3, Name = "code_N3")] 
		public string ISON3
		{
            get { return _isoN3; }
			set
			{
                if (value != _isoN3)
				{
                    _isoN3 = value;
                    RaisePropertyChanged("ISON3");
				}
			}
		}

		private string _region;
		/// <summary>
		/// Gets or sets the region.
		/// </summary>
		/// <value>The region.</value>
        [Column(HasDefault = true, Title = ModelProperties.Header_Region)] 
		public string Region
		{
            get { return _region; }
			set
			{
				if (value != _region)
				{
					_region = value;
					RaisePropertyChanged("Region");
				}
			}
		}

		private double _lat;
		/// <summary>
		/// Gets or sets the latitude.
		/// </summary>
		/// <value>The latitude.</value>
        [Column(HasDefault = true, AutoSize = true, Title = ModelProperties.Header_Lat)] 
		public double Lat
        {
			get { return _lat; }
            set
            {
				if (!_lat.Equals(value))
                {
					_lat = value;
					RaisePropertyChanged("Lat");
                }
            }
        }

		private double _lon;
		/// <summary>
		/// Gets or sets the longitude.
		/// </summary>
		/// <value>The longitude.</value>
        [Column(HasDefault = true, AutoSize = true, Title = ModelProperties.Header_Lon)] 
		public double Lon
		{
			get { return _lon; }
			set
			{
				if (!_lon.Equals(value))
				{
					_lon = value;
					RaisePropertyChanged("Lon");
				}
			}
		}

        private static TableAdapterExt _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TableAdapterExt TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TableAdapterExt();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
				Code = record["country_code"].ToString();
				ISOA3 = record["code_A3"].ToString();
				ISON3 = record["code_N3"].ToString();
				Region = record["region"].ToString();
                Lat = (double)record["lat"];
                Lon = (double)record["lon"];
                Country = record["country"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
				row["country_code"] = this.Code;
				row["code_A3"] = this.ISOA3;
                row["code_N3"] = this.ISON3;
				row["region"] = this.Region;
                row["lat"] = this.Lat;
                row["lon"] = this.Lon;
                row["country"] = this.Country;
            }

            return row;
        }
        #endregion

        /// <summary>
		/// Child class of table adapter to perform other table tasks
		/// </summary>
        public sealed class TableAdapterExt : Data.TheProfessionalDataSetTableAdapters.countriesTableAdapter
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TheProfessional.Model.CountriesModel.TableAdapterExt"/> class.
            /// </summary>
            public TableAdapterExt()
            {
            }

            /// <summary>
            /// Gets/Sets a collection of commands this adapater is responsible for
            /// </summary>
            public string[] ModelQueryCollection
            {
                get
                {
                    return new string[] { "SELECT countries.*, countries_descriptions.country FROM countries " + 
                            "INNER JOIN countries_descriptions ON countries.country_code = countries_descriptions.country_code "+
                            "ORDER BY countries_descriptions.country_code" };
                }
            }

            /// <summary>
            /// Gets a command collection based on the given query index
            /// </summary>
            /// <param name="index"></param>
            /// <returns></returns>
            public global::MySql.Data.MySqlClient.MySqlCommand ModelCommand(int index)
            {
                return new global::MySql.Data.MySqlClient.MySqlCommand
                {
                    Connection = base.Connection,
                    CommandText = ModelQueryCollection[index],
                    CommandType = global::System.Data.CommandType.Text
                };
            }

            /// <summary>
            /// Gets the data with descriptions.
            /// </summary>
            /// <returns>The data with descriptions.</returns>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, false)]
            public DataTable GetDataWithDescriptions(int offset = -1, int limit = -1)
            {
                base.Adapter.SelectCommand = ModelCommand(0);

                if (offset != -1 && limit != -1)
                {
                    base.Adapter.SelectCommand.CommandText += String.Format(" LIMIT {0}, {1}", offset, limit);
                }

                DataTable dataTable = BaseModel.Database.GetDataTableByAdapter("countries", base.Adapter.SelectCommand.CommandText);
                return dataTable;
            }
        }
    }
}