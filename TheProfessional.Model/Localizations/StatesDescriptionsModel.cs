using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class StatesDescriptionsModel : BaseModel, IModel
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.StatesDescriptionsModel"/> class.
		/// </summary>
		public StatesDescriptionsModel()
		{
			TableName = "states_descriptions";
			TableSortBy = "state_id";
		}

        /// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.StatesDescriptionsModel"/> class.
        /// </summary>
		/// <param name="stateId"></param>
		/// <param name="state">virtual property</param>
		public StatesDescriptionsModel(int stateId, string state):this()
		{
            if (String.IsNullOrEmpty(state))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_State));
            }
            try
            {
				ID = stateId;
                State = state;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

        /// <summary>
        /// Update description based on parent model
        /// </summary>
        /// <param name="model"></param>
        public StatesDescriptionsModel(StatesModel model)
        {
            ID = model.ID;
            State = model.State;
        }

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private int _id;
        /// <summary>
        /// Gets the state id
        /// </summary>
        /// <value>The state ID.</value>
		[Column(PrimaryKey = true, Required = true, Sort = true, Name = "state_id")] 
        public int ID
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }
        private string _state;
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_State)] 
        public string State
        {
            get { return _state; }
            set
            {
                if (value != _state)
                {
                    _state = value;
                    RaisePropertyChanged("State");
                }
            }
        }

        private static Data.TheProfessionalDataSetTableAdapters.states_descriptionsTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's description adapter
        /// </summary>
        /// <value>The model table's description adapter.</value>
        [Ignore]
        public static Data.TheProfessionalDataSetTableAdapters.states_descriptionsTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new Data.TheProfessionalDataSetTableAdapters.states_descriptionsTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = (int)record["state_id"];
                State = record["state"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["state_id"] = this.ID;
                row["state"] = this.State;
            }

            return row;
        }
        #endregion
    }
}