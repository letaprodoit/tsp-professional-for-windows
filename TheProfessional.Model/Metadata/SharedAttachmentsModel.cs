using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared attachments model.
	/// </summary>
    public class SharedAttachmentsModel : BaseModel, IModel
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedAttachmentsModel"/> class.
		/// </summary>
		public SharedAttachmentsModel()
		{
			TableName = "shared_attachments";
			TableSortBy = "attachment_id";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedAttachmentsModel"/> class.
		/// </summary>
		/// <param name="id">ID.</param>
        /// <param name="comment_id">Comment ID.</param>
        /// <param name="type">Attachment Type.</param>
        /// <param name="name">File name.</param>
        /// <param name="description">Description.</param>
        /// <param name="file_path">File Path.</param>
        public SharedAttachmentsModel(int id, int comment_id, int type, string name, string description, string file_path)
            : this()
		{
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Name));
            }
            if (String.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Description));
            }
            if (String.IsNullOrEmpty(file_path))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_File));
            }
            try
            {
				ID = id;
                CommentID = comment_id;
                Type = type;
                Name = name;
                Description = description;
                File = file_path;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
		private int _id;
		/// <summary>
		/// Gets or sets the ID
		/// </summary>
		/// <value>The ID.</value>
		[Column(PrimaryKey = true, Required = true, AutoIncrement = true, Name = "attachment_id")] 
		public int ID
		{
			get { return _id; }
			set
			{
				if (value != _id)
				{
					_id = value;
					RaisePropertyChanged("ID");
				}
			}
		}

        private int _comment_id;
        /// <summary>
        /// Gets or sets the comment ID
        /// </summary>
        /// <value>comment ID.</value>
        [Column(ForeignKey = true, Required = true, AutoSize = true,  ReadOnly = true, Title = ModelProperties.Header_CommentID, Name = "comment_id")] 
        public int CommentID
        {
            get { return _comment_id; }
            set
            {
                if (value != _comment_id)
                {
                    _comment_id = value;
                    RaisePropertyChanged("CommentID");
                }
            }
        }

        private int _type;
        /// <summary>
        /// Gets or sets the attachment type
        /// </summary>
        /// <value>the attachment type.</value>
        [Column(ForeignKey = true, Required = true, AutoSize = true, ReadOnly = true, Title = ModelProperties.Header_Type, Name = "attachment_type")]
        [Filter(Field = "owner", Value = "F")]
        public int Type
        {
            get { return _type; }
            set
            {
                if (value != _type)
                {
                    _type = value;
                    RaisePropertyChanged("Type");
                }
            }
        }

        private string _name;
        /// <summary>
        /// The name.
        /// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Name)] 
        public string Name
        {
            get { return _name; }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        private string _description;
        /// <summary>
        /// The description.
        /// </summary>
        [Column(Field = FieldType.Text, Title = ModelProperties.Header_Description)] 
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }

        private string _file;
        /// <summary>
        /// The file path.
        /// </summary>
        [Column(Required = true, ReadOnly = true, Title = ModelProperties.Header_File, Name = "file_path", Field = FieldType.File)] 
        public string File
        {
            get { return _file; }
            set
            {
                if (value != _file)
                {
                    _file = value;
                    RaisePropertyChanged("File");
                }
            }
        }

        private static TableAdapterExt _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TableAdapterExt TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TableAdapterExt();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = Convert.ToInt32(record["attachment_id"]); ;
                CommentID = Convert.ToInt32(record["comment_id"]); ;
                Type = Convert.ToInt32(record["attachment_type"]); ;
                Name = record["name"].ToString();
                Description = record["description"].ToString();
                File = record["file_path"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["attachment_id"] = this.ID;
                row["comment_id"] = this.CommentID;
                row["attachment_type"] = this.Type;
                row["name"] = this.Name;
                row["description"] = this.Description;
                row["file_path"] = this.File;
            }

            return row;
        }
       	#endregion

        /// <summary>
        /// Child class of table adapter to perform other table tasks
        /// </summary>
        public sealed class TableAdapterExt : Data.TheProfessionalDataSetTableAdapters.shared_attachmentsTableAdapter
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TheProfessional.Model.SharedAttachmentsModel.TableAdapterExt"/> class.
            /// </summary>
            public TableAdapterExt()
            {
            }

            /// <summary>
            /// Gets/Sets a collection of commands this adapater is responsible for
            /// </summary>
            public string[] ModelQueryCollection
            {
                get
                {
                    return new string[] { 
                        "SELECT * FROM shared_attachments " +
                            "ORDER BY attachment_id",

                            "SELECT * FROM shared_attachments " +
                            "WHERE attachment_type = @attachment_type " +
                            "ORDER BY attachment_id",
                    };
                }
            }

            /// <summary>
            /// Gets a command collection based on the given query index
            /// </summary>
            /// <param name="index"></param>
            /// <returns></returns>
            public global::MySql.Data.MySqlClient.MySqlCommand ModelCommand(int index)
            {
                return new global::MySql.Data.MySqlClient.MySqlCommand
                {
                    Connection = base.Connection,
                    CommandText = ModelQueryCollection[index],
                    CommandType = global::System.Data.CommandType.Text
                };
            }

            /// <summary>
            /// Gets the data by owner
            /// </summary>
            /// <param name="offset"></param>
            /// <param name="limit"></param>
            /// <returns></returns>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, false)]
            public DataTable GetData(int offset = -1, int limit = -1)
            {
                this.Adapter.SelectCommand = ModelCommand(0);

                if (offset != -1 && limit != -1)
                {
                    base.Adapter.SelectCommand.CommandText += String.Format(" LIMIT {0}, {1}", offset, limit);
                }

                DataTable dataTable = BaseModel.Database.GetDataTableByAdapter("shared_attachments", base.Adapter.SelectCommand.CommandText);
                return dataTable;
            }

            /// <summary>
            /// Gets the data by owner
            /// </summary>
            /// <param name="attachment_type"></param>
            /// <param name="offset"></param>
            /// <param name="limit"></param>
            /// <returns></returns>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, false)]
            public DataTable GetDataByType(int attachment_type, int offset = -1, int limit = -1)
            {
                this.Adapter.SelectCommand = ModelCommand(1);
                if ((attachment_type == 0))
                {
                    throw new global::System.ArgumentNullException("attachment_type");
                }
                else
                {
                    this.Adapter.SelectCommand.CommandText = this.Adapter.SelectCommand.CommandText.Replace("@attachment_type", attachment_type.ToString());
                }

                if (offset != -1 && limit != -1)
                {
                    base.Adapter.SelectCommand.CommandText += String.Format(" LIMIT {0}, {1}", offset, limit);
                }

                DataTable dataTable = BaseModel.Database.GetDataTableByAdapter("shared_attachments", base.Adapter.SelectCommand.CommandText);
                return dataTable;
            }
        }
    }
}