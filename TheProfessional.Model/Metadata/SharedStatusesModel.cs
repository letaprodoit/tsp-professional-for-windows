using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class SharedStatusesModel : BaseModel, IModel
    {
		/// <summary>
		/// Shared statuses owners.
		/// </summary>
		public enum StatusOwner
		{
			/// <summary>
			/// Empty enum
			/// </summary>
			[DefaultValue("")]
			None = 0,
			/// <summary>
			/// The status is associated with a user record.
			/// </summary>
			[DefaultValue("U")]
			User = 1,
			/// <summary>
			/// The status is associated with a project record.
			/// </summary>
			[DefaultValue("P")]
			Project = 2,
			/// <summary>
			/// The status is associated with a task record.
			/// </summary>
			[DefaultValue("T")]
			Task = 3,
			/// <summary>
			/// The status is associated with a work record.
			/// </summary>
			[DefaultValue("W")]
			Work = 4,
			/// <summary>
			/// The type is associated with an todo/activity type.
			/// </summary>
			[DefaultValue("D")]
			Activity = 12,
		};

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedStatusesModel"/> class.
		/// </summary>
		public SharedStatusesModel()
		{
			TableName = "shared_statuses";
			TableSortBy = "owner";
		}
			
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedStatusesModel"/> class.
		/// </summary>
		/// <param name="id">ID.</param>
        /// <param name="owner">Owner.</param>
        /// <param name="status">Status.</param>
        /// <param name="description">Description.</param>
		public SharedStatusesModel(int id, string owner, string @status,  
			string description ):this()
		{
            if (String.IsNullOrEmpty(owner))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Owner));
            }
            if (String.IsNullOrEmpty(status))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Status));
            }
            if (String.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Description));
            }
            try
            {
				ID = id;
                Owner = (StatusOwner)EnumToValueConverter.GetEnumValue(owner, typeof(StatusOwner));
                Status = @status[0];
                Description = description;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedStatusesModel"/> class.
		/// </summary>
		/// <param name="id">ID.</param>
        /// <param name="owner">Owner.</param>
        /// <param name="status">Status.</param>
        /// <param name="description">Description.</param>
        /// <param name="background">Background Color.</param>
        /// <param name="foreground">Foreground Color (color of the text).</param>
		public SharedStatusesModel(int id, string owner, string status, string description, string background, string foreground)
			: this( id, owner, status, description )
		{
            if (String.IsNullOrEmpty(background))
            {
                background = LibraryProperties.Resources.DefaultValue_COLOR; // transparent
            }
            if (String.IsNullOrEmpty(foreground))
            {
                foreground = Colors.Black.ToString();
            }

            try
            {
                Background = (Color)ColorConverter.ConvertFromString(background);
                Foreground = (Color)ColorConverter.ConvertFromString(foreground);
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
		private int _id;
		/// <summary>
		/// Gets or sets the id of the status.
		/// </summary>
		/// <value>The id of the status.</value>
		[Column(PrimaryKey = true, Required = true, AutoIncrement = true, Name = "status_id")] 
		public int ID
		{
			get { return _id; }
			set
			{
				if (value != _id)
				{
					_id = value;
					RaisePropertyChanged("ID");
				}
			}
		}

		private StatusOwner _owner;
		/// <summary>
		/// Gets or sets the owner of the status.
		/// </summary>
		/// <value>The type of the target.</value>
        [Column(Required = true, Sort = true, Title = ModelProperties.Header_Owner)] 
		public StatusOwner Owner
        {
            get { return _owner; }
            set
            {
                if (value != _owner)
                {
                    _owner = value;
                    RaisePropertyChanged("Owner");
                }
            }
        }

        private char _status;
        /// <summary>
        /// The status.
        /// </summary>
        [Column(PrimaryKey = true, Required = true, Title = ModelProperties.Header_Status)] 
        public char Status
        {
            get { return _status; }
            set
            {
                if (value != _status)
                {
                    _status = value;
                    RaisePropertyChanged("Status");
                }
            }
        }

        private string _description;
        /// <summary>
        /// The description.
        /// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Description)] 
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }

        private Color _background;
        /// <summary>
        /// The background color.
        /// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Background)] 
        public Color Background
        {
            get { return _background; }
            set
            {
                if (value != _background)
                {
                    _background = value;
                    RaisePropertyChanged("Background");
                }
            }
        }

        private Color _foreground;
        /// <summary>
        /// The foreground color.
        /// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Foreground)] 
        public Color Foreground
        {
            get { return _foreground; }
            set
            {
                if (value != _foreground)
                {
                    _foreground = value;
                    RaisePropertyChanged("Foreground");
                }
            }
        }

        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_statusesTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_statusesTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_statusesTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
				ID = Convert.ToInt32(record["status_id"]);
                Owner = (StatusOwner)EnumToValueConverter.GetEnumValue(record["owner"].ToString(), typeof(StatusOwner));
                Status = @record["status"].ToString()[0];
                Description = record["description"].ToString();
                Background = (Color)ColorConverter.ConvertFromString(record["background"].ToString());
                Foreground = (Color)ColorConverter.ConvertFromString(record["foreground"].ToString());
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
				row[ "status_id" ] = this.ID;
                row["owner"] = EnumToValueConverter.GetDefaultValue(this.Owner);
                row["status"] = this.Status.ToString();
                row["background"] = this.Background.ToString();
                row["foreground"] = this.Foreground.ToString();
                row["description"] = this.Description;
            }

            return row;
        }
        #endregion
    }
}