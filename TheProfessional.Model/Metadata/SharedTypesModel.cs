using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class SharedTypesModel : BaseModel, IModel
    {

		/// <summary>
		/// Shared statuses owners.
		/// </summary>
		/// 
		public enum TypeOwner
		{
			/// <summary>
			/// Empty enum
			/// </summary>
			[DefaultValue("")]
			None = 0,
			/// <summary>
			/// The type is associated with a user record.
			/// </summary>
			[DefaultValue("U")]
			User = 1,
			/// <summary>
			/// The type is associated with a project record.
			/// </summary>
			[DefaultValue("P")]
			Project = 2,
			/// <summary>
			/// The type is associated with a resource record.
			/// </summary>
			[DefaultValue("R")]
			Resource = 5,
			/// <summary>
			/// The type is associated with a comment record.
			/// </summary>
			[DefaultValue("M")]
			Comment = 6,
			/// <summary>
			/// The type is associated with an attachment/file record.
			/// </summary>
			[DefaultValue("F")]
			Attachment = 8,
			/// <summary>
			/// The type is associated with a company record.
			/// </summary>
			[DefaultValue("C")]
			Company = 9,
			/// <summary>
			/// The type is associated with an profile/address record.
			/// </summary>
			[DefaultValue("A")]
			Profile = 10,
			/// <summary>
			/// The type is associated with an billing type.
			/// </summary>
			[DefaultValue("B")]
			Billing = 11,
			/// <summary>
			/// The type is associated with an activity/todo type.
			/// </summary>
			[DefaultValue("D")]
			Activity = 12,
		};

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedTypesModel"/> class.
		/// </summary>
		public SharedTypesModel()
		{
			TableName = "shared_types";
			TableSortBy = "owner";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedTypesModel"/> class.
		/// </summary>
		/// <param name="id">ID.</param>
        /// <param name="owner">Owner.</param>
        /// <param name="type">Responsibility.</param>
        /// <param name="description">Description.</param>
		public SharedTypesModel(int id, string owner, string type,  
			string description ):this()
		{
            if (String.IsNullOrEmpty(owner))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Owner));
            }
            if (String.IsNullOrEmpty(type))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Type));
            }
            try
            {
				ID = id;
                Owner = (TypeOwner)EnumToValueConverter.GetEnumValue(owner, typeof(TypeOwner));
                Type = type;
                Description = description;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
		private int _id;
		/// <summary>
		/// Gets or sets the id of the status.
		/// </summary>
		/// <value>The id of the status.</value>
		[Column(PrimaryKey = true, Required = true, AutoIncrement = true, Name = "type_id")] 
		public int ID
		{
			get { return _id; }
			set
			{
				if (value != _id)
				{
					_id = value;
					RaisePropertyChanged("ID");
				}
			}
		}

		private TypeOwner _owner;
		/// <summary>
		/// Gets or sets the owner of the status.
		/// </summary>
		/// <value>The type of the target.</value>
		[Column(Required = true, Sort = true, AutoSize = true, Title = ModelProperties.Header_Owner)] 
		public TypeOwner Owner
        {
            get { return _owner; }
            set
            {
                if (value != _owner)
                {
                    _owner = value;
                    RaisePropertyChanged("Owner");
                }
            }
        }

        private string _type;
        /// <summary>
        /// The status.
        /// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Type)] 
        public string Type
        {
            get { return _type; }
            set
            {
                if (value != _type)
                {
                    _type = value;
                    RaisePropertyChanged("Type");
                }
            }
        }

        private string _description;
        /// <summary>
        /// The description.
        /// </summary>
        [Column(Field = FieldType.Text, Title = ModelProperties.Header_Description)] 
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }

        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_typesTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_typesTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_typesTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
				ID = Convert.ToInt32(record["type_id"]);
                Owner = (TypeOwner)EnumToValueConverter.GetEnumValue(record["owner"].ToString(), typeof(TypeOwner));
                Type = record["type"].ToString();
                Description = record["description"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
				row["type_id" ] = this.ID;
                row["owner"] = EnumToValueConverter.GetDefaultValue(this.Owner);
                row["type"] = this.Type;
                row["description"] = this.Description;
            }

            return row;
        }
       	#endregion
    }
}