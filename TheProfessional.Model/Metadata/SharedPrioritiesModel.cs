using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
	/// <summary>
	/// Shared statuses model.
	/// </summary>
	public class SharedPrioritiesModel : BaseModel, IModel
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedPrioritiesModel"/> class.
		/// </summary>
		public SharedPrioritiesModel()
		{
			TableName = "shared_priorities";
			TableSortBy = "priority_id";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedPrioritiesModel"/> class.
		/// </summary>
		/// <param name="id">ID.</param>
		/// <param name="priority">Priority.</param>
		public SharedPrioritiesModel(int id, string priority ):this()
		{
			if (String.IsNullOrEmpty(priority))
			{
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Priority));
			}
			try
			{
				ID = id;
				Priority = priority;
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

		#region Properties
		private int _id;
		/// <summary>
		/// Gets or sets the id of the status.
		/// </summary>
		/// <value>The id of the status.</value>
		[Column(PrimaryKey = true, Required = true, AutoIncrement = true, Name = "priority_id")] 
		public int ID
		{
			get { return _id; }
			set
			{
				if (value != _id)
				{
					_id = value;
					RaisePropertyChanged("ID");
				}
			}
		}

		private string _priority;
		/// <summary>
		/// The priority.
		/// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Priority)] 
		public string Priority
		{
			get { return _priority; }
			set
			{
				if (value != _priority)
				{
					_priority = value;
					RaisePropertyChanged("priority");
				}
			}
		}

        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_prioritiesTableAdapter _tableAdapter;
		/// <summary>
		/// Gets the model table's adapter
		/// </summary>
		/// <value>The model table's adapter.</value>
		[Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_prioritiesTableAdapter TableAdapter
		{
			get
			{
				if (_tableAdapter == null)
				{

					_tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.shared_prioritiesTableAdapter();
				}
				return _tableAdapter;
			}
		}
		#endregion

        #region IModel Implementation: Methods
        /// <summary>
		/// Updates this model from DataRowView
		/// </summary>
		/// <returns>none</returns>
		/// <param name="record">record.</param>
		public void UpdateFromView(DataRowView record)
		{
			if (record == null)
				return;

			try
			{
				ID = Convert.ToInt32(record["priority_id"]);
				Priority = record["priority"].ToString();
			}
			catch (Exception ex)
			{
				// Get call stack
				StackTrace stackTrace = new StackTrace();
				String methodName = stackTrace.GetFrame(1).GetMethod().Name;

				// Get calling method name
				Trace.WriteLine(methodName + ": " + ex.Message);
			}
		}

		/// <summary>
		/// Copies the current data to the DataRow.
		/// </summary>
		/// <returns>The data row.</returns>
		/// <param name="row">Row.</param>
		public DataRow CopyToDataRow(DataRow row)
		{
			if (row != null)
			{
				row["priority_id" ] = this.ID;
				row["priority"] = this.Priority;
			}

			return row;
		}
		#endregion
    }
}