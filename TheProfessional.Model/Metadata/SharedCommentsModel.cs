using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class SharedCommentsModel : BaseModel, IModel
    {

        /// <summary>
        /// Shared comment owners.
        /// </summary>
        public enum CommentOwner
        {
            /// <summary>
            /// Empty enum
            /// </summary>
            [DefaultValue("")]
            None = 0,
            /// <summary>
            /// The status is associated with a user record.
            /// </summary>
            [DefaultValue("U")]
            User = 1,
            /// <summary>
            /// The status is associated with a project record.
            /// </summary>
            [DefaultValue("P")]
            Project = 2,
            /// <summary>
            /// The status is associated with a task record.
            /// </summary>
            [DefaultValue("T")]
            Task = 3,
            /// <summary>
            /// The status is associated with a work record.
            /// </summary>
            [DefaultValue("W")]
            Work = 4,
            /// <summary>
            /// The type is associated with a resource record.
            /// </summary>
            [DefaultValue("R")]
            Resource = 5,
            /// <summary>
            /// The type is associated with a invoice record.
            /// </summary>
            [DefaultValue("I")]
            Invoice = 7,
            /// <summary>
            /// The type is associated with an todo type.
            /// </summary>
            [DefaultValue("D")]
            Todo = 12,
        };

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedCommentsModel"/> class.
		/// </summary>
		public SharedCommentsModel()
		{
			TableName = "shared_comments";
			TableSortBy = "owner";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.SharedCommentsModel"/> class.
		/// </summary>
		/// <param name="id">ID.</param>
        /// <param name="owner">Owner.</param>
        /// <param name="owner_id">Owner ID.</param>
        /// <param name="type">Comment Type.</param>
        /// <param name="submitter_id">Submitter.</param>
        /// <param name="description">Description.</param>
        public SharedCommentsModel(int id, string owner, int owner_id, int type,
            int submitter_id, string description)
            : this()
		{
            if (String.IsNullOrEmpty(owner))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Owner));
            }
            if (String.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Description));
            }
            try
            {
				ID = id;
                Owner = (CommentOwner)EnumToValueConverter.GetEnumValue(owner, typeof(CommentOwner));
                OwnerID = owner_id;
                Type = type;
                Submitter = submitter_id;
                Description = description;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
		private int _id;
		/// <summary>
		/// Gets or sets the id of the status.
		/// </summary>
		/// <value>The id of the status.</value>
		[Column(PrimaryKey = true, Required = true, AutoIncrement = true, Name = "comment_id")] 
		public int ID
		{
			get { return _id; }
			set
			{
				if (value != _id)
				{
					_id = value;
					RaisePropertyChanged("ID");
				}
			}
		}

		private CommentOwner _owner;
		/// <summary>
		/// Gets or sets the owner of the status.
		/// </summary>
		/// <value>The type of the target.</value>
        [Column(Required = true, Sort = true, AutoSize = true, ReadOnly = true, Title = ModelProperties.Header_Owner)] 
		public CommentOwner Owner
        {
            get { return _owner; }
            set
            {
                if (value != _owner)
                {
                    _owner = value;
                    RaisePropertyChanged("Owner");
                }
            }
        }

        private int _owner_id;
        /// <summary>
        /// The ID of the owner. The ID represents where the comment is placed.
        /// </summary>
        [Column(Required = true, ReadOnly = true, Title = ModelProperties.Header_OwnerID, Name = "owner_id")] 
        public int OwnerID
        {
            get { return _owner_id; }
            set
            {
                if (value != _owner_id)
                {
                    _owner_id = value;
                    RaisePropertyChanged("OwnerID");
                }
            }
        }

        private int _type;
        /// <summary>
        /// The status.
        /// </summary>
        [Column(ForeignKey = true, ReadOnly = true, Required = true, AutoSize = true, Title = ModelProperties.Header_Type, Name = "comment_type")]
        [Filter(Field = "owner", Value = "M")]
        public int Type
        {
            get { return _type; }
            set
            {
                if (value != _type)
                {
                    _type = value;
                    RaisePropertyChanged("Type");
                }
            }
        }

        private int _submitter;
        /// <summary>
        /// The ssubmitter.
        /// </summary>
        [Column(ForeignKey = true, ReadOnly = true, AutoSize = true, Required = true, Title = ModelProperties.Header_Submitter, Name = "submitter_id")] 
        public int Submitter
        {
            get { return _submitter; }
            set
            {
                if (value != _submitter)
                {
                    _submitter = value;
                    RaisePropertyChanged("Submitter");
                }
            }
        }

        private string _description;
        /// <summary>
        /// The description.
        /// </summary>
        [Column(ReadOnly = true, Required = true, AutoSize = true, Field = FieldType.Text, Title = ModelProperties.Header_Description)] 
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }

        private static TableAdapterExt _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TableAdapterExt TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TableAdapterExt();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
				ID = Convert.ToInt32(record["comment_id"]);
                Owner = (CommentOwner)EnumToValueConverter.GetEnumValue(record["owner"].ToString(), typeof(CommentOwner));
                OwnerID = Convert.ToInt32(record["owner_id"]);
                Type = Convert.ToInt32(record["comment_type"]);
                Submitter = Convert.ToInt32(record["submitter_id"]);
                Description = record["description"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["comment_id"] = this.ID;
                row["owner"] = EnumToValueConverter.GetDefaultValue(this.Owner);
                row["owner_id"] = this.OwnerID;
                row["comment_type"] = this.Type;
                row["submitter_id"] = this.Submitter;
                row["description"] = this.Submitter;
            }

            return row;
        }
       	#endregion

        /// <summary>
        /// Child class of table adapter to perform other table tasks
        /// </summary>
        public sealed class TableAdapterExt : Data.TheProfessionalDataSetTableAdapters.shared_commentsTableAdapter
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TheProfessional.Model.SharedCommentsModel.TableAdapterExt"/> class.
            /// </summary>
            public TableAdapterExt()
            {
            }

            /// <summary>
            /// Gets/Sets a collection of commands this adapater is responsible for
            /// </summary>
            public string[] ModelQueryCollection
            {
                get
                {
                    return new string[] { 
                        "SELECT * FROM shared_comments " +
                            "ORDER BY owner",

                            "SELECT * FROM shared_comments " +
                            "WHERE owner = '@owner' " +
                            "ORDER BY owner",
                    };
                }
            }

            /// <summary>
            /// Gets a command collection based on the given query index
            /// </summary>
            /// <param name="index"></param>
            /// <returns></returns>
            public global::MySql.Data.MySqlClient.MySqlCommand ModelCommand(int index)
            {
                return new global::MySql.Data.MySqlClient.MySqlCommand
                {
                    Connection = base.Connection,
                    CommandText = ModelQueryCollection[index],
                    CommandType = global::System.Data.CommandType.Text
                };
            }

            /// <summary>
            /// Gets the data by owner
            /// </summary>
            /// <param name="offset"></param>
            /// <param name="limit"></param>
            /// <returns></returns>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, false)]
            public DataTable GetData(int offset = -1, int limit = -1)
            {
                this.Adapter.SelectCommand = ModelCommand(0);

                if (offset != -1 && limit != -1)
                {
                    base.Adapter.SelectCommand.CommandText += String.Format(" LIMIT {0}, {1}", offset, limit);
                }

                DataTable dataTable = BaseModel.Database.GetDataTableByAdapter("shared_comments", base.Adapter.SelectCommand.CommandText);
                return dataTable;
            }

            /// <summary>
            /// Gets the data by owner
            /// </summary>
            /// <param name="owner"></param>
            /// <param name="offset"></param>
            /// <param name="limit"></param>
            /// <returns></returns>
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "4.0.0.0")]
            [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
            [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, false)]
            public DataTable GetDataByOwner(string owner, int offset = -1, int limit = -1)
            {
                this.Adapter.SelectCommand = ModelCommand(1);
                if ((owner == null))
                {
                    throw new global::System.ArgumentNullException("owner");
                }
                else
                {
                    base.Adapter.SelectCommand.CommandText = this.Adapter.SelectCommand.CommandText.Replace("@owner", owner);
                }

                if (offset != -1 && limit != -1)
                {
                    base.Adapter.SelectCommand.CommandText += String.Format(" LIMIT {0}, {1}", offset, limit);
                }

                DataTable dataTable = BaseModel.Database.GetDataTableByAdapter("shared_comments", base.Adapter.SelectCommand.CommandText);
                return dataTable;
            }
        }
    }
}