﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheProfessional.Model
{
	/// <summary>
	/// Model properties.
	/// </summary>
	public static class ModelProperties
    {
        /// <summary>
        /// The header_ background.
        /// </summary>
        public const string Header_Background = "Background";
        /// <summary>
        /// The header_ category.
        /// </summary>
        public const string Header_Category = "Category";
        /// <summary>
        /// The header_ code.
        /// </summary>
        public const string Header_Code = "Code";
        /// <summary>
        /// The header_ comment.
        /// </summary>
        public const string Header_Comment = "Comment";
        /// <summary>
        /// The header_ comment ID.
        /// </summary>
        public const string Header_CommentID = "Comment #";
        /// <summary>
        /// The header_ country.
        /// </summary>
        public const string Header_Country = "Country";
        /// <summary>
        /// The header_ country code.
        /// </summary>
        public const string Header_CountryCode = "Country Code";
        /// <summary>
        /// The header_ description.
        /// </summary>
        public const string Header_Description = "Description";
        /// <summary>
        /// The header_ file.
        /// </summary>
        public const string Header_File = "File";
        /// <summary>
        /// The header_ foreground.
        /// </summary>
        public const string Header_Foreground = "Foreground";
        /// <summary>
        /// The header_ ISO a3.
        /// </summary>
        public const string Header_ISOA3 = "ISO A3";
        /// <summary>
        /// The header_ ISO n3.
        /// </summary>
        public const string Header_ISON3 = "ISO N3";
        /// <summary>
        /// The header_ lat.
        /// </summary>
        public const string Header_Lat = "Lat";
        /// <summary>
        /// The header_ lon.
        /// </summary>
        public const string Header_Lon = "Lon";
        /// <summary>
        /// The name of the header_.
        /// </summary>
        public const string Header_Name = "Name";
        /// <summary>
        /// The header_ owner.
        /// </summary>
        public const string Header_Owner = "Owner";
        /// <summary>
        /// The header_ owner I.
        /// </summary>
        public const string Header_OwnerID = "Owner #";
        /// <summary>
        /// The header_ priority.
        /// </summary>
        public const string Header_Priority = "Priority";
        /// <summary>
        /// The header_ profession.
        /// </summary>
        public const string Header_Profession = "Profession";
        /// <summary>
        /// The header_ region.
        /// </summary>
        public const string Header_Region = "Region";
        /// <summary>
        /// The header_ responsibility.
        /// </summary>
        public const string Header_Responsibility = "Responsibility";
        /// <summary>
        /// The state of the header_.
        /// </summary>
        public const string Header_State = "State";
        /// <summary>
        /// The header_ state code.
        /// </summary>
        public const string Header_StateCode = "State Code";
        /// <summary>
        /// The header_ status.
        /// </summary>
        public const string Header_Status = "Status";
        /// <summary>
        /// The header_ submitter.
        /// </summary>
        public const string Header_Submitter = "Submitter";
        /// <summary>
        /// The header_ title.
        /// </summary>
        public const string Header_Title = "Title";
        /// <summary>
        /// The type of the header_.
        /// </summary>
        public const string Header_Type = "Type";
        /// <summary>
        /// The header_ username.
        /// </summary>
        public const string Header_Username = "Username";
        /// <summary>
        /// The header_ user type.
        /// </summary>
        public const string Header_UserType = "User Type";
        /// <summary>
        /// The header_ password.
        /// </summary>
        public const string Header_Password = "Password";
        /// <summary>
        /// The name of the header_ FirstName.
        /// </summary>
        public const string Header_FirstName = "First Name";
        /// <summary>
        /// The name of the header_ LastName.
        /// </summary>
        public const string Header_LastName = "Last Name";
        /// <summary>
        /// The header_ date created.
        /// </summary>
        public const string Header_DateCreated = "Date Created";
        /// <summary>
        /// The header_ last update.
        /// </summary>
        public const string Header_LastUpdate = "Last Update";
        /// <summary>
        /// The header_ last login.
        /// </summary>
        public const string Header_LastLogin = "Last Login";
        /// <summary>
        /// The type of the header_ profile.
        /// </summary>
        public const string Header_ProfileType = "Profile Type";
        /// <summary>
        /// The header_ user I.
        /// </summary>
        public const string Header_User = "User";
        /// <summary>
        /// The header_ company ID.
        /// </summary>
        public const string Header_Company = "Company";
        /// <summary>
        /// The name of the header_ company.
        /// </summary>
        public const string Header_CompanyName = "Company Name";
        /// <summary>
        /// The type of the header_ company.
        /// </summary>
        public const string Header_CompanyType = "Company Type";
        /// <summary>
        /// The header_ address.
        /// </summary>
        public const string Header_Address = "ContactInfo (Line #1)";
        /// <summary>
        /// The header_ address2.
        /// </summary>
        public const string Header_Address2 = "ContactInfo (Line #2)";
        /// <summary>
        /// The header_ city.
        /// </summary>
        public const string Header_City = "City";
        /// <summary>
        /// The header_ zip code.
        /// </summary>
        public const string Header_ZipCode = "Zip Code";
        /// <summary>
        /// The header_ email.
        /// </summary>
        public const string Header_Email = "Email";
        /// <summary>
        /// The header_ phone1.
        /// </summary>
        public const string Header_Phone1 = "Phone #1";
        /// <summary>
        /// The header_ phone2.
        /// </summary>
        public const string Header_Phone2 = "Phone #2";
        /// <summary>
        /// The header_ fax.
        /// </summary>
        public const string Header_Fax = "Fax";
        /// <summary>
        /// The header_ URL.
        /// </summary>
        public const string Header_URL = "URL";
        /// <summary>
        /// The type of the header_ contact.
        /// </summary>
        public const string Header_ContactType = "Contact Type";
    }
}
