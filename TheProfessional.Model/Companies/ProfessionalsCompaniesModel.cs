using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class ProfessionalsCompaniesModel : BaseModel, IModel
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionalsCompaniesModel"/> class.
		/// </summary>
		public ProfessionalsCompaniesModel()
		{
			TableName = "professionals_companies";
			TableSortBy = "company_id";
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionalsCompaniesModel"/> class.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="company_type"></param>
        /// <param name="company_name"></param>
        /// <param name="user_id"></param>
        public ProfessionalsCompaniesModel(int id, int company_type, string company_name, int user_id)
            : this()
		{
            if (String.IsNullOrEmpty(company_name))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_CompanyName));
            }
            try
            {
                ID = id;
                CompanyType = company_type;
                CompanyName = company_name;
                UserID = user_id;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private int _id;
        /// <summary>
        /// Gets the company's ID
        /// </summary>
        /// <value>The company's ID.</value>
        [Column(Required = true, AutoIncrement = true, PrimaryKey = true, Sort = true, Name = "company_id")]
        public int ID
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        private int _companyType;
        /// <summary>
        /// Gets the company's type
        /// </summary>
        /// <value>The company's type.</value>
        [Column(ForeignKey = true, Required = true, Name = "company_type", Title = ModelProperties.Header_CompanyType)]
        public int CompanyType
        {
            get { return _companyType; }
            set
            {
                if (value != _companyType)
                {
                    _companyType = value;
                    RaisePropertyChanged("CompanyType");
                }
            }
        }

        private string _companyName;
        /// <summary>
        /// The title.
        /// </summary>
        [Column(Required = true, AutoSize = true, Name = "company_name", Title = ModelProperties.Header_CompanyName)] 
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                if (value != _companyName)
                {
                    _companyName = value;
                    RaisePropertyChanged("CompanyName");
                }
            }
        }

        private string _description;
        /// <summary>
        /// The description.
        /// </summary>
        [Column(HideInGrid = true, Field = FieldType.Text, Title = ModelProperties.Header_Description)]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }

        private int _userID;
        /// <summary>
        /// The company's user ID.
        /// </summary>
        [Column(HideInGrid = true, ForeignKey = true, Name = "user_id")]
        public int UserID
        {
            get { return _userID; }
            set
            {
                if (value != _userID)
                {
                    _userID = value;
                    RaisePropertyChanged("UserIDx");
                }
            }
        }

        private DateTime _dateCreated;
        /// <summary>
        /// The date the record was created.
        /// </summary>
        [Column(HideInGrid = true, ReadOnly = true, Field = FieldType.Date, Name = "date_created", Title = ModelProperties.Header_DateCreated)]
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set
            {
                if (value != _dateCreated)
                {
                    _dateCreated = value;
                    RaisePropertyChanged("DateCreated");
                }
            }
        }

        private DateTime _lateUpdate;
        /// <summary>
        /// The date the record was last updated.
        /// </summary>
        [Column(HideInGrid = true, ReadOnly = true, Field = FieldType.Date, Name = "date_last_update", Title = ModelProperties.Header_LastUpdate)]
        public DateTime LastUpdate
        {
            get { return _lateUpdate; }
            set
            {
                if (value != _lateUpdate)
                {
                    _lateUpdate = value;
                    RaisePropertyChanged("LastUpdate");
                }
            }
        }

        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_companiesTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_companiesTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_companiesTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = (int)record["company_id"];
                CompanyType = (int)record["company_type"];
                CompanyName = record["name"].ToString();
                UserID = (int)record["user_id"];
                Description = record["description"].ToString();
                DateCreated = (DateTime)record["date_created"];
                LastUpdate = (DateTime)record["date_last_update"];
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["company_id"] = this.ID;
                row["company_type"] = this.CompanyType;
                row["name"] = this.CompanyName;
                row["user_id"] = this.UserID;
                row["description"] = this.Description;
                row["date_created"] = this.DateCreated;
                row["date_last_update"] = this.LastUpdate;
            }

            return row;
        }
        #endregion
    }
}