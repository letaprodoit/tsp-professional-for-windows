using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class ProfessionalsModel : BaseModel, IModel
    {
		/// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionalsModel"/> class.
		/// </summary>
		public ProfessionalsModel()
		{
			TableName = "professionals";
			TableSortBy = "user_id";
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionalsModel"/> class.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="user_type"></param>
        /// <param name="profession_id"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="first_name"></param>
        /// <param name="last_name"></param>
        public ProfessionalsModel(int id, int status, int user_type, int profession_id, string username, string password, string first_name, string last_name)
            : this()
		{
            if (String.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Username));
            }
            else if (String.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Password));
            }
            else if (String.IsNullOrEmpty(first_name))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_FirstName));
            }
            else if (String.IsNullOrEmpty(last_name))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_LastName));
            }
            try
            {
                ID = id;
                Status = status;
                UserType = user_type;
                Profession = profession_id;
                Username = username;
                Password = password;
                FName = first_name;
                LName = last_name;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private int _id;
        /// <summary>
        /// Gets the user's ID
        /// </summary>
        /// <value>The user's ID.</value>
        [Column(Required = true, AutoIncrement = true, PrimaryKey = true, Sort = true, Name = "user_id")]
        public int ID
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        private int _status;
        /// <summary>
        /// Gets the user's status
        /// </summary>
        /// <value>The user's status.</value>
        [Column(Required = true, ForeignKey = true)]
        public int Status
        {
            get { return _status; }
            set
            {
                if (value != _status)
                {
                    _status = value;
                    RaisePropertyChanged("Status");
                }
            }
        }

        private int _userType;
        /// <summary>
        /// Gets the user's type
        /// </summary>
        /// <value>The user's type.</value>
        [Column(ForeignKey = true, Required = true, Name = "user_type", Title = ModelProperties.Header_UserType)]
        public int UserType
        {
            get { return _userType; }
            set
            {
                if (value != _userType)
                {
                    _userType = value;
                    RaisePropertyChanged("UserType");
                }
            }
        }

        private int _profession;
        /// <summary>
        /// Gets the user's profession's ID
        /// </summary>
        /// <value>The user's profession's ID.</value>
        [Column(ForeignKey = true, Required = true, Name = "profession_id", Title = ModelProperties.Header_Profession)]
        public int Profession
        {
            get { return _profession; }
            set
            {
                if (value != _profession)
                {
                    _profession = value;
                    RaisePropertyChanged("Profession");
                }
            }
        }

        private string _username;
        /// <summary>
        /// The title.
        /// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Username)] 
        public string Username
        {
            get { return _username; }
            set
            {
                if (value != _username)
                {
                    _username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }

        private string _password;
        /// <summary>
        /// The title.
        /// </summary>
        [Column(Ignore = true, Required = true, Title = ModelProperties.Header_Password)]
        public string Password
        {
            get { return _password; }
            set
            {
                if (value != _password)
                {
                    _password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }

        private string _salt;
        /// <summary>
        /// The salt.
        /// </summary>
        [Column(Ignore = true, Required = true)]
        public string Salt
        {
            get { return _salt; }
            set
            {
                if (value != _salt)
                {
                    _salt = value;
                    RaisePropertyChanged("Salt");
                }
            }
        }

        private string _fname;
        /// <summary>
        /// The user's first name.
        /// </summary>
        [Column(Required = true, AutoSize = true, Name = "first_name", Title = ModelProperties.Header_FirstName)]
        public string FName
        {
            get { return _fname; }
            set
            {
                if (value != _fname)
                {
                    _fname = value;
                    RaisePropertyChanged("FName");
                }
            }
        }

        private string _lname;
        /// <summary>
        /// The user's last name.
        /// </summary>
        [Column(Required = true, AutoSize = true, Name = "last_name", Title = ModelProperties.Header_LastName)]
        public string LName
        {
            get { return _lname; }
            set
            {
                if (value != _lname)
                {
                    _lname = value;
                    RaisePropertyChanged("LName");
                }
            }
        }

        private DateTime _dateCreated;
        /// <summary>
        /// The date the record was created.
        /// </summary>
        [Column(HideInGrid = true, ReadOnly = true, Field = FieldType.Date, Name = "date_created", Title = ModelProperties.Header_DateCreated)]
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set
            {
                if (value != _dateCreated)
                {
                    _dateCreated = value;
                    RaisePropertyChanged("DateCreated");
                }
            }
        }

        private DateTime _lateUpdate;
        /// <summary>
        /// The date the record was last updated.
        /// </summary>
        [Column(HideInGrid = true, ReadOnly = true, Field = FieldType.Date, Name = "date_last_update", Title = ModelProperties.Header_LastUpdate)]
        public DateTime LastUpdate
        {
            get { return _lateUpdate; }
            set
            {
                if (value != _lateUpdate)
                {
                    _lateUpdate = value;
                    RaisePropertyChanged("LastUpdate");
                }
            }
        }

        private DateTime _lastLogin;
        /// <summary>
        /// The user's last login.
        /// </summary>
        [Column(HideInGrid = true, ReadOnly = true, Field = FieldType.Date, Name = "date_last_login", Title = ModelProperties.Header_LastLogin)]
        public DateTime LastLogin
        {
            get { return _lastLogin; }
            set
            {
                if (value != _lastLogin)
                {
                    _lastLogin = value;
                    RaisePropertyChanged("LastLogin");
                }
            }
        }

        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionalsTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionalsTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionalsTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = (int)record["user_id"];
                UserType = (int)record["user_type"];
                Profession = (int)record["profession_id"];
                Username = record["username"].ToString();
                Password = record["password"].ToString();
                FName = record["first_name"].ToString();
                LName = record["last_name"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["user_id"] = this.ID;
                row["user_type"] = this.UserType;
                row["profession_id"] = this.Profession;
                row["username"] = this.Username;
                row["password"] = this.Password;
                row["first_name"] = this.FName;
                row["last_name"] = this.LName;
            }

            return row;
        }
        #endregion
    }
}