using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class ProfessionsModel : BaseModel, IModel
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionsModel"/> class.
		/// </summary>
		public ProfessionsModel()
		{
			TableName = "professionals_professions";
			TableSortBy = "title";
		}

        /// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionsModel"/> class.
		/// </summary>
        /// <param name="id">ID.</param>
        /// <param name="parent">Profession.</param>
        /// <param name="title">Responsibility.</param>
		public ProfessionsModel(int id, int? parent, string title ):this()
		{
            if (String.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Title));
            }
            try
            {
                ID = id;
                Category = parent;
                Title = title;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private int _id;
		/// <summary>
		/// Gets the profession id
		/// </summary>
		/// <value>The profession ID.</value>
		[Column(PrimaryKey = true, Required = true, AutoIncrement = true, Name = "profession_id")] 
		public int ID
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

		private int? _category;
		/// <summary>
		/// The parent category ID.
        /// </summary>
		[Column(HasDefault = true, ForeignKey = true, AutoSize = true, Title = ModelProperties.Header_Category, Name = "category_id")] 
        public int? Category
        { 
			get { return _category; }
            set
            {
				if (value != _category)
                {
					_category = value;
					RaisePropertyChanged("Category");
                }
            }
        }

        private string _title;
        /// <summary>
        /// The title.
        /// </summary>
        [Column(Required = true, AutoSize = true, Sort = true, Title = ModelProperties.Header_Title)] 
        public string Title
        {
            get { return _title; }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }

        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_professionsTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_professionsTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_professionsTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = (int)record["profession_id"];

                // if the database contains a DBNull value then
                // set the model's value to null
                // if the database contains a DBNull value then
                // set the model's value to null
                if (record["category_id"] == DBNull.Value)
                {
                    Category = null;
                }
                else
                {
                    Category = (int)record["category_id"];
                }

                Title = record["title"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["profession_id"] = this.ID;

                // Foreign keys can only have null values and the database
                // will recognize DBNull or any other non-null value
                if (this.Category == 0 || this.Category == null)
                {
                    row["category_id"] = DBNull.Value;
                }
                else
                {
                    row["category_id"] = this.Category;
                }

                row["title"] = this.Title;
            }

            return row;
        }
        #endregion
    }
}