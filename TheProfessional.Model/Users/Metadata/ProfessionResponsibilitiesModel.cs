using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
	public class ProfessionResponsibilitiesModel : BaseModel, IModel
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionResponsibilitiesModel"/> class.
		/// </summary>
		public ProfessionResponsibilitiesModel()
		{
			TableName = "professionals_responsibilities";
			TableSortBy = "responsibility_id";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionResponsibilitiesModel"/> class.
		/// </summary>
        /// <param name="id">ID.</param>
        /// <param name="profession">Profession.</param>
        /// <param name="responsibility">Responsibility.</param>
		public ProfessionResponsibilitiesModel(int id, int? profession, string responsibility ):this()
		{
            if (String.IsNullOrEmpty(responsibility))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Responsibility));
            }
            try
            {
                ID = id;
                Profession = profession;
                Responsibility = responsibility;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private int _id;
		/// <summary>
		/// Gets the profession id
		/// </summary>
		/// <value>The profession ID.</value>
		[Column(PrimaryKey = true, Required = true, AutoIncrement = true, Sort = true, Name = "responsibility_id")] 
		public int ID
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        private int? _profession;
        /// <summary>
        /// The profession ID.
        /// </summary>
        [Column(HasDefault = true, ForeignKey = true, AutoSize = true, Title = ModelProperties.Header_Profession, Name = "profession_id")] 
        public int? Profession
        { 
            get { return _profession; }
            set
            {
                if (value != _profession)
                {
                    _profession = value;
                    RaisePropertyChanged("Profession");
                }
            }
        }

        private string _responsibility;
        /// <summary>
        /// The title.
        /// </summary>
        [Column(Required = true, AutoSize = true, Title = ModelProperties.Header_Responsibility)] 
        public string Responsibility
        {
            get { return _responsibility; }
            set
            {
                if (value != _responsibility)
                {
                    _responsibility = value;
                    RaisePropertyChanged("Responsibility");
                }
            }
        }

        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_responsibilitiesTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_responsibilitiesTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_responsibilitiesTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = (int)record["responsibility_id"];

                // if the database contains a DBNull value then
                // set the model's value to null
                if (record["profession_id"] == DBNull.Value)
                {
                    Profession = null;
                }
                else
                {
                    Profession = (int)record["profession_id"];
                }

                Responsibility = record["responsibility"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["responsibility_id"] = this.ID;

                // Foreign keys can only have null values and the database
                // will recognize DBNull or any other non-null value
                if (this.Profession == 0 || this.Profession == null)
                {
                    row["profession_id"] = DBNull.Value;
                }
                else
                {
                    row["profession_id"] = this.Profession;
                }
                
                row["responsibility"] = this.Responsibility;
            }

            return row;
        }
        #endregion
    }
}