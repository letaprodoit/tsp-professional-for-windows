using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Diagnostics;
using TheProfessional.Library.Converters;
using TheProfessional.Library;
using LibraryResources = TheProfessional.Library.Properties.Resources;
using System.Reflection;

namespace TheProfessional.Model
{
    /// <summary>
	/// Shared statuses model.
	/// </summary>
    public class ProfessionalsProfilesModel : BaseModel, IModel
    {
        /// <summary>
        /// Profile owners.
        /// </summary>
        public enum ProfileOwner
        {
            /// <summary>
            /// Empty enum
            /// </summary>
            [DefaultValue("")]
            None = 0,
            /// <summary>
            /// The status is associated with a user record.
            /// </summary>
            [DefaultValue("U")]
            User = 1,
            /// <summary>
            /// The type is associated with a company record.
            /// </summary>
            [DefaultValue("C")]
            Company = 9,
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionalsProfilesModel"/> class.
		/// </summary>
		public ProfessionalsProfilesModel()
		{
			TableName = "professionals_profiles";
			TableSortBy = "profile_id";
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="TheProfessional.Model.ProfessionalsProfilesModel"/> class.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="profile_type"></param>
        /// <param name="owner"></param>
        /// <param name="owner_id"></param>
        /// <param name="profile_address"></param>
        public ProfessionalsProfilesModel(int id, int profile_type, string owner, int owner_id, Address profile_address)
            : this()
		{
            if (String.IsNullOrEmpty(owner))
            {
                throw new ArgumentNullException(String.Format(LibraryProperties.Resources.ErrorMessage_BAD_FIELD, ModelProperties.Header_Owner));
            }
            try
            {
                ID = id;
                ProfileType = profile_type;
                Owner = (ProfileOwner)EnumToValueConverter.GetEnumValue(owner, typeof(ProfileOwner));
                OwnerID = owner_id;
                ProfileAddress = profile_address;
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
		}

		/// <summary>
		/// Gets or sets the property with the specified columnName.
		/// </summary>
		/// <param name="dbColName">Database column name.</param>
        [Ignore]
        public object this[string dbColName]
        {
            get
            {
                string propertyName = base.GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                return prop.GetValue(this);
            }
            set
            {
                string propertyName = GetPropertyName(this, dbColName);
                PropertyInfo prop = this.GetType().GetProperty(propertyName);

                prop.SetValue(this, value);
            }
        }

        #region Properties
        private int _id;
        /// <summary>
        /// Gets the profile's ID
        /// </summary>
        /// <value>The profile's ID.</value>
        [Column(Required = true, AutoIncrement = true, PrimaryKey = true, Sort = true, Name = "profile_id")]
        public int ID
        {
            get { return _id; }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        private int _profileType;
        /// <summary>
        /// Gets the profile's type
        /// </summary>
        /// <value>The profile's type.</value>
        [Column(ForeignKey = true, Required = true, Name = "profile_type", Title = ModelProperties.Header_ProfileType)]
        public int ProfileType
        {
            get { return _profileType; }
            set
            {
                if (value != _profileType)
                {
                    _profileType = value;
                    RaisePropertyChanged("ProfileType");
                }
            }
        }


        private ProfileOwner _owner;
        /// <summary>
        /// Gets or sets the owner of the status.
        /// </summary>
        /// <value>The type of the target.</value>
        [Column(Required = true, Sort = true, Title = ModelProperties.Header_Owner)]
        public ProfileOwner Owner
        {
            get { return _owner; }
            set
            {
                if (value != _owner)
                {
                    _owner = value;
                    RaisePropertyChanged("Owner");
                }
            }
        }

        private int _owner_id;
        /// <summary>
        /// The ID of the owner. The ID represents where the comment is placed.
        /// </summary>
        [Column(Required = true, ReadOnly = true, Title = ModelProperties.Header_OwnerID, Name = "owner_id")]
        public int OwnerID
        {
            get { return _owner_id; }
            set
            {
                if (value != _owner_id)
                {
                    _owner_id = value;
                    RaisePropertyChanged("OwnerID");
                }
            }
        }

        private Address _profileAddress;
        /// <summary>
        /// The address record for the profile
        /// </summary>
        [Column(HideInGrid = true, Virtual = true)]
        public Address ProfileAddress
        {
            get { return _profileAddress; }
            set
            {
                if (value != _profileAddress)
                {
                    _profileAddress = value;
                    RaisePropertyChanged("ProfileAddress");
                }
            }
        }

        private DateTime _dateCreated;
        /// <summary>
        /// The date the record was created.
        /// </summary>
        [Column(HideInGrid = true, ReadOnly = true, Field = FieldType.Date, Name = "date_created", Title = ModelProperties.Header_DateCreated)]
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set
            {
                if (value != _dateCreated)
                {
                    _dateCreated = value;
                    RaisePropertyChanged("DateCreated");
                }
            }
        }

        private DateTime _lateUpdate;
        /// <summary>
        /// The date the record was last updated.
        /// </summary>
        [Column(HideInGrid = true, ReadOnly = true, Field = FieldType.Date, Name = "date_last_update", Title = ModelProperties.Header_LastUpdate)]
        public DateTime LastUpdate
        {
            get { return _lateUpdate; }
            set
            {
                if (value != _lateUpdate)
                {
                    _lateUpdate = value;
                    RaisePropertyChanged("LastUpdate");
                }
            }
        }


        private static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_profilesTableAdapter _tableAdapter;
        /// <summary>
        /// Gets the model table's adapter
        /// </summary>
        /// <value>The model table's adapter.</value>
        [Ignore]
        public static TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_profilesTableAdapter TableAdapter
        {
            get
            {
                if (_tableAdapter == null)
                {

                    _tableAdapter = new TheProfessional.Model.Data.TheProfessionalDataSetTableAdapters.professionals_profilesTableAdapter();
                }
                return _tableAdapter;
            }
        }
        #endregion

        #region IModel Implementation: Methods
        /// <summary>
        /// Updates this model from DataRowView
        /// </summary>
        /// <returns>none</returns>
        /// <param name="record">record.</param>
        public void UpdateFromView(DataRowView record)
        {
            if (record == null)
                return;

            try
            {
                ID = (int)record["profile_id"];
                ProfileType = (int)record["profile_type"];
                Owner = (ProfileOwner)EnumToValueConverter.GetEnumValue(record["owner"].ToString(), typeof(ProfileOwner));
                OwnerID = Convert.ToInt32(record["owner_id"]);
                DateCreated = (DateTime)record["date_created"];
                LastUpdate = (DateTime)record["date_last_update"];

                ProfileAddress = new Address();
                ProfileAddress.Address1 = record["address1"].ToString();
                ProfileAddress.Address2 = record["address2"].ToString();
                ProfileAddress.City = record["city"].ToString();
                ProfileAddress.State = (int)record["state"];
                ProfileAddress.ZipCode = record["zip_code"].ToString();
                ProfileAddress.Phone1 = record["phone1"].ToString();
                ProfileAddress.Phone2 = record["phone2"].ToString();
                ProfileAddress.Fax = record["fax"].ToString();
                ProfileAddress.Email = record["email"].ToString();
                ProfileAddress.URL = record["url"].ToString();
            }
            catch (Exception ex)
            {
                // Get call stack
                StackTrace stackTrace = new StackTrace();
                String methodName = stackTrace.GetFrame(1).GetMethod().Name;

                // Get calling method name
                Trace.WriteLine(methodName + ": " + ex.Message);
            }
        }

        /// <summary>
        /// Copies the current data to the DataRow.
        /// </summary>
        /// <returns>The data row.</returns>
        /// <param name="row">Row.</param>
        public DataRow CopyToDataRow(DataRow row)
        {
            if (row != null)
            {
                row["profile_id"] = this.ID;
                row["profile_type"] = this.ProfileType;
                row["owner"] = EnumToValueConverter.GetDefaultValue(this.Owner);
                row["owner_id"] = this.OwnerID;
                row["date_created"] = this.DateCreated;
                row["date_last_update"] = this.LastUpdate;
                row["address1"] = ProfileAddress.Address1;
                row["address2"] = ProfileAddress.Address2;
                row["city"] = ProfileAddress.City;
                row["state"] = ProfileAddress.State;
                row["zip_code"] = ProfileAddress.ZipCode;
                row["phone1"] = ProfileAddress.Phone1;
                row["phone2"] = ProfileAddress.Phone2;
                row["fax"] = ProfileAddress.Fax;
                row["email"] = ProfileAddress.Email;
                row["url"] = ProfileAddress.URL;
            }

            return row;
        }
        #endregion
    }
}